package com.shwifty.tex.activities;


import android.app.Application;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.audiofx.EnvironmentalReverb;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shwifty.tex.R;
import com.shwifty.tex.handlers.CastHandler;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.handlers.TPBHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.services.LibtorrentService;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.utilities.AnalyticsConstants;
import com.shwifty.tex.utilities.Constants;
import com.shwifty.tex.utilities.Prefs;


import java.io.File;
import java.util.ArrayList;

import go.libtorrent.Libtorrent;
import go.libtorrent.StatsTorrent;

public class MainApplication extends Application {
    private static MainApplication sInstance = null;
    private LibtorrentService libtorrentService;
    public final String TAG_NOTIFICATION_CANCEL = "cancelDownload";
    private IntentFilter filter = new IntentFilter();
    public boolean holdHintShown = false;
    private boolean paused;
    private static boolean bound = false;
    private CastHandler castHandler;
    private static int versionNumber;
    private static String hostUrl;
    public static final String TAG_APK_DOWNLOAD = "apkDownload";
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        sInstance = this;
        TorrentHandler torrentHandler = new TorrentHandler();
        torrentHandler.initialiseTorrentList();
        castHandler = new CastHandler();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle params = new Bundle();
        if (Prefs.SEARCH_ENABLED) {
            params.putString("event_name", AnalyticsConstants.LAUNCH_SEARCH_ENABLED_APP_NAME);
            mFirebaseAnalytics.logEvent(AnalyticsConstants.LAUNCH_SEARCH_ENABLED_BUNDLE, params);
        } else {
            params.putString("event_name", AnalyticsConstants.LAUNCH_SEARCH_DISABLED_APP_NAME);
            mFirebaseAnalytics.logEvent(AnalyticsConstants.LAUNCH_SEARCH_DISABLED_BUNDLE, params);
        }


//        castHandler.initializeCastContext(this);
        super.onCreate();
    }

    public static MainApplication getInstance() {
        return sInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void bindToLibtorrent(Context context) {
        Intent intent = new Intent(context.getApplicationContext(), LibtorrentService.class);
        context.getApplicationContext().bindService(intent, libtorrentConnection, Context.BIND_AUTO_CREATE);
        filter.addAction(TAG_NOTIFICATION_CANCEL);
        BroadcastReceiver notificationCancelReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                TorrentHandler torrentHandler = new TorrentHandler();
                if (intent.getAction().equals(TAG_NOTIFICATION_CANCEL)) {
                    long receivedID = intent.getLongExtra("torrentID", 0);
                    if (receivedID > 0) {
                        Torrent torrent = torrentHandler.getTorrentFromID(receivedID);
                        stopTorrent(context, torrent);
                        DownloadHandler.removeNotification(context, torrent);
                    }

                }

            }


        };
        context.getApplicationContext().registerReceiver(notificationCancelReceiver, filter);
    }

    public ServiceConnection libtorrentConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LibtorrentService.LocalBinder binder = (LibtorrentService.LocalBinder) service;
            libtorrentService = binder.getService();
            libtorrentService.createLibTorrent();
            showRunningNotification();
            bound = true;
            libtorrentService.startNetworkReceiver();
            SplashActivity.notifyLock();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };

    public void startTorrent(Context context, Torrent torrent) {
        libtorrentService.startTorrent(torrent);
        torrent.setDownloading(true);
        DownloadHandler.showNotification(context, torrent);
    }

    public void startForStream(Torrent torrent) {
        libtorrentService.startTorrent(torrent);
        torrent.setDownloading(true);
    }

    public void stopTorrent(Context context, Torrent torrent) {
        if (torrent == null) return;
        torrent.setDownloading(false);
        libtorrentService.stopTorrent(torrent);
        DownloadHandler.removeNotification(context, torrent);
    }

    public void torrentFileCheck(Torrent torrent, long index, boolean checked) {
        libtorrentService.torrentFilesCheck(torrent, index, checked);
    }

    public long addMagnet(String storagePath, String magnetString) {
        if (magnetString == null) {
            return -1;
        }

        if (libtorrentService == null) {
            bindToLibtorrent(getApplicationContext());
            while (!isBound()) {
            }
        }


        return libtorrentService.addMagnet(storagePath, magnetString);
    }

    public void downloadMetadata(long torrentID) {
        libtorrentService.downloadMetadata(torrentID);
    }

    public long getFileCount(long torrentID) {
        return libtorrentService.getFileCount(torrentID);
    }

    public go.libtorrent.File getTorrentFile(long torrentID, int index) {
        return libtorrentService.getTorrentFile(torrentID, index);
    }

    public boolean isMetaTorrent(long torrentID) {
        return libtorrentService.isMetaTorrent(torrentID);
    }

    public long getBytesLength(long torrentID) {
        return libtorrentService.getBytesLength(torrentID);
    }

    public long getBytesCompleted(long torrentID) {
        return libtorrentService.getBytesCompleted(torrentID);
    }

    public int getTorrentStatus(long torrentID) {
        return libtorrentService.getTorrentStatus(torrentID);
    }

    public int statusChecking() {
        return libtorrentService.statusChecking();
    }

    public int statusQueued() {
        return libtorrentService.statusQueued();
    }

    public int statusPaused() {
        return libtorrentService.statusPaused();
    }

    public StatsTorrent getTorrentStats(long torrentID) {
        return libtorrentService.getTorrentStats(torrentID);
    }

    public void removeTorrent(long torrentID) {
        libtorrentService.removeTorrent(torrentID);
    }

    public String getTorrentName(long torrentID) {
        return libtorrentService.getName(torrentID);
    }

    public byte[] readBytes(long torrentID, long fileIndex, long offset, long length) {
        return libtorrentService.readBytes(torrentID, fileIndex, offset, length);
    }


    public void showRunningNotification() {
        libtorrentService.notificationRunning();
    }

    public LibtorrentService getLibtorrentService() {
        return libtorrentService;
    }


    public boolean isBound() {
        return bound;
    }


    public void pauseLibtorrent() {
        libtorrentService.pause();
        paused = true;
    }

    public void noConnectionLibtorrent() {
        libtorrentService.noNetwork();

    }

    public void resumeLibtorrent() {
        libtorrentService.resume();
        paused = false;
    }

    public void shutdown(Context context) {
        TorrentHandler.shutDown(context);
        if (libtorrentConnection != null) {
            unbindService(libtorrentConnection);
        }
        if (libtorrentService != null) libtorrentService.stopService();
        bound = false;
        DownloadHandler downloadHandler = new DownloadHandler(context);
        downloadHandler.shutdown(context);
        TPBHandler.stopWaitingForTorrents(); //set torrentsSet
        if (Prefs.cleanStorage) {
            cleanStorage(context);
        }
        ExitActivity.exitApplication(context);
    }

    public boolean cleanStorage(Context context) {
        ArrayList<TorrentFile> downloadingTorrentFiles = new ArrayList<>();
        DownloadHandler downloadHandler = new DownloadHandler(context);
        ArrayList<Torrent> downloadingTorrents = downloadHandler.getTorrents();
        TorrentHandler torrentHandler = new TorrentHandler();
        ArrayList<Torrent> torrentsInApp = torrentHandler.getTorrents();
        ArrayList<String> pathsToDelete = new ArrayList<>();
        ArrayList<String> allDirectories = new ArrayList<>();

        if (!downloadHandler.isTorrentsLoaded()) {
            return false;
        }

        //find files that are in download manager
        //iterate through torrents and files in app
        //if theyre not in download manager delete them

        for (int i = 0; i < downloadingTorrents.size(); i++) {
            ArrayList<TorrentFile> tfs = downloadingTorrents.get(i).getTorrentFiles();
            for (int x = 0; x < tfs.size(); x++) {
                TorrentFile tf = tfs.get(x);
                if (tf.file.getCheck()) {
                    downloadingTorrentFiles.add(tf);
                }
            }
        }


        for (Torrent torrentInApp : torrentsInApp) {
            stopTorrent(context, torrentInApp);
            allDirectories.add(torrentInApp.getDirectory() + File.separator + torrentInApp.getName());
            for (TorrentFile torrentFile : torrentInApp.getTorrentFiles()) {
                boolean shouldDelete = true;
                for (TorrentFile downloadingTorrentFile : downloadingTorrentFiles) {
                    if (torrentFile.file.getPath().equals(downloadingTorrentFile.file.getPath())) {
                        shouldDelete = false;
                    }
                }
                if (shouldDelete) {
                    pathsToDelete.add(torrentInApp.getDirectory() + File.separator + torrentFile.file.getPath());
                }
            }
        }

        for (String path : pathsToDelete) {
            File file = new File(path);
            boolean deleted = file.delete();
            if (!deleted) Log.e("ERROR", "could not delete file: " + file.getPath());
        }


        for (String dir : allDirectories) {
            File file = new File(dir);
            boolean deleted = file.delete();
            if (!deleted) Log.e("ERROR", "could not delete directory: " + file.getPath());
        }


        return false;


    }

    public void cleanStorageOfTorrent(Torrent torrent) {
        String directory = torrent.getDirectory();
        ArrayList<String> pathsToDelete = new ArrayList<>();
        for (TorrentFile torrentFile : torrent.getTorrentFiles()) {
            if (!torrentFile.file.getCheck()) {
                pathsToDelete.add(directory + File.separator + torrentFile.file.getPath());
            }
        }

        boolean deleteErrors = false;
        for (String path : pathsToDelete) {
            File file = new File(path);
            if (file.exists()) {
                boolean deleted = file.delete();
                if (!deleted) {
                    deleteErrors = true;
                }
            }
        }

        if (deleteErrors) {
            Toast.makeText(getApplicationContext(), "Could not remove " + torrent.getName() + "after streaming", Toast.LENGTH_LONG).show();
        }
    }

    public boolean isPaused() {
        return paused;
    }

    public CastHandler getCastHandler() {
        return castHandler;
    }


    public static void downloadSearchEnabledAPK(Context context) {
        if (context instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) context;
            if (!mainActivity.hasPermission()) {
                mainActivity.setPermissionRequestFrom(MainActivity.DOWNLOAD_SEARCH_APK_ID);
                mainActivity.requestPermission();

                return;
            }
        }

        if (hostUrl != null) {
            startAPKDownload(context, hostUrl);
        } else {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference();

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        if (postSnapshot.getKey().equals(Constants.downloadAPKKey)) {
                            hostUrl = postSnapshot.getValue(String.class);
                            startAPKDownload(context, hostUrl);
                        }


                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private static void startAPKDownload(Context context, String host) {
        Uri mUri = Uri.parse(host);
        DownloadManager.Request r = new DownloadManager.Request(mUri);
        String fileName = context.getString(R.string.app_name) + ".apk";
        r.setTitle(fileName);
        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        r.setMimeType("application/vnd.android.package-archive");
        DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        long id = dm.enqueue(r);

        SharedPreferences prefs = context.getSharedPreferences(TAG_APK_DOWNLOAD, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putLong(TAG_APK_DOWNLOAD, id);
        prefsEditor.apply();
    }

    public static int getVersionNumber() {
        return versionNumber;
    }

    public static void setVersionNumber(int versionNumber) {
        MainApplication.versionNumber = versionNumber;
    }

    public static String getHostUrl() {
        return hostUrl;
    }

    public static void setHostUrl(String hostUrl) {
        MainApplication.hostUrl = hostUrl;
    }
}
