package com.shwifty.tex.activities;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shwifty.tex.R;
import com.shwifty.tex.dialogs.Disclaimer;
import com.shwifty.tex.dialogs.SplashCannotConnectTPBAlert;
import com.shwifty.tex.dialogs.SplashNoNetwork;
import com.shwifty.tex.handlers.AddMagnetHandler;
import com.shwifty.tex.handlers.FirebaseHandler;
import com.shwifty.tex.handlers.TPBHandler;
import com.shwifty.tex.utilities.AnalyticsConstants;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.Prefs;
import com.shwifty.tex.utilities.Utilities;
import com.shwifty.tex.utilities.Constants;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends Activity {
    @BindView(R.id.splashLayout)
    RelativeLayout splashLayout;
    @BindView(R.id.splashText)
    TextView splashText;
    @BindView(R.id.tapToRetry)
    TextView tapToRetry;
    private boolean mIsStateAlreadySaved = false;
    private SplashNoNetwork noNetworkAlert;
    private SplashCannotConnectTPBAlert cannotConnectTPBAlert;
    private Disclaimer disclaimer;
    private Context context;
    private MainApplication mainApplication;
    private static final Object LOCK = new Object();
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        mainApplication = MainApplication.getInstance();

        int networkStatus = NetworkUtil.getConnectivityStatus(context);
        if (networkStatus == NetworkUtil.TYPE_NOT_CONNECTED) {
            showNoNetworkNetworkDialog();
        }

        final Intent thisIntent = getIntent();
        String scheme = thisIntent.getScheme();
        if (scheme != null) {
            if (scheme.equalsIgnoreCase("magnet")) {
                AddMagnetHandler.setMagnet(thisIntent.getDataString());
            }
        }


        Prefs.loadWIFI_ONLYFromSP(context);
        Prefs.loadCleanStorageFromSP(context);
        Prefs.loadPlaySoundFromSP(context);
        Prefs.loadDownloadPath(context);
        Prefs.loadDisclaimerAccepted(context);


        splashLayout.setOnClickListener(view -> {
                    if (tapToRetry.getVisibility() == View.VISIBLE) {
                        tapToRetry.setVisibility(View.INVISIBLE);
                        splashText.setText(R.string.retrying);
                        startLaunchThread();
                    }
                }

        );

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("activity_name", AnalyticsConstants.SPLASH_NAME);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.START_SPLASH, params);
    }

    @Override
    protected void onResume() {
        mIsStateAlreadySaved = false;
        super.onResume();

        if (Prefs.SEARCH_ENABLED) {
            if (readySearchEnabled()) {
                startMain();
            } else {
                startLaunchThread();
            }

        } else {
            if (readySearchDisabled()) {
                startMain();
            } else {
                startLaunchThread();
            }
        }
    }

    private void startMain() {
        Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
        mainIntent.putExtra("comingFromSplash", true);
        startActivity(mainIntent);
        finish();
    }

    private void startLaunchThread() {
        Thread thread = new Thread(() -> {


            runOnUiThread(() -> splashText.setText(R.string.creating_directory));
            File fileDir = new File(Prefs.downloadPath);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }


            runOnUiThread(() -> splashText.setText(R.string.binding));
            while (!mainApplication.isBound()) {
                Thread bindingThread = new Thread(() -> mainApplication.bindToLibtorrent(getApplicationContext()));
                bindingThread.start();
                lock();

            }

            Thread syncUpdateThread = new Thread(() -> FirebaseHandler.syncFirebaseUpdateInfo(LOCK));
            syncUpdateThread.start();
            lock();


            if (Prefs.SEARCH_ENABLED) {
                runOnUiThread(() -> splashText.setText(R.string.discovering));
                while (Constants.UrlTpb.length == 0) {
                    Thread syncMirrorUrlsThread = new Thread(() -> FirebaseHandler.getMirrorUrls(LOCK));
                    syncMirrorUrlsThread.start();
                    lock();
                }


                runOnUiThread(() -> splashText.setText(R.string.finding_fastest_mirror));
                while (!Constants.isFastestSet()) {
                    Thread pinProxiesThread = new Thread(() -> Utilities.pingProxies(context));
                    pinProxiesThread.start();
                    lock();
                }


//                runOnUiThread(() -> splashText.setText(R.string.loading));
//                TPBHandler tpbHandler = new TPBHandler();
//                while (!tpbHandler.isTorrentsSet()) {
//                    try {
//                        LOCK.wait();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
            }

            if (!Prefs.disclaimerAccepted) {
                showDisclaimer();
                runOnUiThread(() -> splashText.setText(R.string.waiting_for_disclaimer));
            }


            while (!Prefs.disclaimerAccepted) {
            }

            startMain();
        });
        thread.start();
    }

    @Override
    protected void onPause() {
        mIsStateAlreadySaved = true;
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void showNoNetworkNetworkDialog() {
        if (getFragmentManager() == null) return;
        if (noNetworkAlert != null) {
            noNetworkAlert.dismiss();
        }
        noNetworkAlert = new SplashNoNetwork();
        if (mIsStateAlreadySaved) return;
        noNetworkAlert.show(getFragmentManager(), "dialog");
        runOnUiThread(() -> {
            splashText.setText(R.string.no_network);
            tapToRetry.setVisibility(View.VISIBLE);
        });

    }


    public void showUnableToConnectTPBDialog() {
        if (getFragmentManager() == null) return;
        if (!mIsStateAlreadySaved) {
            if (cannotConnectTPBAlert != null) {
                cannotConnectTPBAlert.dismiss();
            }
            cannotConnectTPBAlert = new SplashCannotConnectTPBAlert();
            try {
                cannotConnectTPBAlert.show(getFragmentManager(), "dialog");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        runOnUiThread(() -> {
            splashText.setText(R.string.tpb_down);
            tapToRetry.setVisibility(View.VISIBLE);
        });
    }

    public void showDisclaimer() {
        if (getFragmentManager() == null) return;
        if (!mIsStateAlreadySaved) {
            if (disclaimer != null) {
                disclaimer.dismiss();
            }
            disclaimer = new Disclaimer();
            try {
                disclaimer.show(getFragmentManager(), "dialog");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setDisclaimerAccepted(boolean disclaimerAccepted) {
        Prefs.disclaimerAccepted = disclaimerAccepted;
        Prefs.saveDisclaimerAccepted(this);
    }


    private boolean readySearchEnabled() {
        if (Constants.UrlTpb.length == 0) {
            return false;
        }

        if (!Constants.isFastestSet()) {
            return false;
        }

        TPBHandler tpbHandler = new TPBHandler();
        if (!tpbHandler.isTorrentsSet()) {
            return false;
        }

        return readySearchDisabled();
    }

    private boolean readySearchDisabled() {
        if (!mainApplication.isBound() || !Prefs.disclaimerAccepted) {
            return false;
        }

        return true;
    }

    private static void lock() {
        synchronized (LOCK) {
            try {
                LOCK.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void notifyLock() {
        synchronized (LOCK) {
            LOCK.notify();
        }
    }

}