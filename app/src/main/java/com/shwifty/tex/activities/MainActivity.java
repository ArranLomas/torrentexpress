package com.shwifty.tex.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;


//TESTS start download torrent, exit app, click notification, exit app, stop internet, click notification

import android.Manifest;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.MediaRouteChooserDialog;
import android.support.v7.media.MediaControlIntent;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.RemotePlaybackClient;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mikepenz.aboutlibraries.LibsBuilder;
import com.mikepenz.aboutlibraries.ui.LibsSupportFragment;
import com.shwifty.tex.BuildConfig;
import com.shwifty.tex.R;
import com.shwifty.tex.dialogs.CouldntLocateFileAlert;
import com.shwifty.tex.dialogs.DownloadDialogFragment;
import com.shwifty.tex.dialogs.DownloadOnNetworkAlert;
import com.shwifty.tex.dialogs.ExitAlert;
import com.shwifty.tex.dialogs.HandleMagnetIntentAlert;
import com.shwifty.tex.dialogs.NoNetworkAlert;
import com.shwifty.tex.dialogs.OpenVideoAlert;
import com.shwifty.tex.dialogs.SelectFileDialog;
import com.shwifty.tex.dialogs.ShowTrackersDialog;
import com.shwifty.tex.dialogs.StreamDialogFragment;
import com.shwifty.tex.dialogs.TorrentAlreadyAddedAlert;
import com.shwifty.tex.dialogs.UnableToConnectTPBAlert;
import com.shwifty.tex.dialogs.UpdateAppAlert;
import com.shwifty.tex.fragments.DownloadManagerFragment;
import com.shwifty.tex.fragments.SearchFragment;
import com.shwifty.tex.fragments.SettingsFragment;
import com.shwifty.tex.fragments.TrackersFragment;

import com.shwifty.tex.handlers.AddMagnetHandler;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.handlers.LoadMoreHandler;
import com.shwifty.tex.handlers.SearchHandler;
import com.shwifty.tex.handlers.ServerHandler;
import com.shwifty.tex.handlers.TPBHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.handlers.VLCHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.utilities.AnalyticsConstants;
import com.shwifty.tex.utilities.Format;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.Prefs;
import com.shwifty.tex.utilities.SetupStream;
import com.shwifty.tex.utilities.jpa.QueryOrder;
import com.shwifty.tex.utilities.jpa.TorrentCategory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.locks.Lock;

import butterknife.BindString;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1;
    private Context context;
    private String currentFrag;
    private boolean inSearchFragment;
    public static final String TAG_FILES_DIALOG = "TAG_FILES_DIALOG";
    public static final String TAG_SHOWING_FRAG = "TAG_SHOWING_FRAG";
    public static final String TAG_TORRENT_POSITION = "TAG_TORRENT_POSITION";
    public static final String TAG_TORRENT_NAME = "TAG_TORRENT_NAME";
    public static final String TAG_DIALOG = "TAG_DIALOG";
    public static final int DOWNLOAD_CLICK_ID = 0;
    public static final int STREAM_CLICK_ID = 1;
    public static final int SHOW_FILE_BROWSER_ID = 2;
    public static final int DOWNLOAD_SEARCH_APK_ID = 3;
    @BindString(R.string.action_show_settings)
    String ACTION_SHOW_SETTINGS;
    @BindString(R.string.action_show_downloads)
    String ACTION_SHOW_DOWNLOADS;
    @BindString(R.string.app_name)
    String appName;
    @BindString(R.string.about)
    String aboutString;
    @BindString(R.string.admob_id)
    String admobId;
    @BindString(R.string.test_device_id)
    String testDeviceID;
    @BindString(R.string.stream_interstatial_ad)
    String interstatialID;
    CharSequence mTitle;
    private FloatingActionButton emailFab;
    private MenuItem searchItem;
    private SearchView searchView;
    private EditText searchEditText;
    private NavigationView navigationView;
    private ProgressDialog loadingTorrentsSpinner;
    private ProgressDialog addingMagnetProgress;
    private FrameLayout contentFrame;
    private RelativeLayout relativeLayout;
    private boolean mIsStateAlreadySaved = false;
    private UnableToConnectTPBAlert unableToConnectTPBAlert;
    private MenuItem mediaRouteMenuItem;

    private HandleMagnetIntentAlert handleMagnetIntentAlert;
    private TorrentAlreadyAddedAlert torrentAlreadyAddedAlert;
    private DownloadOnNetworkAlert downloadOnNetworkAlert;
    private NoNetworkAlert noNetworkAlert;
    private OpenVideoAlert openVideoAlert;
    private UpdateAppAlert updateAppAlert;
    private int permissionRequestFrom = 0;
    private CouldntLocateFileAlert couldntLocateFileAlert;
    private SetupStream setupStream;

    private GoogleApiClient client;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getCastHandler().initializeCastContext(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        setupView();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (BuildConfig.DEBUG) {
            FirebaseAnalytics.getInstance(getApplicationContext()).setAnalyticsCollectionEnabled(false);
        }

        MobileAds.initialize(getApplicationContext(), admobId);


//        AdHandler.createAd(this, interstatialID, testDeviceID);

        DownloadHandler downloadHandler = new DownloadHandler(context);
        if (!downloadHandler.isTorrentsLoaded()) {
            downloadHandler.loadTorrents(this);
        }

        if (savedInstanceState != null) {
            String lastFrag = savedInstanceState.getString(TAG_SHOWING_FRAG);
            if (lastFrag != null) {
                if (lastFrag.equals("search")) {
                    showSearchFragment();
                } else if (lastFrag.equals("downloads")) {
                    showDownloadsFragment();
                } else if (lastFrag.equals("settings")) {
                    showSettingsFragment();
                } else if (lastFrag.equalsIgnoreCase("about")) {
                    showAboutFragment();
                }
            }

        } else {
            handleIntent(getIntent());

        }

        emailFab.setOnClickListener(view -> {
            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.tex_email)});
            if (pInfo == null) {
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
            } else {
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subbject_version) + pInfo.versionName + getString(R.string.subject_vestion_alt) + pInfo.versionCode);
            }

            i.putExtra(Intent.EXTRA_TEXT, getString(R.string.email_body));
            try {
                startActivity(Intent.createChooser(i, getString(R.string.send_email_prompt)));
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(MainActivity.this, R.string.not_email_clients_prompt, Toast.LENGTH_SHORT).show();
            }
        });

        if (!MainApplication.getInstance().isBound()) {
            Thread thread = new Thread(() -> {
                MainApplication.getInstance().bindToLibtorrent(this);
            });
            thread.start();
        }

        Bundle params = new Bundle();
        params.putString("activity_name", AnalyticsConstants.MAIN_NAME);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.START_MAIN, params);

        if (Prefs.SEARCH_ENABLED) {
            checkVersionUpdated();
        }
        
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(TAG_SHOWING_FRAG, currentFrag);
        super.onSaveInstanceState(outState);
    }

    private void setupView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        toolbar.setOnClickListener(view -> {
            if (inSearchFragment) {
                searchItem.expandActionView();
                searchView.setIconified(false);
                searchEditText.requestFocus();
                searchEditText.setSelection(searchEditText.getText().length());

                if (toolbar.getTitle().toString().equalsIgnoreCase(appName)) {
                    searchEditText.setText(R.string.blank);
                } else {
                    searchEditText.setText(toolbar.getTitle());
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setSelected(true);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        emailFab = (FloatingActionButton) findViewById(R.id.emailFab);
        contentFrame = (FrameLayout) findViewById(R.id.content_frame);
        relativeLayout = (RelativeLayout) findViewById(R.id.content_relative_layout);

//        AdView mAdView = (AdView) findViewById(R.id.mainBannerAd);
//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(testDeviceID)
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .build();
//        mAdView.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
//        Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        Fragment currentFrag = getCurrentFragment();
        if (currentFrag instanceof DownloadManagerFragment && currentFrag.isVisible()) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                showSearchFragment();
            }
        } else if (currentFrag instanceof SearchFragment && currentFrag.isVisible()) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                ExitAlert newFragment = new ExitAlert();
                newFragment.show(getFragmentManager(), "exit");
            }
        } else if (currentFrag instanceof SettingsFragment && currentFrag.isVisible()) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                showSearchFragment();
            }
        } else if (currentFrag instanceof LibsSupportFragment && currentFrag.isVisible()) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                showSearchFragment();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mIsStateAlreadySaved = true;
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof SearchFragment) {
            SearchFragment searchFragment = (SearchFragment) fragment;
            searchFragment.stopLoadingFiles();
        }
        dismissAllDialogs();
        MainApplication.getInstance().getCastHandler().removeSessionListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AddMagnetHandler.handleMagnetIntent(this);
        mIsStateAlreadySaved = false;
        File fileDir = new File(Prefs.downloadPath);
        if (!fileDir.exists()) {
            boolean createSuccessful = fileDir.mkdirs();
            if (!createSuccessful && !hasPermission()) {
                requestPermission();
            }
        }
        MainApplication.getInstance().getCastHandler().addSessionListener();

       TorrentHandler torrentHandler = new TorrentHandler();
        torrentHandler.stopStreamingTorrents(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchItem = menu.findItem(R.id.action_search);
        if (!inSearchFragment) {
            searchItem.setVisible(false);
        }
        searchView = (SearchView) searchItem.getActionView();

        searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(Color.WHITE);
        searchEditText.setHintTextColor(Color.WHITE);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu, R.id.media_route_menu_item);

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_movies) {
            executeTorrentSearch("*", TorrentCategory.Movies);
            showSearchFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(0).getSubMenu().getItem(0).setChecked(true);
        } else if (id == R.id.nav_tv) {
            executeTorrentSearch("*", TorrentCategory.TVshows);
            showSearchFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(0).getSubMenu().getItem(1).setChecked(true);
        } else if (id == R.id.nav_music) {
            executeTorrentSearch("*", TorrentCategory.Music);
            showSearchFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(0).getSubMenu().getItem(2).setChecked(true);
        } else if (id == R.id.nav_android) {
            executeTorrentSearch("*", TorrentCategory.AndroidApplications);
            showSearchFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(0).getSubMenu().getItem(3).setChecked(true);
        } else if (id == R.id.nav_all) {
            executeTorrentSearch("*", TorrentCategory.All);
            showSearchFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(0).getSubMenu().getItem(4).setChecked(true);
        } else if (id == R.id.nav_downloads) {
            showDownloadsFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(1).getSubMenu().getItem(0).setChecked(true);
        } else if (id == R.id.nav_open_download) {
            final SelectFileDialog f = new SelectFileDialog(context);
            File file = new File(Prefs.downloadPath);
            f.setCurrentPath(file);
            f.show();
        } else if (id == R.id.nav_settings) {
            showSettingsFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(1).getSubMenu().getItem(2).setChecked(true);
        } else if (id == R.id.nav_about) {
            showAboutFragment();
            uncheckNavigationItems();
            navigationView.getMenu().getItem(1).getSubMenu().getItem(3).setChecked(true);
        } else if (id == R.id.nav_exit) {
            ExitAlert newFragment = new ExitAlert();
            newFragment.show(getFragmentManager(), "exit");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void uncheckNavigationItems() {
        navigationView.getMenu().getItem(0).getSubMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(0).getSubMenu().getItem(1).setChecked(false);
        navigationView.getMenu().getItem(0).getSubMenu().getItem(2).setChecked(false);
        navigationView.getMenu().getItem(0).getSubMenu().getItem(3).setChecked(false);
        navigationView.getMenu().getItem(0).getSubMenu().getItem(4).setChecked(false);
        navigationView.getMenu().getItem(1).getSubMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(1).getSubMenu().getItem(1).setChecked(false);
        navigationView.getMenu().getItem(1).getSubMenu().getItem(2).setChecked(false);
        navigationView.getMenu().getItem(1).getSubMenu().getItem(3).setChecked(false);
        navigationView.getMenu().getItem(1).getSubMenu().getItem(4).setChecked(false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
        this.recreate();
    }


    private void handleIntent(Intent intent) {
        boolean comingFromSplash = intent.getBooleanExtra("comingFromSplash", false);

        if (comingFromSplash) {
            showSearchFragment();
            return;
        }

        String mQuery;
        if (Objects.equals(Intent.ACTION_SEARCH, intent.getAction())) {
            mQuery = intent.getStringExtra(SearchManager.QUERY);

            if (mQuery == null) {
                mQuery = "*";
            }
            if (mQuery.startsWith(Format.magnetLinkIndetifier)) {
                AddMagnetHandler.setMagnet(mQuery);
                AddMagnetHandler.handleMagnetIntent(this);
                showSearchFragment();
                return;
            }

            if (getSupportActionBar() != null) {
                if (mQuery.equals("*")) {
                    getSupportActionBar().setTitle(appName);
                } else {
                    getSupportActionBar().setTitle(mQuery);
                }
            }

            executeTorrentSearch(mQuery, TorrentCategory.All);
            showSearchFragment();
        } else if (ACTION_SHOW_DOWNLOADS.equals(intent.getAction())) {
            showDownloadsFragment();
        } else if (ACTION_SHOW_SETTINGS.equals(intent.getAction())) {
            showSettingsFragment();
        } else {
            mQuery = "*";
            executeTorrentSearch(mQuery, TorrentCategory.Movies);
            showSearchFragment();
        }
    }

    public void hideActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    public void showActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
        }
    }

    public boolean hasPermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Fragment fragment = getCurrentFragment();
                    if (fragment instanceof SearchFragment) {
                        SearchFragment searchFragment = (SearchFragment) fragment;
                        switch (permissionRequestFrom) {
                            case DOWNLOAD_CLICK_ID:
                                searchFragment.downloadClick();
                                break;
                            case STREAM_CLICK_ID:
                                searchFragment.streamClick();
                                break;
                            case DOWNLOAD_SEARCH_APK_ID:
                                MainApplication.downloadSearchEnabledAPK(this);
                        }


                    } else if (fragment instanceof SettingsFragment) {
                        SettingsFragment settingsFragment = (SettingsFragment) fragment;
                        if (permissionRequestFrom == SHOW_FILE_BROWSER_ID) {
                            settingsFragment.showFileBrowser();
                        }
                    }
                } else {
                    permissionDenied();
                }
            }
        }
    }

    private void permissionDenied() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            Toast.makeText(this, "Storage permission is required to continue", Toast.LENGTH_LONG).show();
        });
        hideDownloadFilesDialog();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mTitle);
        }
    }

    public void showSearchFragment() {
        Bundle params = new Bundle();
        params.putString("fragment_name", AnalyticsConstants.FRAG_NAME_SEARCH);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.BUNDLE_NAME_SEARCH, params);

        contentFrame.setPadding(0, 0, 0, 0);
        emailFab.hide();
        currentFrag = "search";
        showActionBar();
        inSearchFragment = true;
        invalidateOptionsMenu();
        uncheckNavigationItems();
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (!mIsStateAlreadySaved) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SearchFragment()).commit();
            uncheckNavigationItems();
            final int category = SearchHandler.getCategory();
            switch (category) {
                case TorrentCategory.Movies:
                    navigationView.getMenu().getItem(0).getSubMenu().getItem(0).setChecked(true);
                    break;
                case TorrentCategory.TVshows:
                    navigationView.getMenu().getItem(0).getSubMenu().getItem(1).setChecked(true);
                    break;
                case TorrentCategory.Music:
                    navigationView.getMenu().getItem(0).getSubMenu().getItem(2).setChecked(true);
                    break;
                case TorrentCategory.AndroidApplications:
                    navigationView.getMenu().getItem(0).getSubMenu().getItem(3).setChecked(true);
                    break;
                case TorrentCategory.All:
                    navigationView.getMenu().getItem(0).getSubMenu().getItem(4).setChecked(true);
                    break;
            }
            if (SearchHandler.getQuery().equalsIgnoreCase("*")) {
                setTitle(appName);
            } else {
                setTitle(SearchHandler.getQuery());
            }
        }

    }

    public void showDownloadsFragment() {
        Bundle params = new Bundle();
        params.putString("fragment_name", AnalyticsConstants.FRAG_NAME_DOWNLOADS);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.BUNDLE_NAME_DOWNLOADS, params);

        contentFrame.setPadding(0, 0, 0, 0);
        emailFab.hide();
        currentFrag = "downloads";
        showActionBar();
        inSearchFragment = false;
        invalidateOptionsMenu();
        uncheckNavigationItems();
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (!mIsStateAlreadySaved) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new DownloadManagerFragment()).commit();
            navigationView.getMenu().getItem(1).getSubMenu().getItem(0).setChecked(true);
            setTitle("Downloads");
        }
    }

    public void showSettingsFragment() {
        Bundle params = new Bundle();
        params.putString("fragment_name", AnalyticsConstants.FRAG_NAME_SETTINGS);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.BUNDLE_NAME_SETTINGS, params);

        contentFrame.setPadding(0, 0, 0, 0);
        emailFab.hide();
        currentFrag = "settings";
        showActionBar();
        inSearchFragment = false;
        invalidateOptionsMenu();
        uncheckNavigationItems();
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (!mIsStateAlreadySaved) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SettingsFragment()).commit();
            navigationView.getMenu().getItem(1).getSubMenu().getItem(1).setChecked(true);
            setTitle("Settings");
        }
    }

    public void showAboutFragment() {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        if (actionBarHeight == 0) {
            contentFrame.setPadding(0, 100, 0, 0);
        } else {
            contentFrame.setPadding(0, actionBarHeight, 0, 0);
        }
        emailFab.show();
        currentFrag = "about";
        showActionBar();
        inSearchFragment = false;
        invalidateOptionsMenu();
        uncheckNavigationItems();
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (!mIsStateAlreadySaved) {
            LibsBuilder builder = new LibsBuilder();
            builder.withAboutAppName(appName)
                    .withAboutDescription(aboutString)
                    .withAboutIconShown(true)
                    .withAboutVersionShown(true);

            LibsSupportFragment aboutFragment = builder.supportFragment();
            fragmentManager.beginTransaction().replace(R.id.content_frame, aboutFragment).commit();
//            navigationView.getMenu().getItem(1).getSubMenu().getItem(0).setChecked(true);
            setTitle("About");
        }
    }

    public void refreshTorrents() {
        executeTorrentSearch(SearchHandler.getQuery(), SearchHandler.getCategory());
    }

    private void executeTorrentSearch(String mQuery, int searchCategory) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, mQuery);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH, bundle);

        SearchHandler.cancel();

        TPBHandler tpbHandler = new TPBHandler();
        tpbHandler.setTorrentsSet(false);

        SearchHandler searchHandler = new SearchHandler(mQuery, searchCategory, QueryOrder.BySeeds);
        searchHandler.execute(context);

        Fragment fragment = getCurrentFragment();
        if (fragment instanceof SearchFragment) {
            SearchFragment searchFragment = (SearchFragment) fragment;
            searchFragment.cancelLoadMoreTorrents();
        }
    }

    public Fragment getCurrentFragment() {
        Fragment currentFrag;
        currentFrag = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        return currentFrag;
    }

    public void showFetchingFilesDialog() {
        loadingTorrentsSpinner = new ProgressDialog(context);
        loadingTorrentsSpinner.setMessage(getString(R.string.fetching_files));
        loadingTorrentsSpinner.show();
        loadingTorrentsSpinner.setOnDismissListener(dialogInterface -> {
            Fragment fragment = getCurrentFragment();
            if (fragment instanceof SearchFragment) {
                SearchFragment searchFragment = (SearchFragment) fragment;
                searchFragment.stopLoadingFiles();
            }
        });
    }

    public void hideProgressDialog() {
        if (loadingTorrentsSpinner != null) {
            loadingTorrentsSpinner.dismiss();
        }
    }


    public void showDownloadFilesDialog(String magnetString) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(TAG_FILES_DIALOG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DownloadDialogFragment newFragment = DownloadDialogFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(DownloadDialogFragment.TAG_MAGNET_ARG, magnetString);
        newFragment.setArguments(bundle);

        if (!mIsStateAlreadySaved) {
            newFragment.show(ft, TAG_FILES_DIALOG);
        }

    }

    public void showStreamFilesDialog(String magnetString) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(TAG_FILES_DIALOG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        StreamDialogFragment newFragment = StreamDialogFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(StreamDialogFragment.TAG_MAGNET_ARG, magnetString);
        newFragment.setArguments(bundle);

        if (!mIsStateAlreadySaved) {
            newFragment.show(ft, TAG_FILES_DIALOG);
        }

    }

    public void showTrackersDialog(long torrentID) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(TAG_DIALOG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        ShowTrackersDialog newFragment = ShowTrackersDialog.newInstance(torrentID);
        if (!mIsStateAlreadySaved) {
            newFragment.show(ft, TAG_FILES_DIALOG);
        }

    }


    public void hideDownloadFilesDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(TAG_FILES_DIALOG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
    }

    public void hideStreamFilesDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(TAG_FILES_DIALOG);
        if (prev instanceof StreamDialogFragment) {
            StreamDialogFragment dialogFragment = (StreamDialogFragment) prev;
            dialogFragment.dismiss();
        }
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
    }

    public void showDownloadOnMobileDataDialog() {
        if (downloadOnNetworkAlert != null) {
            downloadOnNetworkAlert.dismiss();
        }
        downloadOnNetworkAlert = new DownloadOnNetworkAlert();
        downloadOnNetworkAlert.show(getFragmentManager(), "downloadOnMobile");
    }

    public void showNoNetworkNetworkDialog() {
        if (noNetworkAlert != null) {
            noNetworkAlert.dismiss();
        }
        noNetworkAlert = new NoNetworkAlert();
        noNetworkAlert.show(getFragmentManager(), "noNetwork");
    }

    public void showOpenVideoDialog(String magnet) {
        if (openVideoAlert != null) {
            openVideoAlert.dismiss();
        }
        openVideoAlert = new OpenVideoAlert();
        openVideoAlert.setMagnet(magnet);
        openVideoAlert.show(getFragmentManager(), "openVideo");
    }

    public void showCouldntLocateDialog(int torrentPosition) {
        if (couldntLocateFileAlert != null) {
            couldntLocateFileAlert.dismiss();
        }
        couldntLocateFileAlert = new CouldntLocateFileAlert();
        Bundle bundle = new Bundle();
//        bundle.putString(TAG_TORRENT_NAME, torrentName);
        bundle.putInt(TAG_TORRENT_POSITION, torrentPosition);
        couldntLocateFileAlert.setArguments(bundle);
        couldntLocateFileAlert.show(getFragmentManager(), "couldntLocate");

    }

    public void showTorrentAlreadyDownloadingAlert(String magnetString, boolean dialogForStream) {
        if (torrentAlreadyAddedAlert != null) {
            torrentAlreadyAddedAlert.dismiss();
        }
        torrentAlreadyAddedAlert = new TorrentAlreadyAddedAlert();
        Bundle bundle = new Bundle();
        torrentAlreadyAddedAlert.setDialogForStream(dialogForStream);
        bundle.putString(TorrentAlreadyAddedAlert.TAG_TORRENT_MAGNET, magnetString);
        bundle.putBoolean(TorrentAlreadyAddedAlert.TAG_STREAM_OR_DOWNLOAD, dialogForStream);

        torrentAlreadyAddedAlert.setArguments(bundle);

        torrentAlreadyAddedAlert.show(getFragmentManager(), "torrentAlreadyAdded");
    }

    public void showUnableToConnectTPBDialog() {
        if (!mIsStateAlreadySaved) {
            if (unableToConnectTPBAlert != null) {
                unableToConnectTPBAlert.dismiss();
            }
            unableToConnectTPBAlert = new UnableToConnectTPBAlert();
            try {
                unableToConnectTPBAlert.show(getFragmentManager(), "unableToConnectTPB");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dismissAllDialogs() {
        try {
            hideProgressDialog();
            hideDownloadFilesDialog();
            if (downloadOnNetworkAlert != null) {
                downloadOnNetworkAlert.dismiss();
            }
            if (noNetworkAlert != null) {
                noNetworkAlert.dismiss();
            }
            if (unableToConnectTPBAlert != null) {
                unableToConnectTPBAlert.dismiss();
            }
            hideStreamFilesDialog();


            dismissHandleMagnetIntentAlert();
            dismissAddingMagnetDialog();

            if (openVideoAlert != null) {
                openVideoAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    public void removeTorrentFromApp(Torrent torrent) {
        if (getCurrentFragment() instanceof DownloadManagerFragment) {
            DownloadManagerFragment downloaderFrag = (DownloadManagerFragment) getCurrentFragment();
            downloaderFrag.removeTorrent(torrent);
        }

        DownloadHandler downloadHandler = new DownloadHandler(context);
        downloadHandler.removeTorrent(context, torrent);
        TorrentHandler torrentHandler = new TorrentHandler();
        torrentHandler.removeTorrent(torrent);
    }


    public void showAddingMagnetDialog() {
        if (addingMagnetProgress == null) {
            addingMagnetProgress = new ProgressDialog(context);
        }
        addingMagnetProgress.setMessage(getString(R.string.adding_torrent));
        if (!((Activity) context).isFinishing()) {
            try {
                addingMagnetProgress.show();
            } catch (Exception e) {
                e.printStackTrace();
                //TODO
                //DON'T WTF IS HAPPENING HERE!
                //java.lang.RuntimeException
                //android.view.WindowManager$BadTokenException: Unable to add window -- token android.os.BinderProxy@386e4 is not valid; is your activity running?
            }

        }
    }

    public void dismissAddingMagnetDialog() {
        if (addingMagnetProgress != null) {
            addingMagnetProgress.dismiss();
            addingMagnetProgress.setOnDismissListener(dialogInterface -> AddMagnetHandler.setMagnet(null));
        }
    }

    public boolean isAddingMagnetShowing() {
        return addingMagnetProgress != null && addingMagnetProgress.isShowing();

    }

    public void showHandleMagnetIntentAlert() {
        if (handleMagnetIntentAlert != null) {
            handleMagnetIntentAlert.dismiss();
        }
        handleMagnetIntentAlert = new HandleMagnetIntentAlert();
        if (!mIsStateAlreadySaved)
            handleMagnetIntentAlert.show(getFragmentManager(), "handleMagnetDialog");
        dismissAddingMagnetDialog();
    }

    public void dismissHandleMagnetIntentAlert() {
        if (handleMagnetIntentAlert != null) {
            handleMagnetIntentAlert.dismiss();
        }
    }

    public void showUpdateAppAlert() {
        if ((updateAppAlert != null) && updateAppAlert.isAdded()) {
            updateAppAlert.dismiss();
        }
        updateAppAlert = new UpdateAppAlert();
        if (!mIsStateAlreadySaved)
            try {
                updateAppAlert.show(getFragmentManager(), "updateAppAlert");
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
    }

    public void setPermissionRequestFrom(int permissionRequestFrom) {
        this.permissionRequestFrom = permissionRequestFrom;
    }

    public void notifyDownloadListChanged() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof DownloadManagerFragment) {
            DownloadManagerFragment downloadManagerFragment = (DownloadManagerFragment) fragment;
            downloadManagerFragment.notifyTorrentsAdded();
        }
    }

    public void showShareSnackbar(String streamUrl) {
        if (relativeLayout == null) return;
        Snackbar snackbar = Snackbar
                .make(relativeLayout, R.string.share_ling, Snackbar.LENGTH_LONG)
                .setAction(R.string.share, view -> {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            streamUrl);
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                });

        snackbar.show();
    }

    public void setupStream(Torrent torrent, int selectedPosition) {
        Bundle params = new Bundle();
        params.putString("event_name", AnalyticsConstants.START_STREAM_NAME);
        params.putString("torrent_name", torrent.getName());
        mFirebaseAnalytics.logEvent("start_event", params);

//        if (setupStream != null) {
//            setupStream.cancel();
//        }
        setupStream = new SetupStream(torrent, selectedPosition);
        setupStream.setup(this);
    }

    public void notifyFinnishedLoadingMoreTorrents(int previousSize) {
        if (getCurrentFragment() instanceof SearchFragment) {
            SearchFragment searchFragment = (SearchFragment) getCurrentFragment();
            searchFragment.notifyFinishedLoadingMoreTorrents(previousSize);
        }
    }

    private void checkVersionUpdated() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo == null) return;
        int version = pInfo.versionCode;
        if (version == 0) return;
        if (version < MainApplication.getVersionNumber()) {
            showUpdateAppAlert();
        }
    }

    public void logDownloadEvent(String torrentName) {
        Bundle params = new Bundle();
        params.putString("event_name", AnalyticsConstants.START_DOWNLOAD_NAME);
        params.putString("torrent_name", torrentName);
        mFirebaseAnalytics.logEvent("start_event", params);
    }

    public void logDownloadAPK() {
        Bundle params = new Bundle();
        params.putString("event_name", AnalyticsConstants.START_APK_DOWNLOAD_NAME);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.BUNDLE_START_APK_DOWNLOAD, params);
    }

    public void logIgnoreAPK() {
        Bundle params = new Bundle();
        params.putString("event_name", AnalyticsConstants.IGNORE_APK_DOWNLOAD_NAME);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.BUNDLE_IGNORE_APK, params);
    }

    public void logStreamClicked() {
        Bundle firebaseBundle = new Bundle();
        firebaseBundle.putString(FirebaseAnalytics.Param.ITEM_ID, AnalyticsConstants.STREAM_CLICKED_ID);
        firebaseBundle.putString(FirebaseAnalytics.Param.ITEM_NAME, AnalyticsConstants.STREAM_CLICKED_NAME);
        firebaseBundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, AnalyticsConstants.STREAM_CLICKED_TYPE);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, firebaseBundle);

    }

    public void logDownloadClicked() {
        Bundle firebaseBundle = new Bundle();
        firebaseBundle.putString(FirebaseAnalytics.Param.ITEM_ID, AnalyticsConstants.DOWNLOAD_CLICKED_ID);
        firebaseBundle.putString(FirebaseAnalytics.Param.ITEM_NAME, AnalyticsConstants.DOWNLOAD_CLICKED_NAME);
        firebaseBundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, AnalyticsConstants.DOWNLOAD_CLICKED_TYPE);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, firebaseBundle);
    }

    public void logChromecastStarting() {
        Bundle firebaseBundle = new Bundle();
        firebaseBundle.putString(FirebaseAnalytics.Param.ITEM_ID, AnalyticsConstants.CHROMECAST_CLICKED_ID);
        firebaseBundle.putString(FirebaseAnalytics.Param.ITEM_NAME, AnalyticsConstants.CHROMECAST_CLICKED_NAME);
        firebaseBundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, AnalyticsConstants.CHROMECAST_CLICKED_TYPE);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, firebaseBundle);
    }



}
