package com.shwifty.tex.activities;

// based on: https://bitbucket.org/edwardcw/libvlc-android-sample/src/ae8dd1ab984f645df459a5c44a62a271fc976d23/src/com/compdigitec/libvlcandroidsample/VideoActivity.java?at=master&fileviewer=file-view-default

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.RemoteController;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.MediaRouteChooserDialog;
import android.support.v7.media.MediaControlIntent;
import android.support.v7.media.MediaItemStatus;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaSessionStatus;
import android.support.v7.media.RemotePlaybackClient;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.shwifty.tex.R;
import com.shwifty.tex.dialogs.DownloadDialogFragment;
import com.shwifty.tex.dialogs.OpenSubtitlesDialog;
import com.shwifty.tex.handlers.CastHandler;
import com.shwifty.tex.handlers.ServerHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.handlers.VLCHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.ui.StrokedRobotoTextView;
import com.shwifty.tex.utilities.AnalyticsConstants;
import com.shwifty.tex.utilities.MimeConstants;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.Prefs;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import subtitleFile.Caption;
import subtitleFile.FormatSRT;
import subtitleFile.TimedTextObject;


public class VideoActivity extends AppCompatActivity implements IVLCVout.Callback {

    //veiws
    private ProgressBar bufferingSpinner;
    private ImageButton playButton;
    private ImageButton pauseButton;
    private SeekBar seekBar;
    private CoordinatorLayout mediaController;
    private FloatingActionsMenu addFAB;
    private FloatingActionButton subtitleFAB;
    private FloatingActionButton chromecastFAB;


    // display surface
    private SurfaceView mSurface;
    private SurfaceHolder holder;
    private View mContentView;

    // media player
    private int mVideoWidth;
    private int mVideoHeight;

    // variables
    private static boolean isPlaying = false;
    private static final int FADE_TIME = 200;
    private boolean uiVisible;
//    private boolean adShowing;
    private boolean notifiedPlaying = false;
    public static final String TAG_VIDEO_URL = "TAG_VIDEO_URL";
    public static final String TAG_MAGNET_STRING = "TAG_MAGNET_STRING";
    public static final String TAG_FORMAT_STRING = "TAG_FORMAT_STRING";
    @BindString(R.string.test_device_id)
    String testDeviceID;
    @BindString(R.string.stream_interstatial_ad)
    String interstatialID;
    private FirebaseAnalytics mFirebaseAnalytics;
    private static String magnet;
    private static String format;

    //subtitles
    private TimedTextObject tto;
    private Caption mLastSub = null;
    private Handler mDisplayHandler;
    @BindView(R.id.subtitle_text)
    StrokedRobotoTextView mSubtitleText;


    /*************
     * Activity
     *************/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);
        MainApplication.getInstance().getCastHandler().initializeCastContext(this);

        // Receive path to play from intent
        Intent intent = getIntent();
        String url = intent.getExtras().getString(TAG_VIDEO_URL);

        // bind views
        playButton = (ImageButton) findViewById(R.id.playButton);
        pauseButton = (ImageButton) findViewById(R.id.pauseButton);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        bufferingSpinner = (ProgressBar) findViewById(R.id.bufferingSpinner);
        mSurface = (SurfaceView) findViewById(R.id.surface);
        holder = mSurface.getHolder();
        addFAB = (FloatingActionsMenu) findViewById(R.id.fab_add);
        subtitleFAB = (FloatingActionButton) findViewById(R.id.fab_add_subtitles);
        chromecastFAB = (FloatingActionButton) findViewById(R.id.fab_chromecast);

        setupUi();

        magnet = intent.getExtras().getString(TAG_MAGNET_STRING);
        format = intent.getExtras().getString(TAG_FORMAT_STRING);

        if (savedInstanceState == null) {
//            setupAd();
            createNewPlayer(url);
        } else {
            bindPlayerToActivity();
        }


        LibVLC.setOnNativeCrashListener(() -> {
            MainApplication.getInstance().shutdown(getApplicationContext());
            RuntimeException e = new RuntimeException(getString(R.string.vlcCrash));
            FirebaseCrash.report(new Exception("VLC NATIVE CRASH"));
            throw e;
        });

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("activity_name", AnalyticsConstants.VIDEO_NAME);
        mFirebaseAnalytics.logEvent(AnalyticsConstants.START_VIDEO, params);

        mDisplayHandler = new Handler(Looper.getMainLooper());
    }


    private void setupUi() {
        mSubtitleText.setTextColor(Color.WHITE);
        mSubtitleText.setTextSize(16);
        mSubtitleText.setStrokeColor(Color.BLACK);
        mSubtitleText.setStrokeWidth(TypedValue.COMPLEX_UNIT_DIP, 2);


        playButton.setOnClickListener(v -> togglePlaying());
        pauseButton.setOnClickListener(v -> togglePlaying());

        mContentView = findViewById(R.id.fullscreen_content);

        mediaController = (CoordinatorLayout) findViewById(R.id.mediaController);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                float newPosition = (float) seekBar.getProgress() / 1000;
                VLCHandler.getMediaPlayer().setPosition(newPosition);
            }
        });
        seekBar.setMax(1000);

        if (Build.VERSION.SDK_INT >= 21) {
            // Set the status bar to dark-semi-transparentish
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        }

        subtitleFAB.setOnClickListener(view -> {
            if (doesContainSubtitles()) {
                showSelectSubtitlesDialog(magnet);
            } else {
                Toast.makeText(this, "this torrent does not contain any subtitles", Toast.LENGTH_LONG).show();
            }
        });

        chromecastFAB.setOnClickListener(view -> {
            if (canCastFromFormat()) {
                checkChromecastState();
            } else {
                Toast.makeText(this, "chromecast does not support this video format", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindPlayerToActivity();
        play();
        MainApplication.getInstance().getCastHandler().addSessionListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindPlayerFromActivity();
        MainApplication.getInstance().getCastHandler().removeSessionListener();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            releasePlayer();
        } else {
            unbindPlayerFromActivity();
        }
    }


    @Override
    public void onNewLayout(IVLCVout vout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0)
            return;

        // store video size
        mVideoWidth = width;
        mVideoHeight = height;
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vout) {

    }

    @Override
    public void onHardwareAccelerationError(IVLCVout vlcVout) {
        this.releasePlayer();
        Toast.makeText(this, getString(R.string.acceleration_error_toast), Toast.LENGTH_LONG).show();
    }

    /*************
     * Surface
     *************/
    private void setSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1)
            return;

        if (holder == null || mSurface == null)
            return;

        // get screen size
        int w = getWindow().getDecorView().getWidth();
        int h = getWindow().getDecorView().getHeight();

//         getWindow().getDecorView() doesn't always take orientation into
//         account, we have to correct the values
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (w > h && isPortrait || w < h && !isPortrait) {
            int i = w;
            w = h;
            h = i;
        }

        float videoAR = (float) mVideoWidth / (float) mVideoHeight;
        float screenAR = (float) w / (float) h;

        if (screenAR < videoAR)
            h = (int) (w / videoAR);
        else
            w = (int) (h * videoAR);

        // force surface buffer size
//        holder.setFixedSize(mVideoWidth, mVideoHeight);
        holder.setFixedSize(20, 20);

        // set display size
        ViewGroup.LayoutParams lp = mSurface.getLayoutParams();

        //TODO add check for multiwindow mode


        lp.width = w;
        lp.height = h;
        mSurface.setLayoutParams(lp);
        mSurface.invalidate();
    }

    /*************
     * Player
     *************/
    private void createNewPlayer(String media) {
        bufferingSpinner.setVisibility(View.VISIBLE);
        releasePlayer();
        try {
            // Create LibVLC
            // TODO: make this more robust, and sync with audio demo
            ArrayList<String> options = new ArrayList<>();
            //options.add("--subsdec-encoding <encoding>");
            options.add("--aout=opensles");
            options.add("--audio-time-stretch"); // time stretching
//            options.add("-vvv"); // verbosity
            VLCHandler.setLibVLC(new LibVLC(options));

            // Create media player
            VLCHandler.setMediaPlayer(new MediaPlayer(VLCHandler.getLibVLC()));
            Media m = new Media(VLCHandler.getLibVLC(), Uri.parse(media));
            VLCHandler.getMediaPlayer().setMedia(m);

        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.create_player_error_toast), Toast.LENGTH_LONG).show();
        }
    }


    private void bindPlayerToActivity() {
        if (VLCHandler.isBoundToActivity()) return;
        VLCHandler.getMediaPlayer().setEventListener(mPlayerListener);

        if (holder == null) {
            //caused by screen locking - when release player is called and isn't followed by onCreate
            holder = mSurface.getHolder();
        }
        holder.setKeepScreenOn(true);


        // Set up video output
        final IVLCVout vout = VLCHandler.getMediaPlayer().getVLCVout();
        vout.setVideoView(mSurface);
        //TODO implement subtitles
        //vout.setSubtitlesView(mSurfaceSubtitles);
        vout.addCallback(this);
        vout.attachViews();

        VLCHandler.setBoundToActivity(true);

        isPlaying = false;
        togglePlaying();
    }

    private void unbindPlayerFromActivity() {
        if (!VLCHandler.isBoundToActivity()) return;
        if (VLCHandler.getLibVLC() == null) return;
        VLCHandler.savePosition();
        VLCHandler.setPlaying(isPlaying);
        VLCHandler.getMediaPlayer().stop();
        final IVLCVout vout = VLCHandler.getMediaPlayer().getVLCVout();
        vout.removeCallback(this);
        vout.detachViews();
        holder = null;
        VLCHandler.getLibVLC().release();

        mVideoWidth = 0;
        mVideoHeight = 0;
        VLCHandler.setBoundToActivity(false);
    }

    // TODO: handle this cleaner
    private void releasePlayer() {
        if (VLCHandler.getLibVLC() == null) return;

        unbindPlayerFromActivity();
        VLCHandler.setLibVLC(null);
        VLCHandler.setMediaPlayer(null);
        VLCHandler.clearLastPosition();
        VLCHandler.setPlaying(true);
    }

    /*************
     * Events
     *************/

    private MediaPlayer.EventListener mPlayerListener = new MyPlayerListener(this);

    public void notifyPlaying() {
        if (notifiedPlaying) return;
        runOnUiThread(() -> bufferingSpinner.setVisibility(View.GONE));
        mContentView.setOnClickListener(view -> toggleUIVisibility());

        if (!VLCHandler.isPlaying()) {
            pause();
        }

        VLCHandler.getMediaPlayer().setPosition(VLCHandler.getLastPosition());
        seekBar.setProgress(Math.round(1000 * VLCHandler.getLastPosition()));
        notifiedPlaying = true;
    }

    private static class MyPlayerListener implements MediaPlayer.EventListener {
        private WeakReference<VideoActivity> mOwner;

        MyPlayerListener(VideoActivity owner) {
            mOwner = new WeakReference<>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            VideoActivity player = mOwner.get();

            switch (event.type) {
                case MediaPlayer.Event.EndReached:
                    player.releasePlayer();
                    player.finish();
                    Intent mainIntent = new Intent(player, SplashActivity.class);
                    player.startActivity(mainIntent);
                    player.finish();
                    break;
                case MediaPlayer.Event.Playing:
                    player.notifyPlaying();
                    break;
                case MediaPlayer.Event.Paused:
                case MediaPlayer.Event.Stopped:
                case MediaPlayer.Event.PositionChanged:
                    player.progressSubtitleCaption();
                    player.updateSeekBar();
                default:
                    break;
            }
        }
    }

    private void togglePlaying() {
        if (isPlaying) {
            pause();
        } else {
            play();
        }
    }

    private void play() {
        if (VLCHandler.getMediaPlayer() == null) return;
        showPause();
        VLCHandler.getMediaPlayer().play();
        isPlaying = true;
    }

    private void pause() {
        if (VLCHandler.getMediaPlayer() == null) return;
        hidePause();
        VLCHandler.getMediaPlayer().pause();
        isPlaying = false;
    }

    public void showPause() {
        AlphaAnimation fade_in = new AlphaAnimation(0.0f, 1.0f);
        fade_in.setDuration(FADE_TIME);
        fade_in.setAnimationListener(new ShowingAnimationListener(pauseButton));


        AlphaAnimation fade_out = new AlphaAnimation(1.0f, 0.0f);
        fade_out.setDuration(FADE_TIME);
        fade_out.setAnimationListener(new HidingAnimationListener(playButton));

        pauseButton.startAnimation(fade_in);
        playButton.startAnimation(fade_out);
    }

    public void hidePause() {
        AlphaAnimation fade_in = new AlphaAnimation(0.0f, 1.0f);
        fade_in.setDuration(FADE_TIME);
        AlphaAnimation fade_out = new AlphaAnimation(1.0f, 0.0f);
        fade_out.setDuration(FADE_TIME);

        fade_in.setAnimationListener(new ShowingAnimationListener(playButton));
        fade_out.setAnimationListener(new HidingAnimationListener(pauseButton));

        playButton.startAnimation(fade_in);
        pauseButton.startAnimation(fade_out);
    }

    private class HidingAnimationListener implements Animation.AnimationListener {
        private ImageButton mImgButton;

        HidingAnimationListener(ImageButton imgButton) {
            mImgButton = imgButton;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mImgButton.setVisibility(View.INVISIBLE);
            mImgButton.setClickable(false);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto - generated method stub

        }

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto - generated method stub

        }

    }

    private class ShowingAnimationListener implements Animation.AnimationListener {
        private ImageButton mImgButton;

        ShowingAnimationListener(ImageButton imgButton) {
            mImgButton = imgButton;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mImgButton.setVisibility(View.VISIBLE);
            mImgButton.setClickable(true);

        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto - generated method stub

        }

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto - generated method stub

        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideUI();
        }
    }

    private void toggleUIVisibility() {
        if (uiVisible) {
            hideUI();
        } else {
            showUI();
        }
    }

    private void hideUI() {
        uiVisible = false;
        mediaController.setVisibility(View.GONE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void showUI() {
        uiVisible = true;
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        mediaController.setVisibility(View.VISIBLE);
    }

    protected void updateSeekBar() {
        if (VLCHandler.getMediaPlayer() == null || VLCHandler.getLibVLC() == null) return;
        float fPosition = VLCHandler.getMediaPlayer().getPosition();
        int position = Math.round(1000 * fPosition);
        runOnUiThread(() -> seekBar.setProgress(position));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mainIntent = new Intent(this, SplashActivity.class);
        this.startActivity(mainIntent);
        this.finish();
    }

    public void notifySubtitleFileSelected(TorrentFile torrentFile) {
        String path = Prefs.downloadPath + File.separator + torrentFile.file.getPath();
        File subsFile = new File(path);
        if (subsFile.exists()) {
            loadSubtitltes(subsFile);
        } else {
            downloadSubtitleFiles(torrentFile);
        }

    }

    private void downloadSubtitleFiles(TorrentFile tf) {
        int fileIndex = (int) tf.index;
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent torrent = torrentHandler.getTorrentFromMagnet(magnet);
        MainApplication mainApplication = MainApplication.getInstance();
        if (mainApplication.isPaused()) return;

        TorrentFile subtitleFile = torrent.getTorrentFiles().get(fileIndex);


        Thread thread = new Thread(() -> {
            byte[] bytes = mainApplication.readBytes(torrent.getTorrentId(), fileIndex, 0, subtitleFile.file.getLength());

            File outputFile = new File(Prefs.downloadPath + File.separator + subtitleFile.file.getPath());
            BufferedOutputStream bos = null;
            try {
                bos = new BufferedOutputStream(new FileOutputStream(outputFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                bos.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            loadSubtitltes(outputFile);

//            getActivity().runOnUiThread(()->{
//                if(getActivity() instanceof VideoActivity){
//                    VLCHandler.setSubtitltesPath(outputFile.getPath());
//                    VideoActivity videoActivity = (VideoActivity) getActivity();
//                    videoActivity.notifySubtitlesAdded();
//                }
//            });
        });
        thread.start();
    }

    public void showSelectSubtitlesDialog(String magnetString) {
        String TAG_FILES_DIALOG = "OenSubtitlesDialog";
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(TAG_FILES_DIALOG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        OpenSubtitlesDialog newFragment = OpenSubtitlesDialog.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(DownloadDialogFragment.TAG_MAGNET_ARG, magnetString);
        newFragment.setArguments(bundle);

        newFragment.show(ft, TAG_FILES_DIALOG);
        pause();
    }

    private void loadSubtitltes(File subsFile) {
        FormatSRT formatSRT = new FormatSRT();
        try {
            FileInputStream fis = new FileInputStream(subsFile);
            tto = formatSRT.parseFile("subs", fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        runOnUiThread(this::play);
    }

    private float getCurrentTime() {
        return VLCHandler.getMediaPlayer().getTime();
    }

    protected void progressSubtitleCaption() {
        if (VLCHandler.getLibVLC() != null && VLCHandler.getMediaPlayer() != null && VLCHandler.getMediaPlayer().isPlaying() && tto != null) {
            Collection<Caption> subtitles = tto.captions.values();
            double currentTime = getCurrentTime(); // - mSubtitleOffset;
            if (mLastSub != null && currentTime >= mLastSub.start.getMseconds() && currentTime <= mLastSub.end.getMseconds()) {
                showTimedCaptionText(mLastSub);
            } else {
                for (Caption caption : subtitles) {
                    if (currentTime >= caption.start.getMseconds() && currentTime <= caption.end.getMseconds()) {
                        mLastSub = caption;
                        showTimedCaptionText(caption);
                        break;
                    } else if (currentTime > caption.end.getMseconds()) {
                        showTimedCaptionText(null);
                    }
                }
            }
        }
    }

    private void showTimedCaptionText(Caption text) {
        mDisplayHandler.post(() -> {
            if (text == null) {
                if (mSubtitleText.getText().length() > 0) {
                    mSubtitleText.setText("");
                }
                return;
            }
            SpannableStringBuilder styledString = fromHtml(text.content);

            ForegroundColorSpan[] toRemoveSpans = styledString.getSpans(0, styledString.length(), ForegroundColorSpan.class);
            for (ForegroundColorSpan remove : toRemoveSpans) {
                styledString.removeSpan(remove);
            }

            if (!mSubtitleText.getText().toString().equals(styledString.toString())) {
                mSubtitleText.setText(styledString);
            }
        });
    }

    @SuppressWarnings("deprecation")
    public static SpannableStringBuilder fromHtml(String html) {
        SpannableStringBuilder result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = (SpannableStringBuilder) Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = (SpannableStringBuilder) Html.fromHtml(html);
        }
        return result;
    }

    private boolean doesContainSubtitles() {
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent torrent = torrentHandler.getTorrentFromMagnet(magnet);

        boolean doesContain = false;

        for (int z = 0; z < torrent.getTorrentFiles().size(); z++) {
            TorrentFile torrentFile = torrent.getTorrentFiles().get(z);
            for (String format : MimeConstants.subtitleSet) {
                if (torrentFile.file.getPath().endsWith(format)) {
                    doesContain = true;
                }
            }
        }

        return doesContain;

    }

    private boolean canCastFromFormat() {
        if (format.equalsIgnoreCase("x-matroska")) {
            format = "webm";
        }
        if (MimeConstants.chromecastSet.contains(format)) {
            return true;
        } else {
            return false;
        }
    }

    private void checkChromecastState() {
        CastHandler castHandler = new CastHandler();
        pause();
        if (castHandler.isConnected()) {
            startChromecastInCurrentSession();
        } else {

            handleCastButton();
        }
    }

    private void startChromecastInCurrentSession() {
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent torrent = torrentHandler.getTorrentFromMagnet(magnet);
        int index = -1;

        for (TorrentFile torrentFile : torrent.getTorrentFiles()) {
            if (torrentFile.file.getCheck()) {
                index = (int) torrentFile.index;
                break;
            }
        }
        if (index > -1) {
            MainApplication.getInstance().startForStream(torrent);
            TorrentFile selectedTorrentFile = torrent.getTorrentFiles().get(index);

            if (selectedTorrentFile != null) {
                File selectedFile = new File(selectedTorrentFile.file.getPath());
                String ip = NetworkUtil.getIPAddress(true);

                String myURL = "http://" + ip + "/torrentID/" + torrent.getTorrentId() + "/torrentFileIndex/" + selectedTorrentFile.index + "/" + selectedFile.getPath();
                URL streamUrl = null;


                try {
                    URL url = new URL(myURL);
                    URI uri = new URI("http", null, url.getHost(), ServerHandler.streamPort, url.getPath(), url.getQuery(), null);
                    System.out.println("URI " + uri.toString() + " is OK");
                    streamUrl = uri.toURL();
                    System.out.println("URL " + streamUrl.toString());
                } catch (MalformedURLException e) {
                    System.out.println("URL " + myURL + " is a malformed URL");
                } catch (URISyntaxException e) {
                    System.out.println("URI " + myURL + " is a malformed URL");
                }

                String urlString = streamUrl.toString();

                MainApplication.getInstance().getCastHandler().loadRemoteMedia(torrent, urlString, "video/" + format);
            }
        }
        finish();
    }

    private void handleCastButton() {
        MediaRouteSelector selector = new MediaRouteSelector.Builder()
                .addControlCategory(MediaControlIntent.CATEGORY_REMOTE_PLAYBACK)
                .build();


        MyCallback callback = new MyCallback(this);
        MediaRouter router = MediaRouter.getInstance(this);
        router.addCallback(selector, callback, MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
        MediaRouteChooserDialog dialog = new MediaRouteChooserDialog(this);
        dialog.setRouteSelector(selector);
        dialog.show();
    }

    private class MyCallback extends MediaRouter.Callback {
        private MediaRouter.RouteInfo mRoute;
        private RemotePlaybackClient mRemotePlaybackClient;
        private Context context;

        public MyCallback(Context context) {
            this.context = context;
        }

        @Override
        public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo route) {
            Log.d("ARRANZLZ", "onRouteSelected: route=" + route);

            if (route.supportsControlCategory(
                    MediaControlIntent.CATEGORY_REMOTE_PLAYBACK)) {
                finish();
//                startChromecastInCurrentSession();
//
//                mRoute = route;
//
//                mRemotePlaybackClient = new RemotePlaybackClient(context, mRoute);
//                mRemotePlaybackClient.startSession(null, new MySessionActionCallback());

            }
        }


//                TorrentHandler torrentHandler = new TorrentHandler();
//                Torrent torrent = torrentHandler.getTorrentFromMagnet(magnet);
//                int index = -1;
//
//                for (TorrentFile torrentFile : torrent.getTorrentFiles()) {
//                    if (torrentFile.file.getCheck()) {
//                        index = (int) torrentFile.index;
//                        break;
//                    }
//                }
//                if (index > -1) {
//                    MainApplication.getInstance().startForStream(torrent);
//                    TorrentFile selectedTorrentFile = torrent.getTorrentFiles().get(index);
//
//                    if (selectedTorrentFile != null) {
//                        File selectedFile = new File(selectedTorrentFile.file.getPath());
//                        String ip = NetworkUtil.getIPAddress(true);
//
//                        String myURL = "http://" + ip + "/torrentID/" + torrent.getTorrentId() + "/torrentFileIndex/" + selectedTorrentFile.index + "/" + selectedFile.getPath();
//
//
//                        URI uri = null;
//                        try {
//                            URL url = new URL(myURL);
//                            uri = new URI("http", null, url.getHost(), ServerHandler.streamPort, url.getPath(), url.getQuery(), null);
//                        } catch (MalformedURLException e) {
//                            System.out.println("URL " + myURL + " is a malformed URL");
//                        } catch (URISyntaxException e) {
//                            System.out.println("URI " + myURL + " is a malformed URL");
//                        }
//
//                        Uri uri1 = Uri.parse(uri.toString());
//                        Bundle metaData = new Bundle();
//                        Bundle extras = new Bundle();
//                        MyItemCallback myItemCallback = new MyItemCallback();
//
//
//
//                        if(VLCHandler.getMediaPlayer()!=null){
//                            long position = (long) getCurrentTime();
//                            mRemotePlaybackClient.play(uri1, "video/" + format, metaData, position, extras, myItemCallback);
//                        }else{
//                            mRemotePlaybackClient.play(uri1, "video/" + format, metaData, 0, extras, myItemCallback);
//                        }
//                    }
//                }
//            }
//        }

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo route, int reason) {
            Log.d("ARRANZLZ", "onRouteUnselected: route=" + route);

            if (route.supportsControlCategory(
                    MediaControlIntent.CATEGORY_REMOTE_PLAYBACK)) {

                // Changed route: tear down previous client
                if (mRoute != null && mRemotePlaybackClient != null) {
                    mRemotePlaybackClient.release();
                    mRemotePlaybackClient = null;
                }

                // Save the new route
                mRoute = route;

                if (reason != MediaRouter.UNSELECT_REASON_ROUTE_CHANGED) {
                    // Resume local playback  (if necessary)
                    // ...
                }
            }
        }
    }

//    private class MyItemCallback extends RemotePlaybackClient.ItemActionCallback{
//        public MyItemCallback() {
//            super();
//        }
//
//        @Override
//        public void onResult(Bundle data, String sessionId, MediaSessionStatus sessionStatus, String itemId, MediaItemStatus itemStatus) {
//            super.onResult(data, sessionId, sessionStatus, itemId, itemStatus);
//        }
//    }
//
//
//    private class MySessionActionCallback extends RemotePlaybackClient.SessionActionCallback{
//        @Override
//        public void onResult(Bundle data, String sessionId, MediaSessionStatus sessionStatus) {
//            super.onResult(data, sessionId, sessionStatus);
//        }
//
//        public MySessionActionCallback() {
//            super();
//        }
//    }

    public static void notifySessionStarted(){
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent torrent = torrentHandler.getTorrentFromMagnet(magnet);
        int index = -1;

        for (TorrentFile torrentFile : torrent.getTorrentFiles()) {
            if (torrentFile.file.getCheck()) {
                index = (int) torrentFile.index;
                break;
            }
        }
        if (index > -1) {
            MainApplication.getInstance().startForStream(torrent);
            TorrentFile selectedTorrentFile = torrent.getTorrentFiles().get(index);

            if (selectedTorrentFile != null) {
                File selectedFile = new File(selectedTorrentFile.file.getPath());
                String ip = NetworkUtil.getIPAddress(true);

                String myURL = "http://" + ip + "/torrentID/" + torrent.getTorrentId() + "/torrentFileIndex/" + selectedTorrentFile.index + "/" + selectedFile.getPath();
                URL streamUrl = null;


                try {
                    URL url = new URL(myURL);
                    URI uri = new URI("http", null, url.getHost(), ServerHandler.streamPort, url.getPath(), url.getQuery(), null);
                    System.out.println("URI " + uri.toString() + " is OK");
                    streamUrl = uri.toURL();
                    System.out.println("URL " + streamUrl.toString());
                } catch (MalformedURLException e) {
                    System.out.println("URL " + myURL + " is a malformed URL");
                } catch (URISyntaxException e) {
                    System.out.println("URI " + myURL + " is a malformed URL");
                }

                String urlString = streamUrl.toString();

                MainApplication.getInstance().getCastHandler().loadRemoteMedia(torrent, urlString, "video/" + format);
            }
        }
    }

}