package com.shwifty.tex.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.fragments.SearchFragment;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.Constants;
import com.shwifty.tex.utilities.jpa.TPBTorrent;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.CardHolder> {
    private String TAG = getClass().getName();
    private ArrayList<TPBTorrent> data = new ArrayList<>();
    private Context context;
    private SearchFragment fragment;
    private boolean waitingForFilesRunning;
    private final TorrentHandler torrentHandler = new TorrentHandler();
    private String selectedMagnet;
    private int selectedPosition;
    // download = 0
    // stream = 1


    public SearchAdapter(SearchFragment fragment) {
        this.data = new ArrayList<>();
        this.fragment = fragment;
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View parentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_search, parent, false);
        context = parentView.getContext();
        return new CardHolder(parentView);
    }

    @Override
    public void onBindViewHolder(final CardHolder cardHolder, final int position) {
        fragment.stopRefreshing();
        cardHolder.torrentName.setText(data.get(position).Name);
        String seeds = context.getString(R.string.seeds) + Integer.toString(data.get(position).Seeds);
        cardHolder.seeders.setText(seeds);
        String leeches = context.getString(R.string.leechers) + Integer.toString(data.get(position).Leechers);
        cardHolder.leechers.setText(leeches);
        cardHolder.parentCategory.setText(data.get(position).CategoryParent);
        cardHolder.category.setText(data.get(position).Category);
        String size = context.getString(R.string.size) + data.get(position).Size;
        cardHolder.size.setText(size);
        String date = context.getString(R.string.date) + data.get(position).Uploaded;
        cardHolder.date.setText(date);
        cardHolder.uploader.setText(data.get(position).Uled);
        cardHolder.magnet.setText(data.get(position).Magnet);
        cardHolder.torrentLink.setText(data.get(position).Link);

        if (data.get(position).blackoutShowing) {
            showBlackoutButtons(cardHolder, position);
        } else {
            hideBlackoutButtons(cardHolder, position);
        }


        final String magnetString = data.get(position).Magnet;


        loadImage(position, cardHolder);

        cardHolder.itemView.setOnClickListener(view -> {
            MainApplication mainApplication = MainApplication.getInstance();
            if(!mainApplication.holdHintShown){
//                Toast.makeText(context, "Press and Hold for more options", Toast.LENGTH_SHORT).show();
                mainApplication.holdHintShown = true;
            }

            if (!torrentHandler.isTorrentCreatedMagnet(magnetString)) {
                createTorrent(magnetString, context, data.get(position));
            }
            showBlackoutButtons(cardHolder, position);
        });

        cardHolder.downloadButton.setOnClickListener(view -> downloadClicked(magnetString, position));

        cardHolder.streamButton.setOnClickListener(view -> streamClicked(magnetString, position));

        cardHolder.itemView.setOnLongClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

            PopupMenu popupMenu = new PopupMenu(context, cardHolder.torrentName);
            if (data.get(position).ImdbID != null) {
                popupMenu.inflate(R.menu.imdb_menu);
            }
            else {
                popupMenu.inflate(R.menu.default_menu);
            }

            popupMenu.setOnMenuItemClickListener(item -> {
                String url = null;

                if (item.getItemId() == R.id.imdb_open) {
                    url = "http://imdb.com/title/" + data.get(position).ImdbID;
                }
                else if (item.getItemId() == R.id.piratebay_open) {
                    url = Constants.getFastestUrl() + data.get(position).Link;
                }

                try {
                    if (context != null) {
                        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                                .setToolbarColor(Color.parseColor("#212121"))
                                .build();
                        Activity activity = (Activity)context;
                        customTabsIntent.launchUrl(activity, Uri.parse(url));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            });

            popupMenu.show();

            return true;
        });

        cardHolder.blackout.setOnClickListener(view -> hideBlackoutButtons(cardHolder, position));

        cardHolder.blackout.setOnLongClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

            PopupMenu popupMenu = new PopupMenu(context, cardHolder.torrentName);
            if (data.get(position).ImdbID != null) {
                popupMenu.inflate(R.menu.imdb_menu);
            }
            else {
                popupMenu.inflate(R.menu.default_menu);
            }

            popupMenu.setOnMenuItemClickListener(item -> {
                String url = null;

                if (item.getItemId() == R.id.imdb_open) {
                    url = "http://imdb.com/title/" + data.get(position).ImdbID;
                }
                else if (item.getItemId() == R.id.piratebay_open) {
                    url = Constants.getFastestUrl() + data.get(position).Link;
                }

                try {
                    if (context != null) {
                        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                                .setToolbarColor(Color.parseColor("#212121"))
                                .build();
                        Activity activity = (Activity)context;
                        customTabsIntent.launchUrl(activity, Uri.parse(url));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            });

            popupMenu.show();

            return true;
        });
    }


    public void stopLoadingFiles() {
        waitingForFilesRunning = false;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    static class CardHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.torrentName)
        TextView torrentName;
        @BindView(R.id.seeds)
        TextView seeders;
        @BindView(R.id.leechers)
        TextView leechers;
        @BindView(R.id.uploader)
        TextView uploader;
        @BindView(R.id.parentCategory)
        TextView parentCategory;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.size)
        TextView size;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.magnet)
        TextView magnet;
        @BindView(R.id.torrentLink)
        TextView torrentLink;
        @BindView(R.id.coverImage)
        ImageView coverImage;
        @BindView(R.id.downloadButton)
        FloatingActionButton downloadButton;
        @BindView(R.id.streamButton)
        FloatingActionButton streamButton;
        @BindView(R.id.blackout)
        View blackout;
        @BindView(R.id.centre_line_download)
        View centreLine;
        @BindView(R.id.search_hold_hint)
        TextView holdHint;

        CardHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }


    }

    public void setItems(ArrayList<TPBTorrent> items) {
        this.data.clear();
        this.data.addAll(items);
        Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(this::notifyDataSetChanged);
    }

    private void loadImage(int position, CardHolder cardHolder) {
        if (data.get(position).CoverImage != null) {

            Picasso.with(context.getApplicationContext())
                    .load(data.get(position).CoverImage)
                    .into(cardHolder.coverImage);

        } else {
            cardHolder.coverImage.setImageDrawable(null);
        }

    }

    public void downloadClicked(String magnetString, int position) {
        selectedMagnet = magnetString;
        selectedPosition = position;
        boolean hasPermission;
        MainActivity mainActivity = (MainActivity) context;

        //FIRST CHECK IF HAS STORAGE PERMISSION
        if (mainActivity == null) return;
        hasPermission = mainActivity.hasPermission();
        mainActivity.logDownloadClicked();

        if (!hasPermission) {
            mainActivity.setPermissionRequestFrom(MainActivity.DOWNLOAD_CLICK_ID);
            mainActivity.requestPermission();
        }

        //THEN CHECK IF CONNECTED
        if (NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED) {
            mainActivity.showNoNetworkNetworkDialog();
            return;
        }

        //THEN CHECK IF THE CONNECTION TYPE AND THE USER SETTINGS ALLOW FOR DOWNLOAD
        if (!NetworkUtil.getOKForDownload(context)){
            mainActivity.showDownloadOnMobileDataDialog();
            return;
        }

        //THEN CHECK IF ALREADY ADDED
        DownloadHandler downloadHandler = new DownloadHandler(context);
        Torrent torrent = torrentHandler.getTorrentFromMagnet(magnetString);

        if(torrent!=null){
            boolean isAdded = downloadHandler.isTorrentAdded(torrent);
            if (isAdded) {
                mainActivity.showTorrentAlreadyDownloadingAlert(torrent.getMagnetLink(), false);
                return;
            }
        }



        if (!torrentHandler.isTorrentCreatedMagnet(magnetString)) {
            createTorrent(magnetString, context, data.get(position));
        }
        if (torrentHandler.isTorrentCreatedMagnet(magnetString)) {
            mainActivity.showDownloadFilesDialog(magnetString);
        } else {
            showLoading();
            Thread thread = new Thread(() -> {
                waitingForFilesRunning = true;
                while (waitingForFilesRunning) {
                    if (torrentHandler.isTorrentCreatedMagnet(magnetString)) {
                        waitingForFilesRunning = false;
                        mainActivity.showDownloadFilesDialog(magnetString);
                        mainActivity.hideProgressDialog();
                    }
                }
            });
            thread.start();
        }
    }

    public void streamClicked(String magnetString, int position) {
        selectedMagnet = magnetString;
        selectedPosition = position;
        boolean hasPermission;
        MainActivity mainActivity = (MainActivity) context;

        //FIRST CHECK HAS PERMISSION
        if (mainActivity == null) return;
        mainActivity.logStreamClicked();
        hasPermission = mainActivity.hasPermission();

        if (!hasPermission) {
            mainActivity.setPermissionRequestFrom(MainActivity.STREAM_CLICK_ID);
            mainActivity.requestPermission();
        }

        //THEN CHECK IF CONNECTED
        if (NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED) {
            mainActivity.showNoNetworkNetworkDialog();
            return;
        }

        //THEN CHECK IF THE CONNECTION TYPE AND THE USER SETTINGS ALLOW FOR DOWNLOAD
        if (!NetworkUtil.getOKForDownload(context)){
            mainActivity.showDownloadOnMobileDataDialog();
            return;
        }

        //THEN CHECK IF ALREADY ADDED
        DownloadHandler downloadHandler = new DownloadHandler(context);
        Torrent torrent = torrentHandler.getTorrentFromMagnet(magnetString);
        if(torrent!=null){
            boolean isAdded = downloadHandler.isTorrentAdded(torrent);
            if (isAdded) {
                mainActivity.showTorrentAlreadyDownloadingAlert(torrent.getMagnetLink(), true);
                return;
            }
        }

        if (torrentHandler.isTorrentCreatedMagnet(magnetString)) {
            mainActivity.showStreamFilesDialog(magnetString);
        } else {
            createTorrent(magnetString, context, data.get(position));
            showLoading();
            Thread thread = new Thread(() -> {
                waitingForFilesRunning = true;
                while (waitingForFilesRunning) {
                    if (torrentHandler.isTorrentCreatedMagnet(magnetString)) {
                        waitingForFilesRunning = false;
                        mainActivity.showStreamFilesDialog(magnetString);
                        mainActivity.hideProgressDialog();
                    }
                }
            });
            thread.start();

        }


    }

    private void createTorrent(final String magnetString, final Context mContext, TPBTorrent tpbTorrent) {
        try {
            TorrentHandler torrentHandler = new TorrentHandler();
            torrentHandler.addFromMagnet(magnetString, mContext, tpbTorrent);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showBlackoutButtons(CardHolder cardHolder, int position) {
        cardHolder.blackout.setVisibility(View.VISIBLE);
        cardHolder.streamButton.setVisibility(View.VISIBLE);
        cardHolder.downloadButton.setVisibility(View.VISIBLE);
        cardHolder.centreLine.setVisibility(View.VISIBLE);
        cardHolder.holdHint.setVisibility(View.VISIBLE);
        data.get(position).blackoutShowing = true;
    }

    private void hideBlackoutButtons(CardHolder cardHolder, int position) {
        cardHolder.blackout.setVisibility(View.GONE);
        cardHolder.streamButton.setVisibility(View.GONE);
        cardHolder.downloadButton.setVisibility(View.GONE);
        cardHolder.centreLine.setVisibility(View.GONE);
        cardHolder.holdHint.setVisibility(View.GONE);
        data.get(position).blackoutShowing = false;
    }

    private void showLoading() {
        MainActivity mainActivity = (MainActivity) context;
        mainActivity.showFetchingFilesDialog();
    }

    public String getSelectedMagnet() {
        return selectedMagnet;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }
}
