package com.shwifty.tex.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.dialogs.DeleteTorrentAlert;
import com.shwifty.tex.dialogs.SelectFileDialog;
import com.shwifty.tex.fragments.TrackersFragment;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.NetworkUtil;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadManagerAdapter extends RecyclerView.Adapter<DownloadManagerAdapter.CardHolder> {
    private ArrayList<Torrent> torrents = new ArrayList<>();
    private Context context;


    public DownloadManagerAdapter(Context context) {
        this.context = context;

    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View parentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_download, parent, false);
        context = parentView.getContext();
        return new CardHolder(parentView);
    }

    @Override
    public void onBindViewHolder(final CardHolder cardHolder, final int position) {
        cardHolder.torrentName.setText(torrents.get(position).getName());
        if (torrents.get(position).getSeeds() < 0) {
            cardHolder.seeders.setText(R.string.unknown_seeds);
        } else {
            String seeders = context.getString(R.string.seeds) + Integer.toString(torrents.get(position).getSeeds());
            cardHolder.seeders.setText(seeders);
        }

        cardHolder.directory.setText(torrents.get(position).getDirectory());
        cardHolder.magnet.setText(torrents.get(position).getMagnetLink());
        String size = context.getString(R.string.size) + torrents.get(position).getSelectedFilesSize();
        cardHolder.size.setText(size);
        cardHolder.speed.setText(torrents.get(position).getSpeedString());
        cardHolder.progressBar.setMax(100);
        cardHolder.progressBar.setProgress(torrents.get(position).getPercCompleted());
        if (torrents.get(position).showingblackout) {
            showBlackoutButtons(cardHolder, position);
        } else {
            hideBlackoutButtons(cardHolder, position);
        }
        loadImage(position, cardHolder);

        cardHolder.itemView.setOnClickListener(view -> {
            showBlackoutButtons(cardHolder, position);
            notifyDataSetChanged();
        });

        cardHolder.itemView.setOnLongClickListener(view -> {
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

            if(context instanceof MainActivity){
                MainActivity mainActivity = (MainActivity) context;
                mainActivity.showTrackersDialog(torrents.get(position).getTorrentId());
            }
            return  true;
        });

        cardHolder.blackout.setOnLongClickListener(view -> {
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

            if(context instanceof MainActivity){
                MainActivity mainActivity = (MainActivity) context;
                mainActivity.showTrackersDialog(torrents.get(position).getTorrentId());
            }
            return  true;
        });

        cardHolder.blackout.setOnClickListener(view -> hideBlackoutButtons(cardHolder, position));

        cardHolder.openButton.setOnClickListener(view -> {

            Torrent torrent = torrents.get(position);
            String path = torrent.getDirectory() + File.separator + torrent.getName();
            File file = new File(path);
            if (!file.exists()) {
                File directory = new File(path);
                if (!directory.exists()) {
                    if (context instanceof MainActivity) {
                        MainActivity mainActivity = (MainActivity) context;
                        mainActivity.showCouldntLocateDialog(position);
                    }
                    return;
                }
            }

            final SelectFileDialog f = new SelectFileDialog(context);
            f.setCurrentPath(file);
            f.show();
        });

        cardHolder.pausebutton.setOnClickListener(view -> {
            MainApplication.getInstance().stopTorrent(context, torrents.get(position));
            if (torrents.get(position).showingblackout) {
                cardHolder.pausebutton.setVisibility(View.INVISIBLE);
                cardHolder.pausebutton.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
            }
        });

        cardHolder.restartButton.setOnClickListener(view -> {
            if (torrents.get(position).getSpeedString().equalsIgnoreCase("completed")) {
                return;
            }

            MainActivity mainActivity = null;
            if (context instanceof MainActivity) {
                mainActivity = (MainActivity) context;
            }

            if (mainActivity == null) return;

            //THEN CHECK IF CONNECTED
            if (NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED) {
                mainActivity.showNoNetworkNetworkDialog();
                return;
            }

            //THEN CHECK IF THE CONNECTION TYPE AND THE USER SETTINGS ALLOW FOR DOWNLOAD
            if (!NetworkUtil.getOKForDownload(context)) {
                mainActivity.showDownloadOnMobileDataDialog();
                return;
            }

            MainApplication.getInstance().startTorrent(context, torrents.get(position));
            if (torrents.get(position).showingblackout) {
                cardHolder.pausebutton.setVisibility(View.VISIBLE);
                cardHolder.pausebutton.setVisibility(View.INVISIBLE);
                notifyDataSetChanged();
            }


        });

        cardHolder.deleteButton.setOnClickListener(view -> showDeleteTorrentAlert(position));

    }


    @Override
    public int getItemCount() {
        return torrents == null ? 0 : torrents.size();
    }

    static class CardHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.torrentName)
        TextView torrentName;
        @BindView(R.id.seeds)
        TextView seeders;
        @BindView(R.id.directory)
        TextView directory;
        @BindView(R.id.size)
        TextView size;
        @BindView(R.id.magnet)
        TextView magnet;
        @BindView(R.id.speed)
        TextView speed;
        @BindView(R.id.coverImage)
        ImageView coverImage;
        @BindView(R.id.blackout)
        View blackout;
        @BindView(R.id.centre_line_download)
        View centreLine;
        @BindView(R.id.pauseButton)
        FloatingActionButton pausebutton;
        @BindView(R.id.deleteButton)
        FloatingActionButton deleteButton;
        @BindView(R.id.restartButton)
        FloatingActionButton restartButton;
        @BindView(R.id.openButton)
        FloatingActionButton openButton;
        @BindView(R.id.downloadProgressBar)
        NumberProgressBar progressBar;
        @BindView(R.id.download_hold_hint)
        TextView holdHint;


        CardHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public void setItems(ArrayList<Torrent> items) {
        this.torrents.clear();
        this.torrents.addAll(items);
        notifyDataSetChanged();
    }

    private void loadImage(int position, CardHolder cardHolder) {
        if (torrents.get(position).getCoverImage() != null) {
            Picasso.with(context.getApplicationContext())
                    .load(torrents.get(position).getCoverImage())
                    .into(cardHolder.coverImage);

        } else {
            cardHolder.coverImage.setImageDrawable(null);
        }

    }

    private void showDeleteTorrentAlert(int position) {
        DeleteTorrentAlert newFragment = new DeleteTorrentAlert();
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.TAG_TORRENT_NAME, torrents.get(position).getName());
        bundle.putInt(MainActivity.TAG_TORRENT_POSITION, position);
        newFragment.setArguments(bundle);
        Activity activity = (Activity) context;
        newFragment.show(activity.getFragmentManager(), MainActivity.TAG_DIALOG);
    }

    synchronized public void removeTorrent(Torrent torrent) {
            int position = torrents.indexOf(torrent);
            if (position < 0 || position > torrents.size()) return;
            torrents.get(position).showingblackout = false;
            torrents.remove(position);
            notifyDataSetChanged();
    }

    private void showBlackoutButtons(CardHolder cardHolder, int position) {
        torrents.get(position).showingblackout = true;
        cardHolder.blackout.setVisibility(View.VISIBLE);
        cardHolder.centreLine.setVisibility(View.VISIBLE);
        cardHolder.deleteButton.setVisibility(View.VISIBLE);
        cardHolder.holdHint.setVisibility(View.VISIBLE);

        if (torrents.get(position).getPercCompleted() == 100) {
            cardHolder.openButton.setVisibility(View.VISIBLE);
            cardHolder.pausebutton.setVisibility(View.GONE);
            cardHolder.restartButton.setVisibility(View.GONE);
            return;
        }

        cardHolder.openButton.setVisibility(View.GONE);
        if (torrents.get(position).getTorrentState() == 4) {
            cardHolder.pausebutton.setVisibility(View.VISIBLE);
            cardHolder.restartButton.setVisibility(View.GONE);
        } else {
            cardHolder.pausebutton.setVisibility(View.GONE);
            cardHolder.restartButton.setVisibility(View.VISIBLE);
        }


    }

    private void hideBlackoutButtons(CardHolder cardHolder, int position) {
        torrents.get(position).showingblackout = false;
        cardHolder.blackout.setVisibility(View.GONE);
        cardHolder.centreLine.setVisibility(View.GONE);
        cardHolder.deleteButton.setVisibility(View.GONE);
        cardHolder.pausebutton.setVisibility(View.GONE);
        cardHolder.restartButton.setVisibility(View.GONE);
        cardHolder.openButton.setVisibility(View.GONE);
        cardHolder.holdHint.setVisibility(View.GONE);

    }

    public ArrayList<Torrent> getTorrents() {
        return torrents;
    }


    public void updateFinished() {
        notifyDataSetChanged();
    }

}

