package com.shwifty.tex.adapters;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.ServerHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.utilities.Format;
import com.shwifty.tex.utilities.NetworkUtil;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;


public class TorrentFileStreamAdapter extends ArrayAdapter<TorrentFile> {
    private Context context;
    private ArrayList<TorrentFile> torrentFiles = null;
    private String torrentName;
    private Torrent torrent;


    public TorrentFileStreamAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);
        this.context = context;


    }

    public Torrent getTorrent() {
        return torrent;
    }

    public void setTorrent(Torrent torrent) {
        this.torrentName = torrent.getName();
        this.torrentFiles = torrent.getTorrentFiles();
        this.torrent = torrent;
        Collections.sort(torrentFiles, new SortFiles());
    }

    private static class ViewHolderItem {
        TextView torrentFileName;
        TextView torrentFileSize;
        TextView torrentFilefolder;

    }

    static class SortFiles implements Comparator<TorrentFile> {
        @Override
        public int compare(TorrentFile file, TorrentFile file2) {
            List<String> s1 = splitPath(file.file.getPath());
            List<String> s2 = splitPath(file2.file.getPath());

            int c = Integer.valueOf(s1.size()).compareTo(s2.size());
            if (c != 0)
                return c;

            for (int i = 0; i < s1.size(); i++) {
                String p1 = s1.get(i);
                String p2 = s2.get(i);
                c = p1.compareTo(p2);
                if (c != 0)
                    return c;
            }

            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;


        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_item_stream_file, parent, false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                convertView.setClipToOutline(true);
            }
            viewHolder = new ViewHolderItem();
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }


        viewHolder.torrentFileName = (TextView) convertView.findViewById(R.id.torrent_files_name_stream);
        viewHolder.torrentFileSize = (TextView) convertView.findViewById(R.id.torrent_file_size_stream);
        viewHolder.torrentFilefolder = (TextView) convertView.findViewById(R.id.torrent_files_folder_stream);


        final TorrentFile torrentFile = torrentFiles.get(position);

        String s = torrentFile.file.getPath();

        List<String> ss = splitPathFilter(s);

        if (ss.size() == 0) {
            viewHolder.torrentFilefolder.setVisibility(View.GONE);
            viewHolder.torrentFileName.setText(s);
        } else {
            if (position == 0) {
                viewHolder.torrentFilefolder.setVisibility(View.GONE);
            } else {
                File p1 = new File(makePath(ss, context)).getParentFile();
                File p2 = new File(makePath(splitPathFilter(getItem(position - 1).file.getPath()), context)).getParentFile();
                if (p1 == null || p1.equals(p2)) {
                    viewHolder.torrentFilefolder.setVisibility(View.GONE);
                } else {
                    viewHolder.torrentFilefolder.setText(p1.getPath());
                    viewHolder.torrentFilefolder.setVisibility(View.VISIBLE);
                }
            }
            viewHolder.torrentFileName.setText(ss.get(ss.size() - 1));
        }

        viewHolder.torrentFileSize.setText(Format.formatSize(getContext(), torrentFile.file.getLength()));

        return convertView;
    }


    @Override
    public void addAll(Collection<? extends TorrentFile> collection) {
        super.addAll(collection);
    }

    @Override
    public void add(TorrentFile object) {
        super.add(object);
    }


    private List<String> splitPathFilter(String s) {
        List<String> ss = splitPath(s);
        if (ss.get(0).equals(torrentName))
            ss.remove(0);
        return ss;
    }


    private static List<String> splitPath(String s) {
        return new ArrayList<>(Arrays.asList(s.split(Pattern.quote(File.separator))));
    }

    private static String makePath(List<String> ss, Context context) {
        if (ss.size() == 0)
            return context.getString(R.string.forward_slash);
        return TextUtils.join(File.separator, ss);
    }

    public void setupShare(int selectedPosition) {
        TorrentFile selectedTorrentFile = null;
        for (int x = 0; x < torrent.getTorrentFiles().size(); x++) {
            TorrentFile tf = torrent.getTorrentFiles().get(x);
            if (x == selectedPosition) {
                tf.file.setCheck(true);
                MainApplication.getInstance().torrentFileCheck(torrent, tf.index, true);
                torrent.setSelectedFilesSize(Format.formatSize(context, tf.file.getLength()));
                torrent.setSelectedFileString(tf.file.getPath() + ":::");
                selectedTorrentFile = tf;
            } else {
                tf.file.setCheck(false);
                MainApplication.getInstance().torrentFileCheck(torrent, tf.index, false);
            }
        }

        if (selectedTorrentFile != null) {
            String ip = NetworkUtil.getIPAddress(true);
            String streamUrl = context.getString(R.string.http) + ip + context.getString(R.string.semicolon) + ServerHandler.streamPort + context.getString(R.string.torrentID_url) + torrent.getTorrentId() + context.getString(R.string.torrentFileID_url) + selectedTorrentFile.index;
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(torrentName, streamUrl);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, context.getString(R.string.link_copied_toast), Toast.LENGTH_SHORT).show();
                if (context instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) context;
                    mainActivity.showShareSnackbar(streamUrl);
                }
            });


        }
    }

}
