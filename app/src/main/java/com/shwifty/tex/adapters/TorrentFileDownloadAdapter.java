package com.shwifty.tex.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.utilities.Format;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TorrentFileDownloadAdapter extends ArrayAdapter<TorrentFile> {
    private Context context;
    private ArrayList<TorrentFile> torrentFiles = null;
    private String torrentName;
    private Torrent torrent;


    public TorrentFileDownloadAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);
        this.context = context;


    }

    public Torrent getTorrent() {
        return torrent;
    }

    public void setTorrent(Torrent torrent) {
        this.torrentName = torrent.getName();
        this.torrentFiles = torrent.getTorrentFiles();
        this.torrent = torrent;
        Collections.sort(torrentFiles, new SortFiles());
    }

    private class ViewHolderItem {
        TextView torrentFileName;
        TextView torrentFileSize;
        CheckBox torrentCheckBox;
        TextView torrentFilefolder;

    }

    private static class SortFiles implements Comparator<TorrentFile> {
        @Override
        public int compare(TorrentFile file, TorrentFile file2) {
            List<String> s1 = Format.splitPath(file.file.getPath());
            List<String> s2 = Format.splitPath(file2.file.getPath());

            int c = Integer.valueOf(s1.size()).compareTo(s2.size());
            if (c != 0)
                return c;

            for (int i = 0; i < s1.size(); i++) {
                String p1 = s1.get(i);
                String p2 = s2.get(i);
                c = p1.compareTo(p2);
                if (c != 0)
                    return c;
            }

            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;


        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_item_download_file, parent, false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                convertView.setClipToOutline(true);
            }
            viewHolder = new ViewHolderItem();
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }


        viewHolder.torrentFileName = (TextView) convertView.findViewById(R.id.torrent_files_name);
        viewHolder.torrentFileSize = (TextView) convertView.findViewById(R.id.torrent_file_size);
        viewHolder.torrentCheckBox = (CheckBox) convertView.findViewById(R.id.torrent_file_checkbox);
        viewHolder.torrentFilefolder = (TextView) convertView.findViewById(R.id.torrent_files_folder);


        final TorrentFile torrentFile = torrentFiles.get(position);


        viewHolder.torrentCheckBox.setChecked(torrentFile.file.getCheck());
        viewHolder.torrentCheckBox.setOnClickListener(v -> {
            torrentFile.file.setCheck(viewHolder.torrentCheckBox.isChecked());
            MainApplication.getInstance().torrentFileCheck(torrent, torrentFile.index, viewHolder.torrentCheckBox.isChecked());

        });

        convertView.setOnClickListener(view -> {
            if (viewHolder.torrentCheckBox.isChecked()) {
                viewHolder.torrentCheckBox.setChecked(false);
            } else {
                viewHolder.torrentCheckBox.setChecked(true);
            }
            torrentFile.file.setCheck(viewHolder.torrentCheckBox.isChecked());
            MainApplication.getInstance().torrentFileCheck(torrent, torrentFile.index, viewHolder.torrentCheckBox.isChecked());
        });


        String s = torrentFile.file.getPath();

        List<String> ss = Format.splitPathFilter(s, torrentName);

        if (ss.size() == 0) {
            viewHolder.torrentFilefolder.setVisibility(View.GONE);
            viewHolder.torrentFileName.setText(s);
        } else {
            if (position == 0) {
                viewHolder.torrentFilefolder.setVisibility(View.GONE);
            } else {
                File p1 = new File(makePath(ss)).getParentFile();
                File p2 = new File(makePath(Format.splitPathFilter(getItem(position - 1).file.getPath(), torrentName))).getParentFile();
                if (p1 == null || p1.equals(p2)) {
                    viewHolder.torrentFilefolder.setVisibility(View.GONE);
                } else {
                    viewHolder.torrentFilefolder.setText(p1.getPath());
                    viewHolder.torrentFilefolder.setVisibility(View.VISIBLE);
                }
            }
            viewHolder.torrentFileName.setText(ss.get(ss.size() - 1));
        }

        viewHolder.torrentFileSize.setText(Format.formatSize(getContext(), torrentFile.file.getLength()));

        return convertView;
    }

    @Override
    public void addAll(Collection<? extends TorrentFile> collection) {
        super.addAll(collection);
    }

    @Override
    public void add(TorrentFile object) {
        super.add(object);
    }


    private static String makePath(List<String> ss) {
        if (ss.size() == 0)
            return "/";
        return TextUtils.join(File.separator, ss);
    }

}
