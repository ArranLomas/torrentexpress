package com.shwifty.tex.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.jpa.TPBTorrent;

import java.util.ArrayList;


public class MyDatabaseHelper extends SQLiteOpenHelper {

    //TORRENT
    private static final String colUniqueID = "_id";
    private static final String colTorrentID = "torrentID";
    private static final String colMagnetLink = "magnetLink";
    private static final String colName = "name";
    private static final String colSeeds = "seeds";
    private static final String colLeechers = "leechers";
    private static final String colCoverImage = "coverImage";
    private static final String colSize = "size";
    private static final String colSelectedFileSize = "selectedFileSize";
    private static final String colDirectory = "directory";
    private static final String colIsdownloading = "isDownloading";
    private static final String colPercCompleted = "percCompleted";
    private static final String colSelectedFileString = "selectedFileString";
    private static final String colSpeedString = "speedString";
    //TPB_TORRENT
    private static final String colTPBName = "tpbName";
    private static final String colMagnet = "magnet";
    private static final String colLink = "link";
    private static final String colUploaded = "uploaded";
    private static final String colTPBSize = "TPBSize";
    private static final String colUled = "uled";
    private static final String colTPBSeeds = "TPBSeeds";
    private static final String colTPBLeechers = "TPBLeechers";
    private static final String colCategoryParent = "parentCategory";
    private static final String colCategory = "category";
    private static final String colImdbID = "IMDBid";
    private static final String colTPBCoverImage = "TPBCoverImage";
    private static final String colBlackoutShowing = "blackoutShowing";

    private static final String STORED_TORRENTS_TABLE = "storedTorrensTable";
    private static final int TORRENT_DATABASE_VERSION = 1;


    public MyDatabaseHelper(Context context) {
        super(context, STORED_TORRENTS_TABLE, null, TORRENT_DATABASE_VERSION);
    }

    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(MyDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + STORED_TORRENTS_TABLE);
        onCreate(database);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String creationString = "CREATE TABLE IF NOT EXISTS "
                + STORED_TORRENTS_TABLE
                + "("
                + colUniqueID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + colTorrentID + " INTEGER,"
                + colMagnetLink + " TEXT,"
                + colName + " TEXT,"
                + colSeeds + " INTEGER,"
                + colLeechers + " INTEGER,"
                + colCoverImage + " TEXT,"
                + colSize + " TEXT,"
                + colSelectedFileSize + " TEXT,"
                + colDirectory + " TEXT,"
                + colIsdownloading + " INTEGER,"
                + colPercCompleted + " INTEGER,"
                + colSelectedFileString + " TEXT,"
                + colSpeedString + " TEXT,"


                + colTPBName + " TEXT,"
                + colMagnet + " TEXT,"
                + colLink + " TEXT,"
                + colUploaded + " TEXT,"
                + colTPBSize + " TEXT,"
                + colUled + " TEXT,"
                + colTPBSeeds + " INTEGER,"
                + colTPBLeechers + " INTEGER,"
                + colCategoryParent + " TEXT,"
                + colCategory + " TEXT,"
                + colImdbID + " TEXT,"
                + colTPBCoverImage + " TEXT,"
                + colBlackoutShowing + " INTEGER"
                + ")";
        sqLiteDatabase.execSQL(creationString);
    }

    public void addTorrent(Torrent torrent) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        //TORRENT
        values.put(colTorrentID, torrent.getTorrentId());
        values.put(colMagnetLink, torrent.getMagnetLink());
        values.put(colName, torrent.getName());
        values.put(colSeeds, torrent.getSeeds());
        values.put(colLeechers, torrent.getLeechers());
        values.put(colCoverImage, torrent.getCoverImage());
        values.put(colSize, torrent.getSize());
        values.put(colSelectedFileSize, torrent.getSelectedFilesSize());
        values.put(colDirectory, torrent.getDirectory());
        if (torrent.isDownloading()) {
            values.put(colIsdownloading, 1);
        } else {
            values.put(colIsdownloading, 0);
        }
        values.put(colPercCompleted, torrent.getPercCompleted());
        values.put(colSelectedFileString, torrent.getSelectedFilesString());
        values.put(colSpeedString, torrent.getSpeedString());

        //TPDTorrent
        values.put(colTPBName, torrent.getTpbTorrent().getName()); //14
        values.put(colMagnet, torrent.getTpbTorrent().getMagnet());
        values.put(colLink, torrent.getTpbTorrent().getLink());
        values.put(colUploaded, torrent.getTpbTorrent().getUploaded());
        values.put(colTPBSize, torrent.getTpbTorrent().getSize());
        values.put(colUled, torrent.getTpbTorrent().getUled());
        values.put(colTPBSeeds, torrent.getTpbTorrent().getSeeds());
        values.put(colTPBLeechers, torrent.getTpbTorrent().getLeechers());
        values.put(colCategoryParent, torrent.getTpbTorrent().getCategoryParent());
        values.put(colCategory, torrent.getTpbTorrent().getCategory());
        values.put(colImdbID, torrent.getTpbTorrent().getImdbID());
        values.put(colTPBCoverImage, torrent.getTpbTorrent().getCoverImage());
        if (torrent.getTpbTorrent().blackoutShowing) {
            values.put(colBlackoutShowing, 1);
        } else {
            values.put(colBlackoutShowing, 0);
        }


        db.insert(STORED_TORRENTS_TABLE, null, values);
        db.close(); // Closing database connection
    }

    public boolean deleteTorrent(Torrent torrent) {
        SQLiteDatabase db = this.getWritableDatabase();

        String magnet = torrent.getMagnetLink();
        if (magnet == null) return false;


        int deleted = db.delete(STORED_TORRENTS_TABLE, colMagnetLink + "=?", new String[]{torrent.getMagnetLink()});
        db.close();
        return deleted > 0;

    }

    public ArrayList<Torrent> getTorrents() {
        Log.e("DATABES", "ITEM COUNT: " + getTorrentCount());
        ArrayList<Torrent> torrents = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + STORED_TORRENTS_TABLE, null);
        Log.e("cursor count", "count: " + cursor.getCount());
        if (cursor.moveToFirst()) {
            try {
                do {
                    Torrent torrent = parseTorrentFromDB(cursor);
                    torrents.add(torrent);
                }while (cursor.moveToNext());
            }catch (Exception e){
                e.printStackTrace();
                Log.e("ARRAN ERROR", e.toString());
            } finally {
                Log.e("ARRAN", "CLOSED CURSOR");
                cursor.close();
            }

        }

        db.close();
        Log.e("getTorrents", "SIZE: " + torrents.size());
        return torrents;

    }

    private Torrent parseTorrentFromDB(Cursor cursor) throws CursorIndexOutOfBoundsException {
        Torrent torrent = null;

        boolean isDownload = false;


        boolean blackoutShowing = false;
        if (cursor.getInt(26) > 0) blackoutShowing = true;

        TPBTorrent tpbTorrent = new TPBTorrent(
                cursor.getString(14),
                cursor.getString(15),
                cursor.getString(16),
                cursor.getString(17),
                cursor.getString(18),
                cursor.getString(19),
                cursor.getInt(20),
                cursor.getInt(21),
                cursor.getString(22),
                cursor.getString(23),
                cursor.getString(24),
                cursor.getString(25),
                blackoutShowing
        );


        if (cursor.getInt(10) > 0) isDownload = true;
        torrent = new Torrent(
                (long) cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getInt(4),
                cursor.getInt(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8),
                cursor.getString(9),
                isDownload,
                cursor.getInt(11),
                cursor.getString(12),
                cursor.getString(13),
                tpbTorrent);

        Log.e("loaded torrent", "from storage: " + torrent.getName());
        return torrent;
    }

    public long getTorrentCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long cnt = DatabaseUtils.queryNumEntries(db, STORED_TORRENTS_TABLE);
        db.close();
        return cnt;
    }

    public void updateTorrentSpeedAndPerc(Torrent torrent) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(colSpeedString, torrent.getSpeedString());
        values.put(colPercCompleted, torrent.getPercCompleted());

        db.update(STORED_TORRENTS_TABLE, values, colMagnetLink + "=?", new String[]{torrent.getMagnetLink()});

    }

    public void emptyDatabse() {
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(STORED_TORRENTS_TABLE, null, null);
    }


}