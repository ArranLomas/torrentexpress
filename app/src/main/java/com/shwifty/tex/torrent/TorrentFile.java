package com.shwifty.tex.torrent;

public class TorrentFile {

    public long index;
    public go.libtorrent.File file;

    public TorrentFile(long i, go.libtorrent.File f) {
        this.file = f;
        this.index = i;
    }

}
