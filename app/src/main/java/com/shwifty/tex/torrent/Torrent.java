package com.shwifty.tex.torrent;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.services.LibtorrentService;
import com.shwifty.tex.utilities.Format;
import com.shwifty.tex.utilities.jpa.TPBTorrent;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.ArrayList;

import go.libtorrent.StatsTorrent;


public class Torrent {
    private ArrayList<TorrentFile> torrentFiles = new ArrayList<>();
    private String directory;
    private long torrentId;
    private String magnetLink;
    private String name;
    private int seeds;
    private int leechers;
    private String coverImage;
    private String size;
    private String selectedFilesSize;
    private String speedString = "pending";
    private int percCompleted;
    private long bytesLength = -1;
    private boolean downloading;

    private SpeedInfo speedInfo = new SpeedInfo();
    public boolean showingblackout = false;
    private boolean completedShown = false;
    private String selectedFilesString;
    private DescriptiveStatistics speedStatistics = new DescriptiveStatistics();
    private static final  int runningAverageWindow = 10;
    private TPBTorrent tpbTorrent;

//

    public Torrent( long torrentId, String directory, TPBTorrent tpbTorrent, String libtorrentName) {
        speedStatistics.setWindowSize(runningAverageWindow);
        DescriptiveStatistics timeStatistics = new DescriptiveStatistics();
        timeStatistics.setWindowSize(runningAverageWindow);
        this.tpbTorrent = tpbTorrent;
        this.torrentId = torrentId;
        this.directory = directory;
        this.magnetLink = tpbTorrent.Magnet;
        this.name = libtorrentName;
        this.seeds = tpbTorrent.Seeds;
        this.leechers = tpbTorrent.Leechers;
        this.size = tpbTorrent.Size;
        this.coverImage = tpbTorrent.CoverImage;
    }

    public Torrent(long torrentId, String magnetLink, String name, int seeds, int leechers, String coverImage, String size, String selectedFilesSize, String directory, boolean isDownloading, int percCompleted, String selectedFilesString, String speedString, TPBTorrent tpbTorrent) {
        this.torrentId = torrentId;
        this.magnetLink = magnetLink;
        this.name = name;
        this.seeds = seeds;
        this.leechers = leechers;
        this.coverImage = coverImage;
        this.size = size;
        this.selectedFilesSize = selectedFilesSize;
        this.directory = directory;
        this.downloading = isDownloading;
        this.percCompleted = percCompleted;
        this.selectedFilesString = selectedFilesString;
        this.speedString = speedString;
        this.tpbTorrent = tpbTorrent;
    }


    //    public Torrent(String directory, long torrentId, String magnetLink, String name, int seeds, int leechers, String coverImage, String size, String selectedFilesSize) {
//        speedStatistics.setWindowSize(runningAverageWindow);
//        timeStatistics.setWindowSize(runningAverageWindow);
//        this.directory = directory;
//        this.torrentId = torrentId;
//        this.magnetLink = magnetLink;
//        this.name = name;
//        this.seeds = seeds;
//        this.leechers = leechers;
//        this.coverImage = coverImage;
//        this.size = size;
//        this.selectedFilesSize = selectedFilesSize;
//    }

//    public Torrent(Context context) {
//        speedStatistics.setWindowSize(runningAverageWindow);
//        timeStatistics.setWindowSize(runningAverageWindow);
//        this.context = context;
//    }

    public String getMagnetLink() {
        return magnetLink;
    }

    public String toString() {
        String str = name;

        MainApplication mainApplication = MainApplication.getInstance();
        if (mainApplication.isMetaTorrent(torrentId))
            str += " · " + mainApplication.getBytesLength(torrentId);

        str += " · (" + percCompleted + "%)";

        return str;
    }

    public void setTorrentFiles(ArrayList<TorrentFile> torrentFiles) {
        this.torrentFiles.clear();
        this.torrentFiles.addAll(torrentFiles);
    }

    public ArrayList<TorrentFile> getTorrentFiles() {
        return torrentFiles;
    }

    public long getTorrentId() {
        return torrentId;
    }

    public String getName() {
        return name;
    }

    public int getSeeds() {
        return seeds;
    }

    public int getLeechers() {
        return leechers;
    }

    public String getSize() {
        return size;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public int getTorrentState() {
        MainApplication mainApplication = MainApplication.getInstance();
        int s = mainApplication.getTorrentStatus(torrentId);
        if (s == mainApplication.statusChecking()) {
            return 1;
        }
        if (s == mainApplication.statusQueued()) {
            return 2;
        }

        if (s == mainApplication.statusPaused()) {
            return 3;
        } else {
            return 4;
        }

    }

    public String getSelectedFilesSize() {
        return selectedFilesSize;
    }

    public String getSpeedString() {
        return speedString;
    }

    public int getPercCompleted() {
        return percCompleted;
    }

    public void setSelectedFilesSize(String selectedFilesSize) {
        this.selectedFilesSize = selectedFilesSize;
    }

    public void updateProgress() {
        MainApplication mainApplication = MainApplication.getInstance();
        if (this.speedString != null) {
            if (this.percCompleted == 100 || this.speedString.equalsIgnoreCase("completed")) {
                return;
            }
        }

        StatsTorrent b = mainApplication.getTorrentStats(torrentId);
        speedInfo.updateSpeed(System.currentTimeMillis(), b.getDownloaded());

        String speed = Format.formatSize(mainApplication.getApplicationContext(), speedInfo.averageSpeed);
        this.percCompleted = speedInfo.calculatePercComplete();

        int status = mainApplication.getTorrentStatus(this.torrentId);

        if(status == LibtorrentService.TORRENT_STATUS_PAUSED){
            this.speedString = "stopped";
            return;
        }

        if(status == LibtorrentService.TORRENT_STATUS_CHECKING){
            this.speedString = "checking";
            return;
        }

        if(status == LibtorrentService.TORRENT_STATUS_QUEUED){
            this.speedString = "queued";
            return;
        }

        if (this.percCompleted == 100) {
            this.speedString = "completed";
            mainApplication.stopTorrent(mainApplication.getApplicationContext(), this);
            mainApplication.cleanStorageOfTorrent(this);
        } else {
            this.speedString = "↓ " + speed + mainApplication.getApplicationContext().getString(R.string.per_second);
        }
    }

    private class SpeedInfo {
        private long lastTime;
        private long lastBytes;
        private long currentSpeed = 1;
        private long averageSpeed = 1;

        void updateSpeed(long time, long currentBytes) {
            long downloadedBytes = currentBytes - lastBytes;
            long timeTaken = time - lastTime;

            if (timeTaken > 0.0 && downloadedBytes > 0.0){
                currentSpeed = ((downloadedBytes * 1000) / timeTaken);
            }else{
                currentSpeed =  0;
            }
            speedStatistics.addValue(currentSpeed);
            averageSpeed = (long) speedStatistics.getMean();
            this.lastTime = time;
            this.lastBytes = currentBytes;
        }

        int calculatePercComplete() {
            MainApplication mainApplication = MainApplication.getInstance();
            if (mainApplication.isMetaTorrent(torrentId)) {
                if(bytesLength == -1){
                    bytesLength = mainApplication.getBytesLength(torrentId);
                }

                if (bytesLength == 0) return 0;
                return (int) (mainApplication.getBytesCompleted(torrentId) * 100 / bytesLength);
            } else {
                return 0;
            }
        }
    }

    public void setTorrentId(long torrentID) {
        this.torrentId = torrentID;
    }

    public String getDirectory() {
        return directory;
    }

    public void setMagnetLink(String magnetLink) {
        this.magnetLink = magnetLink;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSeeds(int seeds) {
        this.seeds = seeds;
    }

    public void setLeechers(int leechers) {
        this.leechers = leechers;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean isDownloading() {
        return downloading;
    }

    public void setDownloading(boolean downloading) {
        this.downloading = downloading;
    }

    public void setPercCompleted(int percCompleted) {
        this.percCompleted = percCompleted;
    }

    public String getSelectedFilesString() {
        return selectedFilesString;
    }

    public void setSelectedFileString(String selectedFileIndexes) {
        this.selectedFilesString = selectedFileIndexes;
    }

    public void setSpeedString(String speedString) {
        this.speedString = speedString;
    }

    public boolean isCompletedShown() {
        return completedShown;
    }

    public void setCompletedShown(boolean completedShown) {
        this.completedShown = completedShown;
    }

    public TPBTorrent getTpbTorrent() {
        return tpbTorrent;
    }

    public void setTpbTorrent(TPBTorrent tpbTorrent) {
        this.tpbTorrent = tpbTorrent;
    }
}

