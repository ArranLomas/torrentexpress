package com.shwifty.tex.torrent;

import com.shwifty.tex.utilities.jpa.TPBTorrent;

/**
 * Created by arran on 18/01/2017.
 */

public  class StoredTorrent {
    private long torrentId;
    private String magnetLink;
    private String name;
    private int seeds;
    private int leechers;
    private String coverImage;
    private String size;
    private String selectedFilesSize;
    private String directory;
    private boolean isDownloading;
    private int percCompleted;
    private String selectedFilesString;
    private String speedString;
    private TPBTorrent tpbTorrent;

    public long getTorrentId() {
        return torrentId;
    }

    public void setTorrentId(long torrentId) {
        this.torrentId = torrentId;
    }

    public String getMagnetLink() {
        return magnetLink;
    }

    public void setMagnetLink(String magnetLink) {
        this.magnetLink = magnetLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeeds() {
        return seeds;
    }

    public void setSeeds(int seeds) {
        this.seeds = seeds;
    }

    public int getLeechers() {
        return leechers;
    }

    public void setLeechers(int leechers) {
        this.leechers = leechers;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSelectedFilesSize() {
        return selectedFilesSize;
    }

    public void setSelectedFilesSize(String selectedFilesSize) {
        this.selectedFilesSize = selectedFilesSize;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public boolean isDownloading() {
        return isDownloading;
    }

    public void setDownloading(boolean downloading) {
        isDownloading = downloading;
    }

    public int getPercCompleted() {
        return percCompleted;
    }

    public void setPercCompleted(int percCompleted) {
        this.percCompleted = percCompleted;
    }

    public String getSelectedFilesString() {
        return selectedFilesString;
    }

    public void setSelectedFilesString(String selectedFilesString) {
        this.selectedFilesString = selectedFilesString;
    }

    public String getSpeedString() {
        return speedString;
    }

    public void setSpeedString(String speedString) {
        this.speedString = speedString;
    }

    public TPBTorrent getTpbTorrent() {
        return tpbTorrent;
    }

    public void setTpbTorrent(TPBTorrent tpbTorrent) {
        this.tpbTorrent = tpbTorrent;
    }
}
