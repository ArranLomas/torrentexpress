package com.shwifty.tex.ui;


import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ListView;

public class ExpandedListView extends ListView {
    private int myHeight;
    private boolean heightSet = false;

    public ExpandedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        heightSet = false;
        int height = 0;
        for (int i = 0; i < this.getChildCount(); i++) {
            height += this.getChildAt(i).getMeasuredHeight();
            height += this.getDividerHeight();
        }
        myHeight = height;
        heightSet = true;
        android.view.ViewGroup.LayoutParams params = getLayoutParams();
        params.height = myHeight;
        setLayoutParams(params);

        super.onDraw(canvas);
    }


    public int getMyHeight() {
        return myHeight;
    }

    public boolean isHeightSet() {
        return heightSet;
    }
}
