package com.shwifty.tex.handlers;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;
import com.shwifty.tex.activities.VideoActivity;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.MimeConstants;

public class CastHandler {
    private CastSession mCastSession;
    private static SessionManagerListener<CastSession> mSessionManagerListener;
    private CastContext mCastContext;
    private static boolean connected;

    public void initializeCastContext(Context context){
        mCastContext = CastContext.getSharedInstance(context);

        setupCastListener();
    }

    private void setupCastListener() {
        mSessionManagerListener = new SessionManagerListener<CastSession>() {

            @Override
            public void onSessionEnded(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionResumed(CastSession session, boolean wasSuspended) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionResumeFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarted(CastSession session, String sessionId) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionStartFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarting(CastSession session) {}

            @Override
            public void onSessionEnding(CastSession session) {}

            @Override
            public void onSessionResuming(CastSession session, String sessionId) {}

            @Override
            public void onSessionSuspended(CastSession session, int reason) {}

            private void onApplicationConnected(CastSession castSession) {
                connected = true;
                mCastSession = castSession;
                Log.v("arranzlz", "session STARTED!!!");
                VideoActivity.notifySessionStarted();
            }

            private void onApplicationDisconnected() {
                connected = false;
            }
        };
    }

    public void loadRemoteMedia(Torrent torrent, String streamUrl, String mime) {
        if (mCastSession == null) {
            return;
        }
        RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient == null) {
            return;
        }
        MediaInfo mediaInfo = buildMediaInfo(torrent, streamUrl, mime);
        remoteMediaClient.load(mediaInfo, true);
    }

    private static MediaInfo buildMediaInfo(Torrent torrent, String streamUrl, String mime) {
        MediaMetadata movieMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
        movieMetadata.putString(MediaMetadata.KEY_TITLE, torrent.getName());
        if(torrent.getTpbTorrent().getCoverImage()!= null){
            movieMetadata.addImage(new WebImage(Uri.parse(torrent.getTpbTorrent().getCoverImage())));
        }
        return new MediaInfo.Builder(streamUrl)
                .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                .setContentType(mime)
                .setMetadata(movieMetadata)
                .build();
    }

    public void addSessionListener(){
        mCastContext.getSessionManager().addSessionManagerListener(
                mSessionManagerListener, CastSession.class);
    }

    public void removeSessionListener(){
        mCastContext.getSessionManager().removeSessionManagerListener(
                mSessionManagerListener, CastSession.class);
    }

    public boolean isConnected() {
        return connected;
    }

    boolean canCast(String format){
        return MimeConstants.chromecastSet.contains(format.toLowerCase());
    }

    public CastContext getmCastContext() {
        return mCastContext;
    }
}
