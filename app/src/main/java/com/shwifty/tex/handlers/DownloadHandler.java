package com.shwifty.tex.handlers;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.database.MyDatabaseHelper;
import com.shwifty.tex.services.LibtorrentService;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


//TODO     -    on start - open database - add to app - if isDoanloding - start downloading but do not re-add to database
//TODO     -    add another database for TPB torrents and link the databse


public class DownloadHandler {
    private static final String TAG_TORRENT = "torrentDownloadList";
    private static final ReadWriteLock lock = new ReentrantReadWriteLock();
    private static NotificationManager notificationManager;
    private static HashMap<Torrent, NotificationCompat.Builder> notificationHashMap = new HashMap<>();
    private static boolean torrentsLoaded = false;
    private boolean updateThreadRunning = false;
    private Thread torrentUpdateThread;
    private static ArrayList<Torrent> torrents = new ArrayList<>();
    private MyDatabaseHelper myDatabaseHelper;

    public DownloadHandler(Context context) {
        myDatabaseHelper = new MyDatabaseHelper(context);
    }

    public ArrayList<Torrent> getTorrents() {
        return torrents;
    }

    public boolean isTorrentAdded(Torrent torrent) {
       for (int x = 0; x < torrents.size(); x++) {
            if (torrents.get(x).getMagnetLink().equalsIgnoreCase(torrent.getMagnetLink())) {
                return true;
            }
        }
        return false;

    }

    public void addTorrentForDownload(Context context, Torrent torrent) {
        if (!torrentsLoaded) {
            Toast.makeText(context, R.string.cannot_load_torrent_toast, Toast.LENGTH_LONG).show();
            return;
        }
        torrents.add(torrent);
        MainApplication mainApplication = MainApplication.getInstance();
        mainApplication.startTorrent(context, torrent);
        startTorrentUpdateThread(context);

        myDatabaseHelper.addTorrent(torrent);

    }


    public void removeTorrent(Context context, Torrent torrent) {
        for (int x = 0; x < torrents.size(); x++) {
            if (torrents.get(x).getMagnetLink().equalsIgnoreCase(torrent.getMagnetLink())) {
                torrents.remove(x);
                break;
            }
        }
        removeNotification(context, torrent);

        if (torrents.size() == 0) {
            stopTorrentUpdateThreadRunning();
        }

        myDatabaseHelper.deleteTorrent(torrent);
        checkForCleanDatabase();
    }

    public ReadWriteLock getLock() {
        return lock;
    }

    @SuppressWarnings("deprecation")
    public static void showNotification(Context context, Torrent torrent) {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);

        Intent intent = new Intent(context, MainActivity.class);
        String actionShowDownloads = context.getString(R.string.action_show_downloads);
        intent.setAction(actionShowDownloads);
        PendingIntent pi = PendingIntent.getActivity(context, (int) torrent.getTorrentId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Intent cancelI = new Intent(MainApplication.getInstance().TAG_NOTIFICATION_CANCEL);
        cancelI.putExtra(context.getString(R.string.torrentID), torrent.getTorrentId());
        PendingIntent cancelIntent = PendingIntent.getBroadcast(context, (int) torrent.getTorrentId(), cancelI, 0);

//Set notification information:
        notificationBuilder.
                setContentTitle(torrent.getName() + " • " + torrent.getSpeedString())
                .setContentText(torrent.getPercCompleted() + "%")
                .setSmallIcon(R.drawable.ic_download)
                .setProgress(100, torrent.getPercCompleted(), false)
                .setOngoing(true)
                .setContentIntent(pi);

        notificationBuilder.addAction(new android.support.v4.app.NotificationCompat.Action(R.drawable.ic_cancel, context.getString(R.string.CANCEL), cancelIntent));

//Send the notification:
        Notification notification;
        if (Build.VERSION.SDK_INT < 16) {
            notification = notificationBuilder.getNotification();
        } else {
            notification = notificationBuilder.build();
        }

        notificationHashMap.put(torrent, notificationBuilder);
        if (torrent.getPercCompleted() == 100) {
            torrent.setCompletedShown(true);
            return;
        }
        notificationManager.notify((int) torrent.getTorrentId(), notification);
    }

    @SuppressWarnings("deprecation")
    public static void updateNotificationPerc(Context context, Torrent torrent) {
        int status = MainApplication.getInstance().getTorrentStatus(torrent.getTorrentId());

        if (status == LibtorrentService.TORRENT_STATUS_PAUSED && torrent.getPercCompleted() != 100) {
            removeNotification(context, torrent);
            return;
        }

        NotificationCompat.Builder notificationBuilder = notificationHashMap.get(torrent);
        if (notificationBuilder == null) {
            showNotification(context, torrent);
            return;
        }
        notificationBuilder.setProgress(100, torrent.getPercCompleted(), false);
        String speedString = torrent.getSpeedString();
        if (speedString == null || speedString.equalsIgnoreCase("null")) {
            speedString = "0.0B/s";
        }

        notificationBuilder.
                setContentTitle(torrent.getName())
                .setContentText(speedString);


        if (torrent.getPercCompleted() == 100) {
            boolean shown = torrent.isCompletedShown();
            if (shown) {
                return;
            }
            torrent.setCompletedShown(true);
            notificationBuilder.setSmallIcon(R.drawable.ic_completed);
            notificationBuilder.setOngoing(false);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.mActions.clear();
            if (Prefs.playNotificationSound) {
                Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationBuilder.setSound(uri);
            }
        } else {
            notificationBuilder.setOngoing(true);
        }


        Notification notification;
        if (Build.VERSION.SDK_INT < 16) {
            notification = notificationBuilder.getNotification();
        } else {
            notification = notificationBuilder.build();
        }

        notificationManager.notify((int) torrent.getTorrentId(), notification);
    }

    public void shutdown(Context context) {
//        ArrayList<Torrent> torrents = myDatabaseHelper.getTorrents();
        for (Torrent torrent : torrents) {
            MainApplication.getInstance().stopTorrent(context, torrent);
        }
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        checkForCleanDatabase();
    }


    public static void removeNotification(Context context, Torrent torrent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (torrent == null) {
            notificationManager.cancelAll();
            return;
        }
        notificationManager.cancel((int) torrent.getTorrentId());
    }

    public void loadTorrents(Context context) {
        Thread thread = new Thread(() -> {
            torrentsLoaded = false;

            ArrayList<Torrent> loadedTorrents = myDatabaseHelper.getTorrents();

            if (MainApplication.getInstance().getLibtorrentService() == null) {
                torrentsLoaded = true;
                return;
            }
            if (loadedTorrents == null) {
                torrentsLoaded = true;
                return;
            }
            for (Torrent loadedTorrent : loadedTorrents) {
                Log.e("adding torrent", loadedTorrent.getName());
                TorrentHandler torrentHandler = new TorrentHandler();
                torrentHandler.addFromLoadedTorrent(context, loadedTorrent);
            }


            torrentsLoaded = true;
            if (context instanceof MainActivity) {
                MainActivity mainActivity = (MainActivity) context;
                mainActivity.notifyDownloadListChanged();
            }
        });
        thread.start();
    }

    private void checkForCleanDatabase() {
        if (torrents != null) {
            if (torrents.size() < 1) {
                myDatabaseHelper.emptyDatabse();
            }
        }

    }

    public boolean isTorrentsLoaded() {
        return torrentsLoaded;
    }

    private void startTorrentUpdateThread(Context context) {
        if (updateThreadRunning) return;


        updateThreadRunning = true;
        torrentUpdateThread = new Thread(() -> {
            while (updateThreadRunning) {
                Observable.just(updateTorrentSpeeds(context))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe((successful) -> {
                            updateTorrentDatabase();
                        });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        torrentUpdateThread.start();

    }

    private void stopTorrentUpdateThreadRunning() {
        updateThreadRunning = false;
    }

    synchronized public boolean updateTorrentSpeeds(Context context) {
        boolean successful = true;
        final Lock r = getLock().readLock();
        r.lock();

        try {
            for (Torrent torrent : torrents) {
                torrent.updateProgress();
                DownloadHandler.updateNotificationPerc(context, torrent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            successful = false;
        } finally {
            r.unlock();
        }


        return successful;
    }

    synchronized public boolean updateTorrentDatabase() {
        boolean successful = true;
        final Lock r = getLock().readLock();
        r.lock();

        try {
            for (Torrent torrent : torrents) {
                myDatabaseHelper.updateTorrentSpeedAndPerc(torrent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            successful = false;
        } finally {
            r.unlock();
        }


        return successful;
    }

    public void addLoadedTorrentToDownloads(Context context, Torrent torrent) {
        Log.e("torrent added", torrent.getName());
        torrents.add(torrent);
        startTorrentUpdateThread(context);
    }
}
