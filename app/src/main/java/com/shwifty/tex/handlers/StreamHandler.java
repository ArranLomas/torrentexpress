package com.shwifty.tex.handlers;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.VideoActivity;
import com.shwifty.tex.torrent.Torrent;


public class StreamHandler {
    private static Torrent torrent;
    private static String streamUrl;
    private static String fileType;
    private static String format;
    private static String chromecastFormat;
    private static boolean wasMkv;

    public void setupStream(Torrent torrent, String streamUrl, String mimeType, Context context) {
        StreamHandler.torrent = torrent;
        StreamHandler.streamUrl = streamUrl;
        fileType = null;
        format = null;
        if (mimeType == null || !mimeType.contains("/")) {
            return;
        }

        String[] splitMime = mimeType.split("/");
        fileType = splitMime[0];
        format = splitMime[1];
        wasMkv = false;
        chromecastFormat = format;
        checkAvailableFromMime(context, torrent.getMagnetLink());
    }

    private void checkAvailableFromMime(Context context, String magnet) {
        MainActivity mainActivity = null;

        if (fileType == null) {
            Toast.makeText(context, context.getString(R.string.cannot_open_file_toast), Toast.LENGTH_LONG).show();
            return;
        }

        if (context instanceof MainActivity) {
            mainActivity = (MainActivity) context;
        }


        if (!fileType.equalsIgnoreCase("video") && !fileType.equalsIgnoreCase("image") && !fileType.equalsIgnoreCase("audio")) {
            startOtherApp(context);
            return;
        }

        if (fileType.equalsIgnoreCase("image")) {
            startOtherApp(context);
            return;
        }

        if (format.equalsIgnoreCase("x-matroska")) {
            wasMkv = true;
            chromecastFormat = "webm";
        }
        CastHandler castHandler = new CastHandler();
        boolean canCast = castHandler.canCast(chromecastFormat);


        if (fileType.equalsIgnoreCase("video") || fileType.equalsIgnoreCase("audio")) {
            //type is video or audio
            if (canCast && castHandler.isConnected()) {
                //the chromecast is connected and supports the format

                if (mainActivity == null) {
                    // main activity has been killed just show in video activity
                    startLibVlc(context, magnet);
                    return;
                }
                //format is supported by app and chromecast, ask the user what they want to open video in
                mainActivity.showOpenVideoDialog(magnet);
                return;
            }
            //format is not supported by chromecast
            startLibVlc(context, magnet);
        }
    }

    public static void startChromecast (Context context) {
        if(context instanceof MainActivity){
            MainActivity mainActivity = (MainActivity) context;
            mainActivity.logChromecastStarting();
        }
        if (wasMkv) {
            Toast.makeText(context, "Some mkv's may not work on chromecast", Toast.LENGTH_LONG).show();
        }

        MainApplication.getInstance().getCastHandler().loadRemoteMedia(torrent, streamUrl, fileType + "/" + chromecastFormat);
    }

    public static void startLibVlc(Context context, String magnet) {
        Intent vlcIntent = new Intent(context, VideoActivity.class);
        vlcIntent.putExtra(VideoActivity.TAG_VIDEO_URL, streamUrl);
        vlcIntent.putExtra(VideoActivity.TAG_MAGNET_STRING, magnet);
        vlcIntent.putExtra(VideoActivity.TAG_FORMAT_STRING, format);
        context.startActivity(vlcIntent);
    }

    private static void startOtherApp(Context context) {
        Intent mxIntent = new Intent(Intent.ACTION_VIEW);
        Uri streamURI = Uri.parse(streamUrl);
        mxIntent.setDataAndType(streamURI, fileType + "/" + format);
        try {
            context.startActivity(mxIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }

    }

}
