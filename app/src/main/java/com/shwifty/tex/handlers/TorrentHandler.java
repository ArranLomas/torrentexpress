package com.shwifty.tex.handlers;

import android.app.ProgressDialog;
import android.content.Context;

import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.utilities.Format;
import com.shwifty.tex.utilities.Prefs;
import com.shwifty.tex.utilities.Utilities;
import com.shwifty.tex.utilities.jpa.TPBTorrent;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TorrentHandler {
    private static ArrayList<Torrent> torrents;
    private static boolean torrentsInitialized = false;
    private MainApplication mainApplication = MainApplication.getInstance();
    private static final ReadWriteLock lock = new ReentrantReadWriteLock();

    public void initialiseTorrentList() {
        if (!torrentsInitialized) {
            torrents = new ArrayList<>();
            torrentsInitialized = true;
        }

    }
    
    public void addFromMagnet(String magnetString, Context context, TPBTorrent tpbTorrent) {
        Thread thread = new Thread(() -> {
            final String storagePath = Prefs.downloadPath;
            File fileDir = new File(storagePath);
            if (!fileDir.exists()) {
                boolean createSuccessful = false;
                if (fileDir.mkdirs()) createSuccessful = true;
                if(!createSuccessful){
                    if(context instanceof MainActivity){
                        MainActivity mainActivity = (MainActivity) context;
                        if(!mainActivity.hasPermission()) mainActivity.requestPermission();
                    }
                }
            }
            final long torrentID = mainApplication.addMagnet(storagePath, magnetString);
            if (torrentID == -1) {
                return;
            }


            mainApplication.downloadMetadata(torrentID);

            long count;
            boolean running = true;
            while (running) {
                count = mainApplication.getFileCount(torrentID);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (count > 0) {
                    running = false;
                }
            }

            final long torrentFileCount = mainApplication.getFileCount(torrentID);

            ArrayList<TorrentFile> torrentFiles = new ArrayList<>();

            for (int x = 0; x < torrentFileCount; x++) {
                TorrentFile file = new TorrentFile(x, mainApplication.getTorrentFile(torrentID, x));
                torrentFiles.add(file);
            }

            Torrent torrent = new Torrent(torrentID, storagePath, tpbTorrent, mainApplication.getTorrentName(torrentID));

            torrent.setTorrentFiles(torrentFiles);
            torrents.add(torrent);
        });
        thread.start();
    }

    void addFromMagnetIntent(String magnetString, Context context) {
        //TODO remove below
        Thread thread = new Thread(() -> {

            final String storagePath = Prefs.downloadPath;
            File fileDir = new File(storagePath);
            boolean createSuccessful = false;
            if (fileDir.mkdirs()) createSuccessful = true;
            if(!createSuccessful ){
                if(context instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity) context;
                    if(!mainActivity.hasPermission()) mainActivity.requestPermission();
                }
            }
            final long torrentID = mainApplication.addMagnet(storagePath, magnetString);
            if (torrentID == -1) {
                return;
            }


            mainApplication.downloadMetadata(torrentID);

            long count;
            boolean running = true;
            while (running) {
                count = mainApplication.getFileCount(torrentID);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (count > 0) {
                    running = false;
                }
            }

            final long torrentFileCount = mainApplication.getFileCount(torrentID);

            ArrayList<TorrentFile> torrentFiles = new ArrayList<>();

            for (int x = 0; x < torrentFileCount; x++) {
                TorrentFile file = new TorrentFile(x, mainApplication.getTorrentFile(torrentID, x));
                torrentFiles.add(file);
            }

            TPBTorrent tpbTorrent = new TPBTorrent();
            String name = mainApplication.getTorrentName(torrentID);
            long length = mainApplication.getBytesLength(torrentID);
            String size = Format.formatSize(context, length);

            tpbTorrent.Name = name;
            tpbTorrent.Magnet = magnetString;
            tpbTorrent.Size = size;
            tpbTorrent.setLeechers(-1);
            tpbTorrent.setSeeds(-1);

            Torrent torrent = new Torrent(torrentID, storagePath, tpbTorrent, mainApplication.getTorrentName(torrentID));

            torrent.setTorrentFiles(torrentFiles);
            torrents.add(torrent);

        });
        thread.start();
    }


    void addFromLoadedTorrent(Context context, Torrent torrent) {
        String directory;
        if (torrent.getDirectory() != null) {
            directory = torrent.getDirectory();
        } else {
            directory = Prefs.downloadPath;
        }

        File fileDir = new File(directory);
        boolean createSuccessful = false;
        if (fileDir.mkdirs()) createSuccessful = true;
        if(!createSuccessful){
            if(context instanceof MainActivity){
                MainActivity mainActivity = (MainActivity) context;
                if(!mainActivity.hasPermission()) mainActivity.requestPermission();
            }
        }

        final long torrentID = mainApplication.addMagnet(directory, torrent.getMagnetLink());
        if (torrentID == -1) {
            return;
        }
        torrent.setTorrentId(torrentID);

        mainApplication.downloadMetadata(torrentID);

        long count;
        boolean running = true;
        while (running) {
            count = mainApplication.getFileCount(torrentID);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (count > 0) {
                running = false;
            }
        }

        final long torrentFileCount = mainApplication.getFileCount(torrentID);

        ArrayList<TorrentFile> torrentFiles;

        String paths = torrent.getSelectedFilesString();

        if (paths != null) {
            List<String> downloadingFilePaths = Arrays.asList(paths.split(":::"));


            torrentFiles = new ArrayList<>();

            for (int x = 0; x < torrentFileCount; x++) {
                TorrentFile file = new TorrentFile(x, mainApplication.getTorrentFile(torrentID, x));
                for (String selectedPath : downloadingFilePaths) {
                    if (selectedPath.equals(file.file.getPath())) {
                        file.file.setCheck(true);
                        mainApplication.torrentFileCheck(torrent, file.index, true);
                    } else {
                        file.file.setCheck(false);
                        mainApplication.torrentFileCheck(torrent, file.index, false);
                    }
                }

                torrentFiles.add(file);
            }
        } else {
            torrentFiles = new ArrayList<>();
            for (int x = 0; x < torrentFileCount; x++) {
                TorrentFile file = new TorrentFile(x, mainApplication.getTorrentFile(torrentID, x));
                torrentFiles.add(file);
            }
        }


        torrent.setTorrentFiles(torrentFiles);


        torrents.add(torrent);

        DownloadHandler downloadHandler = new DownloadHandler(context);
        downloadHandler.addLoadedTorrentToDownloads(context, torrent);

        if(torrent.isDownloading()){
            MainApplication mainApplication = MainApplication.getInstance();
            mainApplication.startTorrent(context, torrent);
        }
    }

    public ArrayList<Torrent> getTorrents() {
        return torrents;
    }

    public Torrent getTorrentFromMagnet(String magnet) {
        for (int x = 0; x < torrents.size(); x++) {
            if (torrents.get(x).getMagnetLink().equals(magnet)) {
                return torrents.get(x);
            }
        }
        return null;
    }

    public Torrent getTorrentFromFilePath(String path) {
        Torrent selectedTorrent = null;

        for (Torrent torrent : torrents) {
            for (TorrentFile torrentFile : torrent.getTorrentFiles()) {
                String torrentFilePath = torrent.getDirectory() + File.separator + torrentFile.file.getPath();
                if (torrentFilePath.equalsIgnoreCase(path)) {
                    selectedTorrent = torrent;
                }
            }
        }

        return selectedTorrent;
    }

    public Torrent getTorrentFromID(long id) {
        for (int x = 0; x < torrents.size(); x++) {
            if (torrents.get(x).getTorrentId() == id) {
                return torrents.get(x);
            }
        }
        return null;
    }

    public void removeTorrent(Torrent torrent) {
        for (int x = 0; x < torrents.size(); x++) {
            if (torrents.get(x).getMagnetLink().equalsIgnoreCase(torrent.getMagnetLink())) {
                mainApplication.removeTorrent(torrents.get(x).getTorrentId());
                torrents.remove(x);
                break;
            }
        }
    }

    public boolean isTorrentCreatedMagnet(String magnet) {
        if (torrents.size() == 0) return false;
        Torrent torrent;


        for (int x = 0; x < torrents.size(); x++) {
            torrent = torrents.get(x);
            if (torrent == null) {
                return false;
            }

            if (torrents.get(x).getMagnetLink().equals(magnet)) {
                return true;
            }
        }
        return false;
    }

    public void readdTorrentForDownload(Context context, String torrentMagnet) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Re-Adding Torrent");
        progressDialog.show();
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent oldTorrent = torrentHandler.getTorrentFromMagnet(torrentMagnet);
        String storagePath = oldTorrent.getDirectory() + File.separator;
        ArrayList<String> selectedFiles = new ArrayList<>();

        mainApplication.stopTorrent(context, oldTorrent);

        DownloadHandler.removeNotification(context, oldTorrent);


        TPBTorrent tpbTorrent = oldTorrent.getTpbTorrent();
        String magnet = oldTorrent.getMagnetLink();
        ArrayList<TorrentFile> torrentFiles = oldTorrent.getTorrentFiles();
        for (TorrentFile torrentFile : torrentFiles) {
            if (torrentFile.file.getCheck()) {
                selectedFiles.add(torrentFile.file.getPath());
            }
        }

        File file = new File(storagePath + oldTorrent.getName());
        if (file.exists()) {
            Utilities.deleteRecursive(file);
        }

        MainActivity mainActivity = (MainActivity) context;

        if (mainActivity != null) {
            mainActivity.removeTorrentFromApp(oldTorrent);
        }

        torrentHandler.addFromMagnet(magnet, context, tpbTorrent);

        Thread thread = new Thread(() -> {
            boolean waitingForFilesRunning;
            waitingForFilesRunning = true;
            while (waitingForFilesRunning) {
                if (torrentHandler.isTorrentCreatedMagnet(magnet)) {
                    waitingForFilesRunning = false;
                }
            }
            Torrent newTorrent = torrentHandler.getTorrentFromMagnet(magnet);
            ArrayList<TorrentFile> newFiles = newTorrent.getTorrentFiles();
            for (TorrentFile torrentFile : newFiles) {
                if (selectedFiles.contains(torrentFile.file.getPath())) {
                    torrentFile.file.setCheck(true);
                    mainApplication.torrentFileCheck(newTorrent, torrentFile.index, true);
                } else {
                    torrentFile.file.setCheck(false);
                    mainApplication.torrentFileCheck(newTorrent, torrentFile.index, false);
                }
            }

//            Utilities.setDownloadInfoStrings(context, newTorrent);
//            downloadHandler.addTorrentForDownload(context, newTorrent);
//            downloadHandler.saveTorrentList(context);

            if (mainActivity != null) {
                mainActivity.showDownloadFilesDialog(magnet);
            }

        });
        thread.start();
    }

    public void readdTorrentForStream(Context context, String torrentMagnet) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Re-Adding Torrent");
        progressDialog.show();
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent oldTorrent = torrentHandler.getTorrentFromMagnet(torrentMagnet);
        String storagePath = oldTorrent.getDirectory() + File.separator;
        ArrayList<String> selectedFiles = new ArrayList<>();

        mainApplication.stopTorrent(context, oldTorrent);

        DownloadHandler.removeNotification(context, oldTorrent);


        TPBTorrent tpbTorrent = oldTorrent.getTpbTorrent();
        String magnet = oldTorrent.getMagnetLink();
        ArrayList<TorrentFile> torrentFiles = oldTorrent.getTorrentFiles();
        for (TorrentFile torrentFile : torrentFiles) {
            if (torrentFile.file.getCheck()) {
                selectedFiles.add(torrentFile.file.getPath());
            }
        }

        File file = new File(storagePath + oldTorrent.getName());
        if (file.exists()) {
            Utilities.deleteRecursive(file);
        }

        MainActivity mainActivity = (MainActivity) context;

        if (mainActivity != null) {
            mainActivity.removeTorrentFromApp(oldTorrent);
        }

        torrentHandler.addFromMagnet(magnet, context, tpbTorrent);

        Thread thread = new Thread(() -> {
            boolean waitingForFilesRunning;
            waitingForFilesRunning = true;
            while (waitingForFilesRunning) {
                if (torrentHandler.isTorrentCreatedMagnet(magnet)) {
                    waitingForFilesRunning = false;
                }
            }
            Torrent newTorrent = torrentHandler.getTorrentFromMagnet(magnet);
            ArrayList<TorrentFile> newFiles = newTorrent.getTorrentFiles();
            for (TorrentFile torrentFile : newFiles) {
                if (selectedFiles.contains(torrentFile.file.getPath())) {
                    torrentFile.file.setCheck(true);
                } else {
                    torrentFile.file.setCheck(false);
                }
            }
//            Utilities.setDownloadInfoStrings(context, newTorrent);
//            downloadHandler.addTorrentForDownload(context, newTorrent);
//            downloadHandler.saveTorrentList(context);

            if (mainActivity != null) {
                mainActivity.showStreamFilesDialog(magnet);
//                new Handler(Looper.getMainLooper()).post(() -> mainActivity.showDownloadsFragment());
            }

//                                Activity activity = getActivity();
//                                MainActivity mainActivity1 = null;
//                                if (activity instanceof MainActivity) {
//                                    mainActivity1 = (MainActivity) activity;
//                                }
//                                if(ma)
//
////                                Intent intent = new Intent(getActivity(), MainActivity.class);
////                                intent.setAction(MainActivity.ACTION_SHOW_DOWNLOADS);
////                                startActivity(intent);
//                                getDialog().dismiss();
        });
        thread.start();
    }

    public static void shutDown(Context context){
        for(Torrent torrent : torrents){
            MainApplication.getInstance().stopTorrent(context, torrent);
        }
    }

    public void stopStreamingTorrents(Context context) {
        DownloadHandler downloadHandler = new DownloadHandler(context);
        ArrayList<Torrent> downloadingTorrents = downloadHandler.getTorrents();

        final Lock r = getLock().readLock();
        r.lock();

        try {
            for (Torrent torrent : torrents) {
                boolean doStop = true;
                for (Torrent download : downloadingTorrents) {
                    if (torrent.getMagnetLink().equals(download.getMagnetLink())) {
                        doStop = false;
                    }
                }

                if (doStop) {
                    MainApplication.getInstance().stopTorrent(context, torrent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            r.unlock();
        }
    }

    public ReadWriteLock getLock() {
        return lock;
    }

}
