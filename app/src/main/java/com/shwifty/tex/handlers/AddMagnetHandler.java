package com.shwifty.tex.handlers;


import com.shwifty.tex.activities.MainActivity;

public class AddMagnetHandler {
   private static String magnet = null;


    public static void handleMagnetIntent(MainActivity mainActivity) {
        if (magnet == null) return;

        TorrentHandler torrentHandler = new TorrentHandler();

        if (torrentHandler.isTorrentCreatedMagnet(magnet)) {
            mainActivity.showHandleMagnetIntentAlert();
            return;
        }

        mainActivity.showAddingMagnetDialog();

        torrentHandler.addFromMagnetIntent(magnet, mainActivity);


        Thread thread = new Thread(() -> {
            while (!torrentHandler.isTorrentCreatedMagnet(magnet)) {
            }
            mainActivity.runOnUiThread(() -> {
                if(mainActivity.isAddingMagnetShowing()){
                    mainActivity.showHandleMagnetIntentAlert();
                }
            });
        });
        thread.start();
    }


    public static String getMagnet() {
        return magnet;
    }

    public static void setMagnet(String magnet) {
        AddMagnetHandler.magnet = magnet;
    }

}
