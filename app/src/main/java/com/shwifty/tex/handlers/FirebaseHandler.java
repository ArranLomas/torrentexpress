package com.shwifty.tex.handlers;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.activities.SplashActivity;
import com.shwifty.tex.utilities.Constants;

import java.util.ArrayList;

/**
 * Created by arran on 4/01/2017.
 */

public class FirebaseHandler {

    public static void getMirrorUrls(Object LOCK) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        ArrayList<String> urls = new ArrayList<>();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    if (!postSnapshot.getKey().equals(Constants.downloadAPKKey) && !postSnapshot.getKey().equals(Constants.latestVersionKey)) {
                        String post = postSnapshot.getValue(String.class);
                        urls.add(post);
                    }
                }
                Constants.setUrlTpb(urls);
                notifyLock(LOCK);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                notifyLock(LOCK);
            }
        });
    }


    public static void syncFirebaseUpdateInfo(Object LOCK) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.v("Count ", "" + snapshot.getChildrenCount());
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    if(postSnapshot.getKey().equals(Constants.latestVersionKey)){
                        MainApplication.setVersionNumber(postSnapshot.getValue(Integer.class));
                    }else if(postSnapshot.getKey().equals(Constants.downloadAPKKey)){
                        MainApplication.setHostUrl(postSnapshot.getValue(String.class));
                    }

                }
                notifyLock(LOCK);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                notifyLock(LOCK);
            }
        });
    }

    private static void lock(Object LOCK) {
        synchronized (LOCK) {
            try {
                LOCK.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void notifyLock(Object LOCK) {
        synchronized (LOCK) {
            LOCK.notify();
        }
    }
}
