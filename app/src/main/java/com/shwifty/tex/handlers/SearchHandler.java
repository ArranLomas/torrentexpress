package com.shwifty.tex.handlers;

import android.content.Context;

import com.shwifty.tex.utilities.Prefs;
import com.shwifty.tex.utilities.Utilities;
import com.shwifty.tex.utilities.Constants;
import com.shwifty.tex.utilities.jpa.QueryOrder;
import com.shwifty.tex.utilities.jpa.TorrentCategory;
import com.shwifty.tex.utilities.search.SearchPing;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SearchHandler {
    private static ArrayList<Future> searchPings;
    private static String Query = "*";
    private static int Category = TorrentCategory.All;
    private static QueryOrder Order = QueryOrder.BySeeds;

    public SearchHandler(String mQuery, int mCategory, QueryOrder mOrder) {
        SearchHandler.Query = mQuery;
        SearchHandler.Category = mCategory;
        SearchHandler.Order = mOrder;
    }

    public void execute(Context context) {
        if (!Prefs.SEARCH_ENABLED) return;
        Object LOCK = new Object();


        Thread thread = new Thread(() -> {


            if (Constants.UrlTpb.length == 0) {
                Thread syncMirrorUrlsThread = new Thread(() -> FirebaseHandler.getMirrorUrls(LOCK));
                syncMirrorUrlsThread.start();
                lock(LOCK);
            }

            ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(Constants.UrlTpb.length);


            if (!Constants.isFastestSet()) {
                Utilities.pingProxies(context);
                while (!Constants.isFastestSet()) {

                }
            }

            if (searchPings != null) {
                for (Future ping : searchPings) {
                    ping.cancel(true);
                }
            }
            searchPings = new ArrayList<>();

            int proxyCount = Constants.UrlTpb.length;
            for (int x = 0; x < proxyCount; x++) {
                SearchPing searchPing = new SearchPing(x, SearchHandler.Query, 0, SearchHandler.Category, SearchHandler.Order, context);
                Future searchTask = threadPoolExecutor.submit(searchPing);
                searchPings.add(searchTask);
            }
        });
        thread.start();
    }

    public static void cancel() {
        if (searchPings != null) {
            for (Future ping : searchPings) {
                if(ping == null)return;
                ping.cancel(true);
            }
        }
    }

    public static String getQuery() {
        return SearchHandler.Query;
    }

    public static int getCategory() {
        return SearchHandler.Category;
    }

    public static QueryOrder getOrder() {
        return Order;
    }

    private static void lock(Object LOCK) {
        synchronized (LOCK) {
            try {
                LOCK.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
