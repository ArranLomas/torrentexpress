package com.shwifty.tex.handlers;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.MediaPlayer;

public class VLCHandler {
    private static LibVLC libVLC;
    private static MediaPlayer mediaPlayer;
    private static boolean boundToActivity;
    private static float lastPosition;
    private static boolean playing = true;
    private static String subtitltesPath;


    public static LibVLC getLibVLC() {
        return libVLC;
    }

    public static void setLibVLC(LibVLC libVLC) {
        VLCHandler.libVLC = libVLC;
    }

    public static MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public static void setMediaPlayer(MediaPlayer mediaPlayer) {
        VLCHandler.mediaPlayer = mediaPlayer;
    }

    public static boolean isBoundToActivity() {
        return boundToActivity;
    }

    public static void setBoundToActivity(boolean boundToActivity) {
        VLCHandler.boundToActivity = boundToActivity;
    }

    public static void savePosition(){
        lastPosition = mediaPlayer.getPosition();
    }

    public static float getLastPosition() {
        return lastPosition;
    }

    public static void clearLastPosition(){
        lastPosition = 0.0f;
    }


    public static boolean isPlaying() {
        return playing;
    }

    public static void setPlaying(boolean playing) {
        VLCHandler.playing = playing;
    }

    public static void bindSubtitles(){
        mediaPlayer.setSubtitleFile(subtitltesPath);
    }


    public static String getSubtitltesPath() {
        return subtitltesPath;
    }

    public static void setSubtitltesPath(String subtitltesPath) {
        VLCHandler.subtitltesPath = subtitltesPath;
    }

    public static boolean hasSubtitltes(){
        return subtitltesPath != null;
    }
}
