package com.shwifty.tex.handlers;

import android.content.Context;
import com.shwifty.tex.utilities.CompletedVideoServer;
import com.shwifty.tex.utilities.VideoStreamServer;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerHandler {
    private VideoStreamServer videoStreamServer;
    private CompletedVideoServer completedVideoServer;
    public static int streamPort = 9090;
    public static int completedPort = 9191;


    public void startStreamServer(Context context) {
        try {
            ServerSocket s = new ServerSocket(0);
            streamPort = s.getLocalPort();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (videoStreamServer == null) {
            videoStreamServer = new VideoStreamServer(streamPort, context);
        } else {
            videoStreamServer.stop();
            videoStreamServer = new VideoStreamServer(streamPort, context);
        }

        try {
            videoStreamServer.startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startCompletedServer(Context context) {
        try {
            ServerSocket s = new ServerSocket(0);
            completedPort = s.getLocalPort();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (completedVideoServer == null) {
            completedVideoServer = new CompletedVideoServer(completedPort, context);
        } else {
            completedVideoServer.stop();
            completedVideoServer = new CompletedVideoServer(completedPort, context);
        }

        try {
            completedVideoServer.startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
