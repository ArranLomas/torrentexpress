package com.shwifty.tex.handlers;

import android.content.Context;

import com.shwifty.tex.utilities.Constants;
import com.shwifty.tex.utilities.Prefs;
import com.shwifty.tex.utilities.Utilities;
import com.shwifty.tex.utilities.jpa.QueryOrder;
import com.shwifty.tex.utilities.jpa.TorrentCategory;
import com.shwifty.tex.utilities.search.LoadMorePing;
import com.shwifty.tex.utilities.search.SearchPing;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by arran on 6/01/2017.
 */

public class LoadMoreHandler {
    private static ArrayList<Future> loadMorePings;
    private static String Query = "*";
    private static int Category = TorrentCategory.All;
    private static QueryOrder Order = QueryOrder.BySeeds;
    private static int mPage = 0;
    private TPBHandler tpbHandler = new TPBHandler();
    private static int previousSize;
    private static boolean loading = false;


    public boolean isLoading() {
        return loading;
    }

    public static void setLoading(boolean loading) {
        LoadMoreHandler.loading = loading;
    }

    public LoadMoreHandler(String mQuery, int mCategory, QueryOrder mOrder, boolean resetPage) {
        LoadMoreHandler.Query = mQuery;
        LoadMoreHandler.Category = mCategory;
        LoadMoreHandler.Order = mOrder;
        if(resetPage)mPage = 0;
        previousSize = tpbHandler.getTpbTorrents().size();
    }

    private void preExecute(){
        mPage++;
        loading = true;
    }

    public void execute(Context context) {
        preExecute();
        if (!Prefs.SEARCH_ENABLED) return;
        Object LOCK = new Object();


        Thread thread = new Thread(() -> {


            if (Constants.UrlTpb.length == 0) {
                Thread syncMirrorUrlsThread = new Thread(() -> FirebaseHandler.getMirrorUrls(LOCK));
                syncMirrorUrlsThread.start();
                lock(LOCK);
            }

            ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(Constants.UrlTpb.length);


            if (loadMorePings != null) {
                for (Future ping : loadMorePings) {
                    ping.cancel(true);
                }
            }
            loadMorePings = new ArrayList<>();

            int proxyCount = Constants.UrlTpb.length;
            for (int x = 0; x < proxyCount; x++) {
                LoadMorePing loadMorePing = new LoadMorePing(x, LoadMoreHandler.Query, LoadMoreHandler.mPage, LoadMoreHandler.Category, LoadMoreHandler.Order, context);
                Future loadMoreTask = threadPoolExecutor.submit(loadMorePing);
                loadMorePings.add(loadMoreTask);
            }
        });
        thread.start();
    }

    public void cancel() {
        if (loadMorePings != null) {
            for (Future ping : loadMorePings) {
                ping.cancel(true);
            }
        }
        loading = false;
    }



    public static String getQuery() {
        return LoadMoreHandler.Query;
    }

    public static int getCategory() {
        return LoadMoreHandler.Category;
    }

    private static void lock(Object LOCK) {
        synchronized (LOCK) {
            try {
                LOCK.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static int getPreviousSize() {
        return previousSize;
    }
}
