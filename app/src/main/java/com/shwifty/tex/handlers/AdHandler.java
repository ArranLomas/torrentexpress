//package com.shwifty.tex.handlers;
//
//import android.content.Context;
//
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.InterstitialAd;
//
//public class AdHandler {
//    private static InterstitialAd ad;
//    private static String testDeviceID;
//
//    public static void createAd(Context context, String adID, String testDeviceID) {
//        // Create an ad.
//        AdHandler.testDeviceID = testDeviceID;
//        ad = new InterstitialAd(context);
//        ad.setAdUnitId(adID);
//
//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice(testDeviceID).build();
//
//        // Load the interstitial ad.
//        ad.loadAd(adRequest);
//    }
//
//    public static InterstitialAd getAd() {
//        return ad;
//    }
//
//    public static void recreateAd(){
//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice(testDeviceID).build();
//
//        // Load the interstitial ad.
//        ad.loadAd(adRequest);
//    }
//}
