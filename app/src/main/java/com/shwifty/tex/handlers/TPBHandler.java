package com.shwifty.tex.handlers;


import com.shwifty.tex.utilities.jpa.TPBTorrent;

import java.util.ArrayList;

public class TPBHandler {
    private static boolean torrentsSet;
    private static ArrayList<TPBTorrent> tpbTorrents = new ArrayList<>();

    public ArrayList<TPBTorrent> getTpbTorrents() {
        return tpbTorrents;
    }

    public void setTpbTorrents(ArrayList<TPBTorrent> tpbTorrents, String query, int category) {
        String lastQuery = SearchHandler.getQuery();
        int lastCategory = SearchHandler.getCategory();
        if(query.equalsIgnoreCase(lastQuery) && category == lastCategory){
            TPBHandler.tpbTorrents.clear();
            TPBHandler.tpbTorrents.addAll(tpbTorrents);
        }

    }

    public void addMore(ArrayList<TPBTorrent> tpbTorrents) {
        TPBHandler.tpbTorrents.addAll(tpbTorrents);
    }

    public boolean isTorrentsSet() {
        return torrentsSet;
    }

    public void setTorrentsSet(boolean torrentsSet) {
        TPBHandler.torrentsSet = torrentsSet;
    }

    public static void stopWaitingForTorrents(){
        torrentsSet = true;
    }
}
