package com.shwifty.tex.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shwifty.tex.R;
import com.shwifty.tex.adapters.DownloadManagerAdapter;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.SpacesItemDecoration;
import com.shwifty.tex.utilities.Utilities;


import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadManagerFragment extends Fragment {
    @BindView(R.id.downloadCardList)
    RecyclerView mRecyclerView;
    @BindString(R.string.title_activity_download)
    String activtyTitle;
    @BindView(R.id.loadingTorrentsSpinner)
    ProgressBar loadingTorrentsSpinner;
    @BindView(R.id.loadingTorrents)
    TextView loadingTorrents;
    private boolean uiHandlerRunning;
    private Thread torrentUpdateThread;

    private DownloadManagerAdapter mAdapter;

    private static boolean inFocus = false;


    public DownloadManagerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_download_manager, container, false);
        ButterKnife.bind(this, view);

        DownloadHandler downloadHandler = new DownloadHandler(getActivity());
        if (downloadHandler.isTorrentsLoaded()) {
            loadingTorrents.setVisibility(View.GONE);
            loadingTorrentsSpinner.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
        }


        mAdapter = new DownloadManagerAdapter(getActivity());
        mAdapter.setItems(downloadHandler.getTorrents());
        mRecyclerView.setAdapter(mAdapter);
        RecyclerView.ItemDecoration decoration = new SpacesItemDecoration(getActivity(), R.dimen.item_offset);
        mRecyclerView.addItemDecoration(decoration);

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && Utilities.isTablet(getActivity())) {
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }
        return view;
    }

    @Override
    public void onResume() {
        inFocus = true;
        setupTorrentUpdateThread();
        super.onResume();
    }

    @Override
    public void onPause() {
        uiHandlerRunning = false;
        inFocus = false;
        super.onPause();
    }

    public void removeTorrent(Torrent torrent) {
        if (mAdapter != null) {
            mAdapter.removeTorrent(torrent);
        }
    }

    public DownloadManagerAdapter getmAdapter() {
        return mAdapter;
    }

    public void updateTorrentData() {
        if (inFocus && mAdapter != null && mAdapter.getItemCount() > 0) {
            mAdapter.updateFinished();
        }
    }

    public void notifyTorrentsAdded() {
        if (getmAdapter() == null) return;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            getmAdapter().notifyDataSetChanged();
            loadingTorrentsSpinner.setVisibility(View.GONE);
            loadingTorrents.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            if (mAdapter == null) {
                mAdapter = new DownloadManagerAdapter(getActivity());
            }
            DownloadHandler downloadHandler = new DownloadHandler(getActivity());
            mAdapter.setItems(downloadHandler.getTorrents());
            mRecyclerView.setAdapter(mAdapter);
        });
    }

    private void setupTorrentUpdateThread() {
        uiHandlerRunning = true;
        torrentUpdateThread = new Thread(() -> {
            while (uiHandlerRunning) {
                getActivity().runOnUiThread(()->{
                    updateTorrentData();
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        torrentUpdateThread.start();
    }
}
