package com.shwifty.tex.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.adapters.SearchAdapter;
import com.shwifty.tex.handlers.LoadMoreHandler;
import com.shwifty.tex.handlers.SearchHandler;
import com.shwifty.tex.handlers.TPBHandler;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.Prefs;
import com.shwifty.tex.utilities.SpacesItemDecoration;
import com.shwifty.tex.utilities.Utilities;
import com.shwifty.tex.utilities.Constants;
import com.shwifty.tex.utilities.jpa.QueryOrder;
import com.shwifty.tex.utilities.jpa.TPBTorrent;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchFragment extends Fragment {
    @BindView(R.id.searchCardList)
    RecyclerView mRecyclerView;
    @BindView(R.id.searchSwipeRefresh)
    SwipeRefreshLayout mSwipeLayout;
    @BindView(R.id.loadMoreProgress)
    ProgressBar loadMoreSpinner;
    @BindView(R.id.search_disabled_hint)
    TextView searchDisabledHint;
    @BindString(R.string.app_name)
    String appName;


    private int visibleThreshold = 1;
    private int actionBarHeight = 0;

    private SearchAdapter mAdapter;
    private TPBHandler tpbHandler = new TPBHandler();
    private LoadMoreHandler loadMoreHandler;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);

        if (!Prefs.SEARCH_ENABLED) {
            if (searchDisabledHint != null) {
                searchDisabledHint.setOnClickListener(view1 -> {
                    Toast.makeText(getActivity(), "downloading", Toast.LENGTH_LONG).show();
                    MainApplication.downloadSearchEnabledAPK(getActivity());
                });
            }

            mAdapter = new SearchAdapter(this);
            searchDisabledHint.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mSwipeLayout.setVisibility(View.GONE);
            loadMoreSpinner.setVisibility(View.GONE);


            return view;
        }


        getActivity().runOnUiThread(() -> loadMoreSpinner.setVisibility(View.GONE));
        mSwipeLayout.setOnRefreshListener(() -> {
            MainActivity activity = (MainActivity) getActivity();
            mSwipeLayout.setProgressViewOffset(true, 0, (actionBarHeight + (actionBarHeight / 2)));
            activity.refreshTorrents();
            waitForTorrents();
        });


        mAdapter = new SearchAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        RecyclerView.ItemDecoration decoration = new SpacesItemDecoration(getActivity(), R.dimen.item_offset);
        mRecyclerView.addItemDecoration(decoration);

        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }


//        executeLoadMoreTorrents(SearchHandler.getQuery(), SearchHandler.getCategory(), false);
        loadMoreHandler = new LoadMoreHandler(SearchHandler.getQuery(), SearchHandler.getCategory(), SearchHandler.getOrder(), false);


        mSwipeLayout.setProgressViewOffset(true, 0, (actionBarHeight + (actionBarHeight / 2)));

        GridLayoutManager mLayoutManager;
        if (Utilities.isTablet(getActivity())) {
            mLayoutManager = new GridLayoutManager(getActivity(), 2);
        } else {
            mLayoutManager = new GridLayoutManager(getActivity(), 1);
        }
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSwipeLayout.post(() -> mSwipeLayout.setRefreshing(true));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean hideToolBar = false;

            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 20) {
                    hideToolBar = true;

                } else if (dy < -5) {
                    hideToolBar = false;
                }

                GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = mAdapter.getItemCount();
                int lastVisibleItem = layoutManager.findLastVisibleItemPosition();


                if (totalItemCount < Constants.ITEMS_PER_PAGE) {
                    return;
                }

                if (!loadMoreHandler.isLoading() && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    getActivity().runOnUiThread(() -> loadMoreSpinner.setVisibility(View.VISIBLE));
                    executeLoadMoreTorrents(SearchHandler.getQuery(), SearchHandler.getCategory(), false);
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (hideToolBar) {
                    MainActivity activity = (MainActivity) getActivity();
                    activity.hideActionBar();
                } else {
                    MainActivity activity = (MainActivity) getActivity();
                    activity.showActionBar();
                }
            }


        });

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && Utilities.isTablet(getActivity())) {
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }

        waitForTorrents();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void stopRefreshing() {
        Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(() -> mSwipeLayout.setRefreshing(false));

    }


    public void waitForTorrents() {
        if (!Prefs.SEARCH_ENABLED) {
            stopRefreshing();
            return;
        }
        Thread thread = new Thread(() -> {
            while (!tpbHandler.isTorrentsSet()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mAdapter.setItems(tpbHandler.getTpbTorrents());
            stopRefreshing();
            if (tpbHandler.getTpbTorrents().size() == 0) {
                checkReasonForNoResults();
            }
        });
        thread.start();
    }

    public void notifyFinishedLoadingMoreTorrents(int previousSize) {
        getActivity().runOnUiThread(() -> loadMoreSpinner.setVisibility(View.GONE));

        ArrayList<TPBTorrent> torrents = tpbHandler.getTpbTorrents();
        if (torrents.size() == previousSize) {
            checkReasonForNoResults();
        }
        mAdapter.setItems(tpbHandler.getTpbTorrents());
        mAdapter.notifyDataSetChanged();
    }

    private void checkReasonForNoResults() {
        if (getActivity() == null) return;
        int networkStatus = NetworkUtil.getConnectivityStatus(getActivity());
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            if (networkStatus != NetworkUtil.TYPE_NOT_CONNECTED) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getActivity(), "Please check your internet connection and try again", Toast.LENGTH_LONG).show();
                    });

                }
                return;
            }
            Toast.makeText(getActivity(), "Sorry No More Results", Toast.LENGTH_LONG).show();
        });

    }

    public void stopLoadingFiles() {
        mAdapter.stopLoadingFiles();
    }

    public void downloadClick() {
        mAdapter.downloadClicked(mAdapter.getSelectedMagnet(), mAdapter.getSelectedPosition());
    }

    public void streamClick() {
        mAdapter.streamClicked(mAdapter.getSelectedMagnet(), mAdapter.getSelectedPosition());
    }

    public void cancelLoadMoreTorrents() {
        if (loadMoreHandler == null) return;
        loadMoreHandler.cancel();
    }


    private void executeLoadMoreTorrents(String mQuery, int searchCategory, boolean resetPage) {
        cancelLoadMoreTorrents();

        TPBHandler tpbHandler = new TPBHandler();
        tpbHandler.setTorrentsSet(false);

        loadMoreHandler = new LoadMoreHandler(mQuery, searchCategory, QueryOrder.BySeeds, resetPage);
        loadMoreHandler.execute(getActivity());
    }
}
