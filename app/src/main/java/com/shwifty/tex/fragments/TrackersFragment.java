package com.shwifty.tex.fragments;

/**
 * Created by arran on 18/01/2017.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.shwifty.tex.R;
import com.shwifty.tex.dialogs.OpenFileDialog;
import com.shwifty.tex.utilities.Format;

import java.util.ArrayList;

import go.libtorrent.Libtorrent;
import go.libtorrent.Tracker;

public class TrackersFragment extends Fragment {
    View v;
    View header;
    TextView dhtLast;
    TextView pex;
    TextView lpd;
    View add;
    ArrayList<Tracker> ff = new ArrayList<>();
    Files files;
    ListView list;

    class Files extends BaseAdapter {
        @Override
        public int getCount() {
            return ff.size();
        }

        @Override
        public Tracker getItem(int i) {
            return ff.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            if (view == null) {

                view = inflater.inflate(R.layout.torrent_trackers_item, viewGroup, false);
            }
            final long t = getArguments().getLong("torrent");
            View trash = view.findViewById(R.id.torrent_trackers_trash);
            trash.setOnClickListener(view1 -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.delete_tracker);
                builder.setMessage(ff.get(i).getAddr() + "\n\n" + getContext().getString(R.string.are_you_sure));
                builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                    dialog.cancel();
                    Libtorrent.torrentTrackerRemove(t, ff.get(i).getAddr());
                    update();
                });
                builder.setNegativeButton(R.string.no, (dialog, which) -> dialog.cancel());
                builder.show();
            });
            TextView url = (TextView) view.findViewById(R.id.torrent_trackers_url);
            TextView lastAnnounce = (TextView) view.findViewById(R.id.torrent_trackers_lastannounce);
            TextView nextAnnounce = (TextView) view.findViewById(R.id.torrent_trackers_nextannounce);
            TextView lastScrape = (TextView) view.findViewById(R.id.torrent_trackers_lastscrape);
            Tracker f = getItem(i);
            url.setText(f.getAddr());
            String scrape = Format.formatDate(f.getLastScrape());
            if (f.getLastScrape() != 0)
                scrape += " (S:" + f.getSeeders() + " L:" + f.getLeechers() + " D:" + f.getDownloaded() + ")";
            String ann = Format.formatDate(f.getLastAnnounce());
            if (f.getError() != null && !f.getError().isEmpty()) {
                ann += " (" + f.getError() + ")";
            } else {
                if (f.getLastAnnounce() != 0)
                    ann += " (P:" + f.getPeers() + ")";
            }
            setText(lastAnnounce, ann);
            setDate(nextAnnounce, f.getNextAnnounce());
            setText(lastScrape, scrape);
            return view;
        }
    }

    public static TrackersFragment newInstance(long torrentID) {
        TrackersFragment myFragment = new TrackersFragment();

        Bundle args = new Bundle();
        args.putLong("torrent", torrentID);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        list = new ListView(getContext());
        v = list;
        header = inflater.inflate(R.layout.torrent_trackers, list, false);
        files = new Files();
        list.addHeaderView(header);
        list.setAdapter(files);
        list.setEmptyView(v.findViewById(R.id.empty_list));
        add = header.findViewById(R.id.torrent_trackers_add);
        dhtLast = (TextView) header.findViewById(R.id.torrent_trackers_dht_last);
        pex = (TextView) header.findViewById(R.id.torrent_trackers_pex);
        lpd = (TextView) header.findViewById(R.id.torrent_trackers_lpd);
        final long t = getArguments().getLong("torrent");
        add.setOnClickListener(view -> {
            final OpenFileDialog.EditTextDialog e = new OpenFileDialog.EditTextDialog(getContext());
            e.setTitle(getContext().getString(R.string.add_tracker));
            e.setText("");
            e.setPositiveButton((dialog, which) -> {
                Libtorrent.torrentTrackerAdd(t, e.getText());
                update();
            });
            e.show();
        });
        update();
        return list;
    }

    //    @Override
    public void update() {
        final long t = getArguments().getLong("torrent");
        ff.clear();
        long l = Libtorrent.torrentTrackersCount(t);
        for (long i = 0; i < l; i++) {
            Tracker tt = Libtorrent.torrentTrackers(t, i);
            String url = tt.getAddr();
            if (url.equals("PEX")) {
                setText(pex, Libtorrent.torrentActive(t) ? tt.getPeers() + "" : "");
                continue;
            }
            if (url.equals("LPD")) {
                setText(lpd, Libtorrent.torrentActive(t) ? tt.getPeers() + "" : "");
                continue;
            }
            if (url.equals("DHT")) {
                String str = Format.formatDate(tt.getLastAnnounce());
                if (tt.getError() != null && !tt.getError().isEmpty())
                    str += " (" + tt.getError() + ")";
                else {
                    if (tt.getLastAnnounce() != 0)
                        str += " (P: " + tt.getPeers() + ")";
                }
                setText(dhtLast, str);
                continue;
            }
            ff.add(tt);
        }
        files.notifyDataSetChanged();
    }

    public void setText(View v, String text) {
        TextView t = (TextView) v;
        if (text.isEmpty()) {
            t.setEnabled(false);
            t.setText(R.string.n_a);
        } else {
            t.setEnabled(true);
            t.setText(text);
        }
    }

    public void setDate(View v, long d) {
        String s = Format.formatDate(d);
        setText(v, s);
    }


//    @Override
//    public void close() {
//    }
}
