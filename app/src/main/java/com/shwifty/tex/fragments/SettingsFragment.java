package com.shwifty.tex.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.dialogs.SetDownloadPathDialog;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.Prefs;
import com.shwifty.tex.utilities.Utilities;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsFragment extends Fragment {
    @BindView(R.id.wifiOnlycheckbox)
    CheckBox wifiOnlyCheckBox;
    @BindView(R.id.cleanStorageTextbox)
    CheckBox cleanStorageTextBox;
    @BindView(R.id.playSoundCheckbox)
    CheckBox playSoundCheckbox;
    @BindView(R.id.storagePathTitle)
    TextView pathTitle;
    @BindView(R.id.currentPath)
    TextView storagePath;
    @BindView(R.id.storagePathLayout)
    RelativeLayout storagePathLayout;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        ButterKnife.bind(this, view);
        wifiOnlyCheckBox.setChecked(Prefs.downloadOnWifiOnly);
        cleanStorageTextBox.setChecked(Prefs.cleanStorage);
        playSoundCheckbox.setChecked(Prefs.playNotificationSound);

        String currentPath = Prefs.downloadPath;
        storagePath.setText(currentPath);

        wifiOnlyCheckBox.setOnCheckedChangeListener((compoundButton, checked) -> {
            Prefs.downloadOnWifiOnly = checked;
            Prefs.saveWIFI_ONLYInSP(getActivity());
            MainApplication mainApplication = MainApplication.getInstance();

            int status = NetworkUtil.getConnectivityStatus(getActivity());
            if (status == NetworkUtil.TYPE_NOT_CONNECTED) {
                mainApplication.noConnectionLibtorrent();
                return;
            }
            if (status == NetworkUtil.TYPE_MOBILE) {
                if (checked) {
                    mainApplication.pauseLibtorrent();
                } else {
                    mainApplication.resumeLibtorrent();
                }
                return;
            }
            if (status == NetworkUtil.TYPE_WIFI) {
                mainApplication.resumeLibtorrent();
            }
        });

        cleanStorageTextBox.setOnCheckedChangeListener((compoundButton, checked) -> {
            Prefs.cleanStorage = checked;
            Prefs.saveCleanStorageInSP(getActivity());
        });

        playSoundCheckbox.setOnCheckedChangeListener((compoundButton, checked) -> {
            Prefs.playNotificationSound = checked;
            Prefs.savePlaySoundInSP(getActivity());
        });


        storagePathLayout.setOnClickListener(view1 -> {
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                MainActivity mainActivity = (MainActivity) activity;
                boolean hasPermission = mainActivity.hasPermission();
                if (!hasPermission) {
                    mainActivity.setPermissionRequestFrom(MainActivity.SHOW_FILE_BROWSER_ID);
                    mainActivity.requestPermission();
                } else {
                    showFileBrowser();
                }
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void showFileBrowser() {
        final String previousPath = Prefs.downloadPath;
        final SetDownloadPathDialog f = new SetDownloadPathDialog(getContext());
        String path = Prefs.downloadPath;
        File file = new File(path);

        f.setCurrentPath(file);
        f.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            File newDirectory = f.getCurrentPath();

            if (previousPath.equalsIgnoreCase(newDirectory.getPath())) return;

            Utilities.changeStorage(getActivity(), newDirectory);
            storagePath.setText(Prefs.downloadPath);
        });
        f.show();
    }
}