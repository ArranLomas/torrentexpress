package com.shwifty.tex.utilities;

import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

public class CompletedVideoServer extends NanoHTTPD {
    private Context context;

    public CompletedVideoServer(int port, Context context) {
        super(port);
        this.context = context;
    }

    public void startServer() throws IOException {
        this.start();
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        Map<String, String> headers = session.getHeaders();

        return serveFile(uri, headers);
    }


    /**
     * Serves file from homeDir and its' subdirectories (only). Uses only URI,
     * ignores all headers and HTTP parameters.
     */
    private Response serveFile(String stringUri, Map<String, String> header) {
        String range = null;

        String mimeType;
        Uri parsedURI = Uri.parse(stringUri);
        String path = parsedURI.getPath();

        if(path == null){
            return createResponse(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, null);
        }

        File file = new File(stringUri);

        if(!file.exists()){
            return createResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, null);
        }


        mimeType = Utilities.getMimeType(file, context);

        Iterator it = header.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String key = (String) pair.getKey();
            if (key.contains("range")) {
                range = (String) pair.getValue();
            }
            it.remove();
        }

        long startFrom = 0;
        long endAt = -1;
        if (range != null) {
            if (range.startsWith("bytes=")) {
                range = range.substring("bytes=".length());
                int minus = range.indexOf('-');
                try {
                    if (minus > 0) {
                        String start = range.substring(0, minus);
                        String end = range.substring(minus + 1);
                        startFrom = Long.parseLong(start);
                        endAt = Long.parseLong(end);
                    }
                } catch (NumberFormatException ignored) {
                   ignored.printStackTrace();
                }
            }
        }

        // Change return code and add Content-Range header when skipping is
        // requested
        long fileLen = file.length();

        if (range == null) {
            return responseOK(file, mimeType, fileLen);
        }
        if (startFrom >= fileLen) {
            return responseRangeNotSatisfiable(fileLen);
        }

        if (endAt < 0) {
            endAt = fileLen - 1;
        }
        long newLen = endAt - startFrom + 1;
        if (newLen < 0) {
            newLen = 0;
        }

        return responsePartialContent(file,
                startFrom, endAt, newLen, mimeType, fileLen);

    }



    private Response responseOK(File file, String mimeType, long fileLen) {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return createResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, null);
        }


        Response res = createResponse(Response.Status.OK, mimeType, fileInputStream);

        res.addHeader("Content-Length", "" + fileLen);
        res.addHeader("Content-Type", "" + mimeType);

        return res;
    }

    private Response responseRangeNotSatisfiable(long fileLen) {
        Response res = createResponse(Response.Status.RANGE_NOT_SATISFIABLE,
                NanoHTTPD.MIME_PLAINTEXT, null);
        res.addHeader("Content-Range", "bytes 0-0/" + fileLen);
        return res;
    }

    private Response responsePartialContent(File file,
                                            long startFrom, long endAt, long dataLen,
                                            String mimeType, long fileLen) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if(fileInputStream== null){
            return createResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, null);
        }
        try {
            fileInputStream.skip(startFrom);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Response res = createResponse(Response.Status.PARTIAL_CONTENT, mimeType, fileInputStream);
        res.addHeader("Content-Length", "" + dataLen);
        res.addHeader("Content-Range", "bytes " + startFrom + "-" + endAt + "/" + fileLen);
        res.addHeader("Content-Type", "" + mimeType);
        return res;

    }


    //Announce that the file server accepts partial content requests
    private Response createResponse(Response.Status status, String mimeType,
                                    InputStream message) {
        Response res = new Response(status, mimeType, message);
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

}
