package com.shwifty.tex.utilities;

import android.support.annotation.NonNull;

import com.shwifty.tex.activities.MainApplication;

import java.io.IOException;
import java.io.InputStream;

class MyInputStream extends InputStream {
    private int torrentID;
    private int fileIndex;
    private long offset;
    private long bytesLeft;
    private static final long BUFFER = 128 << 10;
    private long available;

    MyInputStream(int torrentID, int fileIndex, long offset, long bytesLeft) {
        this.torrentID = torrentID;
        this.fileIndex = fileIndex;
        this.offset = offset;
        this.bytesLeft = bytesLeft;
        this.available = bytesLeft;
    }

    @Override
    public int available() throws IOException {
        return (int) available;
    }

    @Override
    public int read(@NonNull byte[] b, int off, int length) throws IOException {
        if (bytesLeft <= 0) {
            return -1;
        }
        long len = bytesLeft;
        if (len > BUFFER) {
            len = BUFFER;
        }
        if(len > b.length){
            len = b.length;
        }

        MainApplication mainApplication = MainApplication.getInstance();
        if(mainApplication.isPaused())return 0;

        byte[] bytes = mainApplication.readBytes(torrentID, fileIndex, offset, len);
        System.arraycopy(bytes, 0, b, off, bytes.length);
        offset += bytes.length;
        bytesLeft -= bytes.length;
        return bytes.length;
    }

    @Override
    public int read() throws IOException {
        byte[] b = new byte[1];
        int n = read(b);

        if (n == 1) {
            return b[0];
        }

        return n;
    }
}
