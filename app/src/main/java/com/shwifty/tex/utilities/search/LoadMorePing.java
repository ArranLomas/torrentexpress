package com.shwifty.tex.utilities.search;

import android.content.Context;

import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.handlers.LoadMoreHandler;
import com.shwifty.tex.handlers.SearchHandler;
import com.shwifty.tex.handlers.TPBHandler;
import com.shwifty.tex.utilities.Constants;
import com.shwifty.tex.utilities.jpa.Jpa;
import com.shwifty.tex.utilities.jpa.Query;
import com.shwifty.tex.utilities.jpa.QueryOrder;
import com.shwifty.tex.utilities.jpa.TPBTorrent;
import com.shwifty.tex.utilities.jpa.TorrentCategory;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by arran on 6/01/2017.
 */

public class LoadMorePing implements Runnable {
    private TPBHandler tpbHandler = new TPBHandler();
    private int index;
    private static int responses = 0;
    private Context context;
    private String mQuery;
    private int mPage = 0;
    private int mCategory = TorrentCategory.All;
    private QueryOrder mOrder = QueryOrder.BySeeds;

    public LoadMorePing(int index, String mQuery, int mPage, int mCategory, QueryOrder mOrder, Context context) {
        this.index = index;
        this.mQuery = mQuery;
        this.mPage = mPage;
        this.mCategory = mCategory;
        this.mOrder = mOrder;
        this.context = context;
    }

    @Override
    public void run() {
        String url = Constants.UrlTpb[index];

        Query query = new Query(mQuery, mPage, mCategory, mOrder);

        try {
            Document doc = Jsoup.connect(query.TranslateToUrl(index))
                    .timeout(10 * 1000)
                    .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36")
                    .get();

            ArrayList<TPBTorrent> results = ScrapeHTML.scrape(doc);

            if(Thread.currentThread().isInterrupted()){
                return;
            }
            if (!tpbHandler.isTorrentsSet() && results.size() > 0) {
                if(!mQuery.equalsIgnoreCase(LoadMoreHandler.getQuery()) && mCategory != LoadMoreHandler.getCategory()){
                    return;
                }
                postExecute(url, results);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(Thread.currentThread().isInterrupted()){
            return;
        }
        responses++;
        if (responses == Constants.UrlTpb.length) {
            if (!tpbHandler.isTorrentsSet()) {
                if (context instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) context;
                    mainActivity.showUnableToConnectTPBDialog();
                }
            }
        }

    }

    private void postExecute(String url, ArrayList<TPBTorrent> results){
        LoadMoreHandler.setLoading(false);

        Constants.setFastestUrl(url);
        Constants.setFastestUrlIndex(index);
        tpbHandler.addMore(results);
        tpbHandler.setTorrentsSet(true);
        Jpa.LoadImages(results);

        if(context instanceof MainActivity){
            MainActivity mainActivity = (MainActivity) context;
            mainActivity.notifyFinnishedLoadingMoreTorrents(LoadMoreHandler.getPreviousSize());
        }
    }
}
