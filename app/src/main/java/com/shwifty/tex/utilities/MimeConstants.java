package com.shwifty.tex.utilities;

import java.util.Arrays;
import java.util.HashSet;


public class MimeConstants {
    //ALL MUST BE DEFINED IN LOWERCASE
    public static final HashSet<String> chromecastSet= new HashSet<>(Arrays.asList(
            "bmp",
            "gif",
            "jpeg",
            "png",
            "webp",
           "aac",
            "mp3",
            "mp4",
            "wav",
            "webm"
            ));


    public static final HashSet<String> subtitleSet= new HashSet<>(Arrays.asList(
            "srt"
    ));




}
