package com.shwifty.tex.utilities;

import fi.iki.elonen.NanoHTTPD;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class VideoStreamServer extends NanoHTTPD {
    private Context context;

    public VideoStreamServer(int port, Context context) {
        super(port);
        this.context = context;
    }

    public void startServer() throws IOException {
        this.start();
    }


    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        Map<String, String> headers = session.getHeaders();



        return serveFile(uri, headers);
    }

    /**
     * Serves file from homeDir and its' subdirectories (only). Uses only URI,
     * ignores all headers and HTTP parameters.
     */
    private Response serveFile(String stringUri, Map<String, String> header) {
        String range = null;

        int torrentID;
        int torrentFileIndex;
        String mimeType;
        Torrent selectedTorrent = null;
        TorrentFile selectedTorrentFile = null;
        Uri parsedURI = Uri.parse(stringUri);
        String path = parsedURI.getPath();

        String[] parts = path.split("/");

        if(parts.length < 4){
            return createResponse(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, null);
        }

        if(NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED){
            return createResponse(Response.Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, null);
        }

        if(!NetworkUtil.getOKForDownload(context) || MainApplication.getInstance().isPaused()){
            return createResponse(Response.Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, null);
        }

        torrentID = Integer.parseInt(parts[2]);
        torrentFileIndex = Integer.parseInt(parts[4]);


        TorrentHandler torrentHandler = new TorrentHandler();
        ArrayList<Torrent> torrents = torrentHandler.getTorrents();

        for (int x = 0; x < torrents.size(); x++) {
            Torrent t = torrents.get(x);
            if (torrentID == t.getTorrentId()) {
                selectedTorrent = t;
            }
        }


        if (selectedTorrent == null) {
            return createResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, null);
        }

        if(!selectedTorrent.isDownloading()){
            MainApplication.getInstance().startForStream(selectedTorrent);
        }

        ArrayList<TorrentFile> torrentFiles = selectedTorrent.getTorrentFiles();

        for (int x = 0; x < torrentFiles.size(); x++) {
            TorrentFile tf = torrentFiles.get(x);
            if (tf.index == torrentFileIndex) selectedTorrentFile = tf;
        }

        if (selectedTorrentFile == null) {
            return createResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, null);
        }

        String torrentFilePath = selectedTorrentFile.file.getPath();
        String filePath = selectedTorrent.getDirectory() + File.separator + torrentFilePath;
        File fileForMime = new File(filePath);
        mimeType = Utilities.getMimeType(fileForMime, context);

        Iterator it = header.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String key = (String) pair.getKey();
            if (key.contains("range")) {
                range = (String) pair.getValue();
            }
            it.remove();
        }

        long startFrom = 0;
        long endAt = -1;
        if (range != null) {
            if (range.startsWith("bytes=")) {
                range = range.substring("bytes=".length());
                int minus = range.indexOf('-');
                try {
                    if (minus > 0) {
                        String start = range.substring(0, minus);
                        String end = range.substring(minus + 1);
                        startFrom = Long.parseLong(start);
                        endAt = Long.parseLong(end);
                    }
                } catch (NumberFormatException ignored) {
                    ignored.printStackTrace();
                }
            }
        }

        // Change return code and add Content-Range header when skipping is
        // requested
        long fileLen = selectedTorrentFile.file.getLength();

        if (range == null) {
            return responseOK(torrentID, torrentFileIndex, mimeType, fileLen);
        }
        if (startFrom >= fileLen) {
            return responseRangeNotSatisfiable(fileLen);
        }

        if (endAt < 0) {
            endAt = fileLen - 1;
        }
        long newLen = endAt - startFrom + 1;
        if (newLen < 0) {
            newLen = 0;
        }

        return responsePartialContent(torrentID, torrentFileIndex,
                startFrom, endAt, newLen, mimeType, fileLen);

    }



    private Response responseOK(int torrentID, int torrentFileIndex, String mimeType, long fileLen) {
        MyInputStream myInputStream = new MyInputStream(torrentID, torrentFileIndex, 0, fileLen);


        Response res = createResponse(Response.Status.OK, mimeType, myInputStream);

        res.addHeader("Content-Length", "" + fileLen);
        res.addHeader("Content-Type", "" + mimeType);
        return res;
    }

    private Response responseRangeNotSatisfiable(long fileLen) {
        Response res = createResponse(Response.Status.RANGE_NOT_SATISFIABLE,
                NanoHTTPD.MIME_PLAINTEXT, null);
        res.addHeader("Content-Range", "bytes 0-0/" + fileLen);
        return res;
    }

    private Response responsePartialContent(int torrentID, int torrentFileIndex,
                                            long startFrom, long endAt, long dataLen,
                                            String mimeType, long fileLen) {

        MyInputStream myInputStream = new MyInputStream(torrentID, torrentFileIndex, startFrom, dataLen);

        Response res = createResponse(Response.Status.PARTIAL_CONTENT, mimeType, myInputStream);
        res.addHeader("Content-Length", "" + dataLen);
        res.addHeader("Content-Range", "bytes " + startFrom + "-" + endAt + "/" + fileLen);
        res.addHeader("Content-Type", "" + mimeType);
        return res;

    }


    //Announce that the file server accepts partial content requests
    private Response createResponse(Response.Status status, String mimeType,
                                    InputStream message) {
        Response res = new Response(status, mimeType, message);
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

}
