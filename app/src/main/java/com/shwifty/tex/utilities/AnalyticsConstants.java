package com.shwifty.tex.utilities;

/**
 * Created by arran on 5/01/2017.
 */

public class AnalyticsConstants {
    public static final String STREAM_CLICKED_ID = "0";
    public static final String STREAM_CLICKED_NAME = "stream_clicked";
    public static final String STREAM_CLICKED_TYPE = "button";

    public static final String DOWNLOAD_CLICKED_ID = "1";
    public static final String DOWNLOAD_CLICKED_NAME = "download_clicked";
    public static final String DOWNLOAD_CLICKED_TYPE = "button";

    public static final String CHROMECAST_CLICKED_ID = "2";
    public static final String CHROMECAST_CLICKED_NAME = "chromecast_clicked";
    public static final String CHROMECAST_CLICKED_TYPE = "chromecast_icon";

    public static final String MAIN_NAME = "main_activity";
    public static final String START_MAIN = "start_main";

    public static final String SPLASH_NAME = "splash_activity";
    public static final String START_SPLASH = "start_splash";

    public static final String VIDEO_NAME = "video_activity";
    public static final String START_VIDEO = "start_video";

    public static final String FRAG_NAME_SEARCH = "search_fragment";
    public static final String BUNDLE_NAME_SEARCH = "show_search";
    public static final String FRAG_NAME_DOWNLOADS = "downloads_fragment";
    public static final String BUNDLE_NAME_DOWNLOADS= "show_downloads";
    public static final String FRAG_NAME_SETTINGS = "settings_fragment";
    public static final String BUNDLE_NAME_SETTINGS = "show_settings";

    public static final String START_STREAM_NAME = "start_stream";
    public static final String START_DOWNLOAD_NAME = "start_download";


    public static final String START_APK_DOWNLOAD_NAME = "start_APK_download";
    public static final String BUNDLE_START_APK_DOWNLOAD = "start_APK";
    public static final String IGNORE_APK_DOWNLOAD_NAME = "ignore_APK_download";
    public static final String BUNDLE_IGNORE_APK = "ignore_APK";

    public static final String LAUNCH_SEARCH_ENABLED_APP_NAME = "launch_app_enabled";
    public static final String LAUNCH_SEARCH_ENABLED_BUNDLE = "search_enabled";

    public static final String LAUNCH_SEARCH_DISABLED_APP_NAME = "launch_app_disabled";
    public static final String LAUNCH_SEARCH_DISABLED_BUNDLE = "search_disabled";
}
