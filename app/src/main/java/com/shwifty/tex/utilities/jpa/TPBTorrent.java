package com.shwifty.tex.utilities.jpa;

public class TPBTorrent {
    public String Name;
    public String Magnet;
    public String Link;
    public String Uploaded;
    public String Size;
    public String Uled;
    public int Seeds;
    public int Leechers;
    public String CategoryParent;
    public String Category;
    public String ImdbID;
    public String CoverImage;
    public boolean blackoutShowing = false;

    public TPBTorrent() {
    }

    public TPBTorrent(String name, String magnet, String link, String uploaded, String size, String uled, int seeds, int leechers, String categoryParent, String category, String imdbID, String coverImage, boolean blackoutShowing) {
        Name = name;
        Magnet = magnet;
        Link = link;
        Uploaded = uploaded;
        Size = size;
        Uled = uled;
        Seeds = seeds;
        Leechers = leechers;
        CategoryParent = categoryParent;
        Category = category;
        ImdbID = imdbID;
        CoverImage = coverImage;
        this.blackoutShowing = blackoutShowing;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMagnet() {
        return Magnet;
    }

    public void setMagnet(String magnet) {
        Magnet = magnet;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public int getSeeds() {
        return Seeds;
    }

    public void setSeeds(int seeds) {
        Seeds = seeds;
    }

    public void setLeechers(int leechers) {
        Leechers = leechers;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getCoverImage() {
        return CoverImage;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getUploaded() {
        return Uploaded;
    }

    public void setUploaded(String uploaded) {
        Uploaded = uploaded;
    }

    public String getUled() {
        return Uled;
    }

    public void setUled(String uled) {
        Uled = uled;
    }

    public int getLeechers() {
        return Leechers;
    }

    public String getCategoryParent() {
        return CategoryParent;
    }

    public void setCategoryParent(String categoryParent) {
        CategoryParent = categoryParent;
    }

    public String getImdbID() {
        return ImdbID;
    }

    public void setImdbID(String imdbID) {
        ImdbID = imdbID;
    }

    public void setCoverImage(String coverImage) {
        CoverImage = coverImage;
    }

    public boolean isBlackoutShowing() {
        return blackoutShowing;
    }

    public void setBlackoutShowing(boolean blackoutShowing) {
        this.blackoutShowing = blackoutShowing;
    }
}

