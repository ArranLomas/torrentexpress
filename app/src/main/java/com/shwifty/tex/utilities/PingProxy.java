package com.shwifty.tex.utilities;

import android.content.Context;

import com.shwifty.tex.activities.SplashActivity;
import com.shwifty.tex.handlers.SearchHandler;
import com.shwifty.tex.handlers.TPBHandler;
import com.shwifty.tex.utilities.jpa.Jpa;
import com.shwifty.tex.utilities.jpa.TPBTorrent;
import com.shwifty.tex.utilities.search.ScrapeHTML;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;

public class PingProxy implements Runnable {
    private int index;
    private static int responses = 0;
    private Context context;


    public PingProxy(int index, Context context) {
        this.index = index;
        this.context = context;
    }

    @Override
    public void run() {
        String url = Constants.UrlTpb[index];
        String pinger = url + "/search/*/0/*/0";

        try {
            Document doc = Jsoup.connect(pinger)
                    .timeout(10 * 1000)
                    .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36")
                    .get();

            ArrayList<TPBTorrent> results = ScrapeHTML.scrape(doc);


            if (!Constants.isFastestSet() && results.size() > 0) {
                Constants.setFastestUrl(url);
                Constants.setFastestUrlIndex(index);
                Constants.setFastestSet(true);
                TPBHandler tpbHandler = new TPBHandler();
                ArrayList<TPBTorrent> result = ScrapeHTML.scrape(doc);
                tpbHandler.setTpbTorrents(result, SearchHandler.getQuery(), SearchHandler.getCategory());
                tpbHandler.setTorrentsSet(true);
                Jpa.LoadImages(result);
                SplashActivity.notifyLock();
            }
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        responses++;
        if (responses == Constants.UrlTpb.length) {
            SplashActivity.notifyLock();
            if (context instanceof SplashActivity) {
                SplashActivity splashActivity = (SplashActivity) context;
                if (!Constants.isFastestSet()) {
                    splashActivity.showUnableToConnectTPBDialog();
                }
            }
        }
    }

    public static void resetResponseCount() {
        responses = 0;
    }

}
