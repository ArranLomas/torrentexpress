package com.shwifty.tex.utilities.jpa;


import com.shwifty.tex.utilities.search.ScrapeHTML;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Jpa {

    public static ArrayList<TPBTorrent> Search(Query query, int urlIndex) throws IOException, InterruptedException , IndexOutOfBoundsException{
        ArrayList<TPBTorrent> result;
        Document doc;


        doc = Jsoup.connect(query.TranslateToUrl(urlIndex))
                .timeout(10*1000)
                .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36")
                .get();

        result = ScrapeHTML.scrape(doc);

        LoadImages(result);

        return result;
    }

    public static void LoadImages(ArrayList<TPBTorrent> torrents){
        // setup the threadpool
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);

        for (TPBTorrent file : torrents) {
            Runnable worker = new ImageSearch(file);
            executorService.execute(worker);
        }

        // all torrents added to the threadpool
        executorService.shutdown();


        // block until all the threads are finished
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
