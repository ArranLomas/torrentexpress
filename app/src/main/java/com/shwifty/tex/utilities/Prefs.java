package com.shwifty.tex.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

public class Prefs {


    //TODO ALWAYS CHANGE FOR GOOGLE PLAY RELEASE!!!
    public static final boolean SEARCH_ENABLED = true;





    private static String TAG = Prefs.class.getName();
    // preference names
    public static final String KEY_TOP_100 = "pref_top100";
    public static final String KEY_TOP_48H = "pref_top48h";

    public static final String KEY_TOP_AUDIO = "pref_topAudio";
    public static final String KEY_AUDIO_48H = "pref_audio48h";
    public static final String KEY_MUSIC = "pref_Music";
    public static final String KEY_AUDIO_BOOKS = "pref_audioBooks";
    public static final String KEY_SOUND_CLIPS = "pref_soundClips";
    public static final String KEY_FLAC = "pref_flac";
    public static final String KEY_OTHER_MUSIC = "pref_otherMusic";

    public static final String KEY_TOP_VIDEO = "pref_topVideo";
    public static final String KEY_VIDEO_48H = "pref_video48h";
    public static final String KEY_MOVIES = "pref_movies";
    public static final String KEY_MOVIES_DVDR = "pref_moviesDvdr";
    public static final String KEY_MUSIC_VIDEOS = "pref_musicVideos";
    public static final String KEY_MOVIE_CLIPS = "pref_movieClips";
    public static final String KEY_TV_SHOWS = "pref_tvShows";
    public static final String KEY_HANDHELD_VIDEO = "pref_handheldVideo";
    public static final String KEY_HD_MOVIES = "pref_hdMovies";
    public static final String KEY_HD_TV_SHOWS = "pref_hdTvShows";
    public static final String KEY_3D = "pref_3d";
    public static final String KEY_OTHER_VIDEO = "pref_otherVideo";

    public static final String KEY_TOP_GAMES = "pref_topGames";
    public static final String KEY_GAMES_48H = "pref_games48h";
    public static final String KEY_PC_GAMES = "pref_pcGames";
    public static final String KEY_MAC_GAMES = "pref_macGames";
    public static final String KEY_PSX_GAMES = "pref_psxGames";
    public static final String KEY_XBOX_GAMES = "pref_xboxGames";
    public static final String KEY_WII_GAMES = "pref_wiiGames";
    public static final String KEY_HANDHELD_GAMES = "pref_handheldGames";
    public static final String KEY_IOS_GAMES = "pref_iosGames";
    public static final String KEY_ANDROID_GAMES = "pref_androidGames";
    public static final String KEY_OTHER_GAMES = "pref_otherGames";

    public static final String KEY_TOP_APPLICATIONS = "pref_topApplications";
    public static final String KEY_APPLICATIONS_48H = "pref_applications48h";
    public static final String KEY_WINDOWS_APPLICATIONS = "pref_windowsApplications";
    public static final String KEY_MAC_APPLICATIONS = "pref_macApplications";
    public static final String KEY_UNIX_APPLICATIONS = "pref_unixApplications";
    public static final String KEY_HANDHELD_APPLICATIONS = "pref_handheldApplications";
    public static final String KEY_IOS_APPLICATIONS = "pref_iosApplications";
    public static final String KEY_ANDROID_APPLICATIONS = "pref_androidApplications";
    public static final String KEY_OTHER_OS_APPLICATIONS = "pref_otherOsApplications";

    public static final String KEY_TOP_OTHER = "pref_topOther";
    public static final String KEY_OTHER_48H = "pref_other48h";
    public static final String KEY_EBOOKS = "pref_ebooks";
    public static final String KEY_COMICS = "pref_comics";
    public static final String KEY_PICTURES = "pref_pictures";
    public static final String KEY_COVERS = "pref_covers";
    public static final String KEY_PHYSIBLES = "pref_physibles";
    public static final String KEY_OTHER_OTHER = "pref_otherOther";

    public static String DEFAULT_DOWNLOAD_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/Trickl";
    public static final String encoding = "UTF-8";

    public static boolean downloadOnWifiOnly = true;
    public static boolean cleanStorage = true;
    public static boolean playNotificationSound = true;
    public static boolean disclaimerAccepted = false;
    public static String downloadPath = DEFAULT_DOWNLOAD_PATH;

    public static final String TAG_WIFI_ONLY = "shared_prefs_wifiOnlyBool";
    public static final String TAG_CLEAN_STORAGE = "shared_prefs_cleanStorageBool";
    public static final String TAG_DISCLAIMER = "shared_prefs_disclaimer";
    public static final String TAG_DOWNLOADPATH = "shared_prefs_downloadPath";
    public static final String TAG_PLAY_SOUND = "shared_prefs_playSounds";


    public static void loadWIFI_ONLYFromSP(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_WIFI_ONLY, android.content.Context.MODE_PRIVATE);
        downloadOnWifiOnly = preferences.getBoolean(TAG_WIFI_ONLY, true);
    }

    public static void saveWIFI_ONLYInSP(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_WIFI_ONLY, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(TAG_WIFI_ONLY, Prefs.downloadOnWifiOnly);
        editor.apply();
    }


    public static void loadCleanStorageFromSP(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_CLEAN_STORAGE, android.content.Context.MODE_PRIVATE);
        cleanStorage = preferences.getBoolean(TAG_CLEAN_STORAGE, true);
    }

    public static void saveCleanStorageInSP(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_CLEAN_STORAGE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(TAG_CLEAN_STORAGE, Prefs.cleanStorage);
        editor.apply();
    }


    public static void loadPlaySoundFromSP(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_PLAY_SOUND, android.content.Context.MODE_PRIVATE);
        playNotificationSound = preferences.getBoolean(TAG_PLAY_SOUND, true);
    }

    public static void savePlaySoundInSP(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_PLAY_SOUND, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(TAG_PLAY_SOUND, Prefs.playNotificationSound);
        editor.apply();
    }

    public static void loadDisclaimerAccepted(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_DISCLAIMER, android.content.Context.MODE_PRIVATE);
        disclaimerAccepted = preferences.getBoolean(TAG_DISCLAIMER, false);
    }

    public static void saveDisclaimerAccepted(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_DISCLAIMER, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(TAG_DISCLAIMER, Prefs.disclaimerAccepted);
        editor.apply();
    }

    public static void loadDownloadPath(Context context) {
        if (context== null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_DOWNLOADPATH, android.content.Context.MODE_PRIVATE);
        downloadPath = preferences.getString(TAG_DOWNLOADPATH, DEFAULT_DOWNLOAD_PATH);
    }

    public static void saveDownloadPath(Context context) {
        if (context == null)return;
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(Prefs.TAG_DOWNLOADPATH, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TAG_DOWNLOADPATH, Prefs.downloadPath);
        editor.apply();
    }

}


