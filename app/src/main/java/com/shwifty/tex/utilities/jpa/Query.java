package com.shwifty.tex.utilities.jpa;


import com.shwifty.tex.utilities.Constants;

public class Query {
    private QueryOrder Order;
    private int Category;
    private int Page;
    private String Term;
    private boolean Mode48h;

    public Query(String query, int mPage, int category, QueryOrder queryOrder) {
        Term = query;
        Page = mPage;
        Category = category;
        Order = queryOrder;
        Mode48h = false;
    }

    public String TranslateToUrl(int urlIndex) {
        String url;
        String constantUrl = Constants.UrlTpb[urlIndex];

        if (!Mode48h) {
            url =  constantUrl+
                    "/search/" +
                    Term + "/" +
                    Integer.toString(Page) + "/" +
                    Integer.toString(Order.getValue()) + "/" +
                    Integer.toString(Category);
        } else {
            url = constantUrl +
                    "/top/" +
                    Term;
        }
        return url;
    }
}
