package com.shwifty.tex.utilities.jpa;

public class TorrentCategory {

	public static final int All = 0;
    public static final int AllAudio = 100;
    public static final int AllVideo = 200;
    public static final int AllApplication = 300;
    public static final int AllGames = 400;
    public static final int AllPorn = 500;
    public static final int AllOther = 600;
    
    public static final int Music = 101;
    public static final int Audiobooks = 102;
    public static final int Soundclips = 103;
    public static final int FLAC = 104;
    public static final int OtherAudio = 199;

    public static final int Movies = 201;
    public static final int MoviesDVDR = 202;
    public static final int Musicvideos = 203;
    public static final int Movieclips = 204;
    public static final int TVshows = 205;
    public static final int HandheldVideo = 206;
    public static final int HDMovies = 207;
    public static final int HDTVshows = 208;
    public static final int Movies3D = 209;
    public static final int OtherVideo = 299;
    
    public static final int WindowsApplications = 301;
    public static final int MacApplications = 302;
    public static final int UNIXApplications = 303;
    public static final int HandheldApplications = 304;
    public static final int IOSApplications = 305;
    public static final int AndroidApplications = 306;
    public static final int OtherOSApplications = 399;
    
    public static final int PCGames = 401;
    public static final int MacGames = 402;
    public static final int PSxGames = 403;
    public static final int XBOX360Games = 404;
    public static final int WiiGames = 405;
    public static final int HandheldGames = 406;
    public static final int IOSGames = 407;
    public static final int AndroidGames = 408;
    public static final int OtherGames = 499;

    public static final int MoviesPorn = 501;
    public static final int MoviesDVDRPorn = 502;
    public static final int PicturesPorn = 503;
    public static final int GamesPorn = 504;
    public static final int HDMoviesPorn = 505;
    public static final int MovieclipsPorn = 506;
    public static final int OtherPorn = 599;
    
    public static final int Ebooks = 601;
    public static final int Comics = 602;
    public static final int Pictures = 603;
    public static final int Covers = 604;
    public static final int Physibles = 605;
    public static final int OtherOther = 699;
    
    public static String Top100 = "all";
    public static String Top48h = "48hall";
    public static String Audio48h = "48h100";
    public static String Video48h = "48h200";
    public static String Applications48h = "48h300";
    public static String Games48h = "48h400";
    public static String Porn48h = "48h500";
    public static String Other48h = "48h600";
}
