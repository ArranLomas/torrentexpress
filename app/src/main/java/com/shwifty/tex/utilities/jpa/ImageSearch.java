package com.shwifty.tex.utilities.jpa;

import com.shwifty.tex.utilities.Constants;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ImageSearch implements Runnable {
	private final TPBTorrent TPBTorrent;
	private final Pattern pattern;
	
	ImageSearch(TPBTorrent TPBTorrent) {
		this.TPBTorrent = TPBTorrent;
		this.pattern = Pattern.compile(Constants.COVER_IMAGE_REGEX);
	}
	
	public void run() {
		String searchTerm = parseTitle();
		
		try {
			URL url = new URL(Constants.OMDB_URL + searchTerm);
			
			// setup HttpURLConnection
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			
			 // ensure the response code is 200
			 if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				 
				 String line = bufferedReader.readLine();
				 JSONObject object = new JSONObject(line);
				 
				 // get the imageurl and imdbid from the request
				 TPBTorrent.CoverImage = object.getString("Poster");
				 TPBTorrent.ImdbID = object.getString("imdbID");
			 }
		} catch(Exception e) {
			// handle exception
		}
	}

	private String parseTitle() {
		String coverString = TPBTorrent.Name.replace(" ", ".");
		
		Matcher matcher = pattern.matcher(coverString);
		while (matcher.find()) {
			coverString = matcher.group(1);
		}
		
		return coverString;
	}
}
