package com.shwifty.tex.utilities;

import android.content.Context;

import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.ServerHandler;
import com.shwifty.tex.handlers.StreamHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class SetupStream {

    private Torrent torrent;
    private TorrentFile selectedTorrentFile;


    public SetupStream(Torrent torrent, int selectedPosition) {
        this.torrent = torrent;
        selectedTorrentFile = torrent.getTorrentFiles().get(selectedPosition);
    }


    public void setup(Context context) {
        MainApplication.getInstance().startForStream(torrent);

        if (selectedTorrentFile != null) {
            File selectedFile = new File(selectedTorrentFile.file.getPath());
            String ip = NetworkUtil.getIPAddress(true);

            String myURL = "http://" + ip + "/torrentID/" + torrent.getTorrentId() + "/torrentFileIndex/" + selectedTorrentFile.index + "/" + selectedFile.getPath();
            URL streamUrl = null;


            try {
                URL url = new URL(myURL);
                URI uri = new URI("http", null, url.getHost(), ServerHandler.streamPort, url.getPath(), url.getQuery(), null);
                System.out.println("URI " + uri.toString() + " is OK");
                streamUrl = uri.toURL();
                System.out.println("URL " + streamUrl.toString());
            } catch (MalformedURLException e) {
                System.out.println("URL " + myURL + " is a malformed URL");
            } catch (URISyntaxException e) {
                System.out.println("URI " + myURL + " is a malformed URL");
            }
            String torrentFilePath = selectedTorrentFile.file.getPath();
            String filePath = torrent.getDirectory() + File.separator + torrentFilePath;
            File fileForMime = new File(filePath);
            String mimeType = Utilities.getMimeType(fileForMime, context);
            StreamHandler streamHandler = new StreamHandler();
            streamHandler.setupStream(torrent, streamUrl.toString(), mimeType, context);
        }
    }

//    public void cancel() {
//        running = false;
//    }
}
