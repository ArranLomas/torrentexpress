package com.shwifty.tex.utilities;

import java.util.ArrayList;

public class Constants {
    private static int fastestUrlIndex = -1;
    private static String fastestUrl;
    private static boolean fastestSet = false;
    public static final String downloadAPKKey = "host_url";
    public static final String latestVersionKey = "latest_version";

    public static String[] UrlTpb = new String[]{};

    public static void setUrlTpb(ArrayList<String> urls){
        UrlTpb = new String[urls.size()];
        UrlTpb = urls.toArray(UrlTpb);

    }

    public static final String OMDB_URL = "http://www.omdbapi.com/?t=";

    public final static String COVER_IMAGE_REGEX = "(.*?)\\.S?(\\d{1,2})E?(\\d{2})\\.(.*)";

    public static final int ITEMS_PER_PAGE = 30;

    public static String getFastestUrl() {
        return fastestUrl;
    }

    public static void setFastestUrl(String fastestUrl) {
        Constants.fastestUrl = fastestUrl;
    }

    public static boolean isFastestSet() {
        return fastestSet;
    }

    public static void setFastestSet(boolean fastestSet) {
        Constants.fastestSet = fastestSet;
    }

    public static int getFastestUrlIndex() {
        return fastestUrlIndex;
    }

    public static void setFastestUrlIndex(int fastestUrlIndex) {
        Constants.fastestUrlIndex = fastestUrlIndex;
    }
}