package com.shwifty.tex.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.util.TypedValue;
import android.webkit.MimeTypeMap;

import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class Utilities {

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    public static boolean deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        return fileOrDirectory.delete();
    }

    public static void setDownloadInfoStrings(Context context, Torrent torrent) {
        String selectedFilesString = "";
        ArrayList<TorrentFile> files = torrent.getTorrentFiles();
        long selectFileSize = 0;
        for (int x = 0; x < files.size(); x++) {
            TorrentFile file = files.get(x);
            if (file.file.getCheck()) {
                selectedFilesString += file.file.getPath() + ":::";
                selectFileSize += file.file.getLength();
            }
        }

        torrent.setSelectedFilesSize(Format.formatSize(context, selectFileSize));
        torrent.setSelectedFileString(selectedFilesString);
    }

    public static void pingProxies(Context context) {
        Constants.setFastestSet(false);
        int proxyCount = Constants.UrlTpb.length;
        PingProxy.resetResponseCount();

        for (int x = 0; x < proxyCount; x++) {
            Runnable worker = new PingProxy(x, context);
            Thread thread = new Thread(worker);
            thread.start();
        }
    }

    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static int getThemeColor(Context context, int id) {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{id});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;

//        TypedValue tv = new TypedValue();
//        boolean found = context.getTheme().resolveAttribute(id, tv, true);
//        if (found) {
//            ContextCompat.getColor(context, tv.resourceId);
//        } else {
//            return 0;
//        }
    }

    public static void changeStorage(Context context, File newDirectory){
        Prefs.downloadPath = newDirectory.getPath();
        Prefs.saveDownloadPath(context);
    }

    public static String getMimeType(File file, Context context) {
        String mime;
        Uri uri = Uri.parse(file.toURI().toString());

        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mime = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mime;
    }



}

