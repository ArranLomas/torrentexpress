package com.shwifty.tex.utilities.search;


import com.shwifty.tex.utilities.jpa.TPBTorrent;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class ScrapeHTML {
    public static ArrayList<TPBTorrent> scrape(Document doc) {
        ArrayList<TPBTorrent> results = new ArrayList<>();
        // get all table rows
        Elements tableRows = doc.getElementsByTag("tr");
        try {
            for (Element element : tableRows) {

                if (!element.hasClass("header")) {
                    final TPBTorrent TPBTorrent = new TPBTorrent();

                    // first td, lets get the categories
                    Element td1 = element.children().select("td").first();

                    ArrayList<Element> categories = td1.children().select("a");
                    TPBTorrent.CategoryParent = categories.get(0).text();
                    TPBTorrent.Category = categories.get(1).text();

                    // second td, get some more info
                    // get the torrent name
                    Element td2 = element.children().select("td").get(1);
                    Element aTorrentName = td2.children().select("a").first();
                    TPBTorrent.Name = aTorrentName.text();

                    // get the file link
                    TPBTorrent.Link = aTorrentName.attr("href");

                    // get the magnet
                    Element torrentMagnet = td2.children().select("a").get(1);
                    TPBTorrent.Magnet = torrentMagnet.attr("href");

                    // get date, size, and uploader
                    Element details = td2.select("font").first();
                    String torrentInfo = details.text();
                    String[] splitInfo = torrentInfo.split(",");

                    TPBTorrent.Uploaded = splitInfo[0].substring(9);
                    TPBTorrent.Size = splitInfo[1].substring(6);
                    TPBTorrent.Uled = splitInfo[2].substring(9);

                    // third td, get the seeds
                    Element td3 = element.children().select("td").get(2);
                    TPBTorrent.Seeds = Integer.parseInt(td3.text());


                    try {
                        Element td4 = element.children().select("td").get(3);
                        TPBTorrent.Leechers = Integer.parseInt(td4.text());
                    } catch (IndexOutOfBoundsException e) {
                        TPBTorrent.Leechers = -1;
                    }


                    results.add(TPBTorrent);
                }

            }
        } catch (Exception e) {
            //IGNORE
//            e.printStackTrace();
        }
        return results;
    }
}
