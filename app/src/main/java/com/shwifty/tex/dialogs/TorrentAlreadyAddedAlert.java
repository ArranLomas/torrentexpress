package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.handlers.TorrentHandler;

public class TorrentAlreadyAddedAlert extends DialogFragment {
    public static String TAG_TORRENT_MAGNET = "PUNDLE_TORRENT_MAGNET";
    public static String TAG_STREAM_OR_DOWNLOAD= "TAG_STREAM_OR_DOWNLOAD";
    private boolean dialogForStream;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        if(getArguments().get(TAG_TORRENT_MAGNET) == null){
            return null;
        }
        String torrentMagnet = getArguments().get(TAG_TORRENT_MAGNET).toString();
        dialogForStream = getArguments().getBoolean(TAG_STREAM_OR_DOWNLOAD);


        if (dialogForStream){
            return createDialogForStream(context, torrentMagnet);
        }else{
            return createDialogForDownload(context, torrentMagnet);
        }

    }


    private Dialog createDialogForDownload(Context context, String torrentMagnet){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);


        return builder.setTitle(R.string.torrent_already_downloading_dialog_title)
                .setMessage(R.string.torrent_already_downloading_dialog_body)
                .setPositiveButton(R.string.start,
                        (dialog1, whichButton) -> {
                            TorrentHandler torrentHandler = new TorrentHandler();
                            torrentHandler.readdTorrentForDownload(getActivity(), torrentMagnet);
                        }
                )
                .setNegativeButton(R.string.cancel,
                        (dialog1, whichButton) -> dismiss()
                )
                .create();
    }

    private Dialog createDialogForStream(Context context, String torrentMagnet){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        return builder.setTitle(R.string.torrent_already_downloading_dialog_title)
                .setMessage(R.string.R_string_torrent_already_downloading_dialog_stream_body)
                .setPositiveButton(R.string.start,
                        (dialog1, whichButton) -> {
                           TorrentHandler torrentHandler = new TorrentHandler();
                            torrentHandler.readdTorrentForStream(getActivity(), torrentMagnet);
                        }
                )
                .setNegativeButton(R.string.cancel,
                        (dialog1, whichButton) -> {
                            dismiss();
                        }
                )
                .create();
    }

    public void setDialogForStream(boolean dialogForStream) {
        this.dialogForStream = dialogForStream;
    }
}