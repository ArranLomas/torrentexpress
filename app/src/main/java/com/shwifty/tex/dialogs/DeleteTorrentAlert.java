package com.shwifty.tex.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.Toast;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.Utilities;

import java.io.File;
import java.util.ArrayList;

public class DeleteTorrentAlert extends DialogFragment {
    private boolean deleteError = false;
    private MainApplication mainApplication = MainApplication.getInstance();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String torrentName = getArguments().get(MainActivity.TAG_TORRENT_NAME).toString();
        int torrentPosition = (int) getArguments().get(MainActivity.TAG_TORRENT_POSITION);


        return builder.setTitle(R.string.delete)
                .setMessage(this.getString(R.string.confirm_delete_prompt) + torrentName)
                .setPositiveButton(this.getString(R.string.delete),
                        (dialog1, whichButton) -> {
                            MainActivity mainActivity = null;

                            Activity currentActivity = getActivity();
                            if (currentActivity instanceof MainActivity) {
                                mainActivity = (MainActivity) currentActivity;
                            }

                            DownloadHandler downloadHandler = new DownloadHandler(getActivity());
                            ArrayList<Torrent> torrents = downloadHandler.getTorrents();

                            Torrent torrent = torrents.get(torrentPosition);

                            if(torrent == null){
                                Toast.makeText(getActivity(), getActivity().getString(R.string.delete_error_toast), Toast.LENGTH_SHORT).show();
                                dismiss();
                                return;
                            }


                            deleteError = false;

                            int torrentStatus = mainApplication.getTorrentStatus(torrent.getTorrentId());

                            final String storagePath = torrent.getDirectory();
                            File file = new File(storagePath + File.separator + torrent.getName());
                            if (file.exists()) {
                                Utilities.deleteRecursive(file);
                            } else {
                                CouldntLocateFileAlert newFragment = new CouldntLocateFileAlert();
                                Bundle bundle = new Bundle();
                                bundle.putString(MainActivity.TAG_TORRENT_NAME, torrentName);
                                bundle.putInt(MainActivity.TAG_TORRENT_POSITION, torrentPosition);
                                newFragment.setArguments(bundle);
                                newFragment.show(getActivity().getFragmentManager(), MainActivity.TAG_DIALOG);
                                dismiss();
                                return;
                            }

                            mainApplication.stopTorrent(getActivity(), torrent);

                            if (!deleteError) {
                                if(mainActivity!=null) {
                                    mainActivity.removeTorrentFromApp(torrent);
                                }else{
                                    Toast.makeText(getActivity(), "Error: Could not remove torrent", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                deleteError = false;
                                Utilities.deleteRecursive(file);

                                if (!deleteError) {
                                    if(mainActivity!=null) {
                                        mainActivity.removeTorrentFromApp(torrent);
                                    }else{
                                        Toast.makeText(getActivity(), "Error: Could not remove torrent", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    showErrorToast(torrentName);
                                    if (torrentStatus == 1) {
                                        mainApplication.startTorrent(getActivity(), torrent);
                                    }
                                }
                            }

                            dismiss();
                        }
                )
                .setNegativeButton(R.string.cancel,
                        (dialog1, whichButton) -> dismiss()
                )
                .create();
    }

    private void showErrorToast(String torrentName) {
        Toast.makeText(getActivity(), getActivity().getString(R.string.error_deleting) + torrentName + getActivity().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
    }



}