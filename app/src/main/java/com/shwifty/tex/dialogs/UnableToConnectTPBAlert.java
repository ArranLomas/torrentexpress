package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;

public class UnableToConnectTPBAlert extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.connection_error)
                .setMessage(R.string.tpb_down)
                .setPositiveButton(R.string.retry,
                        (dialog1, whichButton) -> {
                            MainActivity mainActivity = null;
                            if(getActivity() instanceof MainActivity){
                                mainActivity = ((MainActivity) getActivity());
                                mainActivity.refreshTorrents();
                            }
                        }
                )
                .setNegativeButton(R.string.cancel,
                        (dialog1, whichButton) -> dismiss()
                )
                .setNeutralButton(R.string.wifi_settings, (dialog1, whichButton) -> startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)))
                .create();
    }
}
