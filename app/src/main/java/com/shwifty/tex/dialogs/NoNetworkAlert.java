package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;

import com.shwifty.tex.R;

public class NoNetworkAlert extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.no_network_dialog_title)
                .setPositiveButton(R.string.wifi_settings,
                        (dialog1, whichButton) -> startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS))
                )
                .setNegativeButton(R.string.close, (dialog1, whichButton) -> dismiss())
                .create();
    }

}
