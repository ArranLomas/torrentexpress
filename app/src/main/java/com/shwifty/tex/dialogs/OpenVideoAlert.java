package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.handlers.StreamHandler;

public class OpenVideoAlert extends DialogFragment {
    private String magnet;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.open_video_dialog_title)
                .setPositiveButton(R.string.on_phone,
                        (dialog1, whichButton) -> {
                            StreamHandler.startLibVlc(getActivity(), magnet);
                            dismiss();
                        }
                )
                .setNegativeButton(R.string.chromecast, (dialog1, whichButton) -> {
                    StreamHandler.startChromecast(getActivity());
                    dismiss();
                })
                .create();
    }

    public void setMagnet(String magnet) {
        this.magnet = magnet;
    }
}