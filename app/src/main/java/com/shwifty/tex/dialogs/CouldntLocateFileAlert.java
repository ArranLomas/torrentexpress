package com.shwifty.tex.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;

import java.util.ArrayList;

public class CouldntLocateFileAlert extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        int torrentPosition = (int) getArguments().get(MainActivity.TAG_TORRENT_POSITION);

        final MainActivity mainActivity;
        Activity currentActivity = getActivity();
        if (currentActivity instanceof MainActivity) {
            mainActivity = ((MainActivity) currentActivity);
        } else {
            dismiss();
            return null;
        }





        DownloadHandler downloadHandler = new DownloadHandler(getActivity());
        ArrayList<Torrent> torrents = downloadHandler.getTorrents();
        Torrent torrent = torrents.get(torrentPosition);


        return builder.setTitle(R.string.file_missing_title)
                .setMessage(R.string.delete_or_redownload_prompt)
                .setPositiveButton(R.string.delete,
                        (dialog1, whichButton) -> {
                            MainApplication.getInstance().stopTorrent(getActivity(), torrent);
                            mainActivity.removeTorrentFromApp(torrent);
                            dismiss();
                        }
                )
                .setNegativeButton(R.string.close, (dialog1, whichButton) -> dismiss())
                .setNeutralButton(R.string.redownload, (dialog1, whichButton) -> {
                    TorrentHandler torrentHandler = new TorrentHandler();
                    torrentHandler.readdTorrentForDownload(getActivity(), torrent.getMagnetLink());
                })
                .create();
    }
}