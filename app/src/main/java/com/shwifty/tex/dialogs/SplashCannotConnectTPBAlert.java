package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.SearchHandler;
import com.shwifty.tex.handlers.TPBHandler;

import java.util.ArrayList;

public class SplashCannotConnectTPBAlert extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.connect_error_dialog_title)
                .setMessage(R.string.connect_error_dialog_body)
                .setPositiveButton(R.string.contine,
                        (dialog1, whichButton) -> {
                            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                            mainIntent.putExtra(getActivity().getString(R.string.coming_from_splash), true);
                            getActivity().startActivity(mainIntent);
                            dismiss();
                            getActivity().finish();
                            TPBHandler tpbHandler = new TPBHandler();
                            tpbHandler.setTpbTorrents(new ArrayList<>(), SearchHandler.getQuery(), SearchHandler.getCategory());
                            tpbHandler.setTorrentsSet(true);
                        }
                )
                .setNegativeButton(R.string.exit,
                        (dialog1, whichButton) -> {
                            getActivity().finish();
                            MainApplication.getInstance().shutdown(getActivity());
                            dismiss();
                        }
                )

                .create();
    }
}
