package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainApplication;



public class ExitAlert extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.exit_dialog_title)
                .setMessage(R.string.exit_dialog_body)
                .setPositiveButton(R.string.exit,
                        (dialog1, whichButton) -> {
                            MainApplication.getInstance().shutdown(getActivity());
                            getActivity().finish();
                        }
                )
                .setNegativeButton(R.string.cancel,
                        (dialog1, whichButton) -> dismiss()
                )
                .create();
    }
}
