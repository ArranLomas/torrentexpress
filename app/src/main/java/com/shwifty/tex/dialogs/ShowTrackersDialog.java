package com.shwifty.tex.dialogs;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.shwifty.tex.R;
import com.shwifty.tex.fragments.DownloadManagerFragment;
import com.shwifty.tex.fragments.TrackersFragment;

/**
 * Created by arran on 18/01/2017.
 */

public class ShowTrackersDialog extends DialogFragment {
    public ShowTrackersDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static ShowTrackersDialog newInstance(long torentID) {
        ShowTrackersDialog frag = new ShowTrackersDialog();
        Bundle args = new Bundle();
        args.putLong("torrent", torentID);
        frag.setArguments(args);

        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_show_trackers, container);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        // Fetch arguments from bundle and set title
        getDialog().setTitle("trackers");
        long torrentID = getArguments().getLong("torrent");
        showTrackersFragment(torrentID);
        // Show soft keyboard automatically and request focus to field
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if (getDialog() == null)
            return;

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (size.x / 20) * 19;
        int screenBaseHeight = (size.y / 20) * 18;
        resizeWindow(width, screenBaseHeight);
    }

    private void showTrackersFragment(long torrentID){
        Bundle bundle = new Bundle();
        bundle.putInt("noTiles", 4);
        TrackersFragment trackersFragment = TrackersFragment.newInstance(torrentID);
        FragmentManager fragmentManager = getChildFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.show_trackers_content, trackersFragment).commit();
    }

    private void resizeWindow(int width, int height) {
        final Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(width, height);
            window.setGravity(Gravity.CENTER);
        }

    }


}
