package com.shwifty.tex.dialogs;

import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Display;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.adapters.TorrentFileStreamAdapter;
import com.shwifty.tex.ui.ExpandedListView;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StreamDialogFragment extends DialogFragment {
    private Torrent torrent;
    private TorrentHandler torrentHandler = new TorrentHandler();
    public boolean isShowing;
    public static final String TAG_MAGNET_ARG = "magnetString";
    private TorrentFileStreamAdapter torrentFileStreamAdapter;


    @BindView(R.id.torrent_file_list_stream)
    ExpandedListView torrentFileList;
    @BindView(R.id.selectFilesTitleStream)
    TextView selectFilesTitle;
    @BindView(R.id.tapAndHoldHint)
    TextView holdHint;

    private String magnetString;


    public StreamDialogFragment() {
        // Required empty public constructor
    }

    public static StreamDialogFragment newInstance() {
        return new StreamDialogFragment();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        isShowing = true;
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        isShowing = false;
        super.onDismiss(dialog);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        magnetString = getArguments().getString(TAG_MAGNET_ARG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_stream, container, false);
        ButterKnife.bind(this, v);
        ExpandedListView list = (ExpandedListView) v.findViewById(R.id.torrent_file_list_stream);


        torrentFileStreamAdapter = new TorrentFileStreamAdapter(getActivity(), R.layout.list_item_stream_file);
        torrentFileList.setAdapter(torrentFileStreamAdapter);

        torrent = torrentHandler.getTorrentFromMagnet(magnetString);
        if (torrent == null) {
            dismiss();
            return null;
        }
        torrentFileStreamAdapter.setTorrent(torrent);

        list.setOnItemClickListener((adapterView, view, position, id) -> {
            if (getActivity() instanceof MainActivity) ;
            {
                MainActivity mainActivity = (MainActivity) getActivity();
                Thread thread = new Thread(() -> {
                    mainActivity.setupStream(torrent, position);
                });
                thread.start();
            }
            dismiss();
        });

        list.setOnItemLongClickListener((adapterView, view, position, id) -> {
            v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            Thread thread = new Thread(() -> {
                torrentFileStreamAdapter.setupShare(position);
            });
            dismiss();
            thread.start();


            return false;
        });

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        showFiles(torrentFileStreamAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void showFiles(final TorrentFileStreamAdapter torrentFileStreamAdapter) {
        ArrayList<Torrent> torrents = torrentHandler.getTorrents();

        for (int x = 0; x < torrents.size(); x++) {
            final Torrent torrent = torrents.get(x);
            if (torrent.getMagnetLink().equalsIgnoreCase(magnetString)) {
                Handler mainHandler;
                if (getActivity() != null) {
                    mainHandler = new Handler(getActivity().getMainLooper());
                } else {
                    return;
                }

                Runnable myRunnable = () -> {
                    torrentFileStreamAdapter.clear();
                    for (int z = 0; z < torrent.getTorrentFiles().size(); z++) {
                        torrentFileStreamAdapter.add(torrent.getTorrentFiles().get(z));
                    }
                    torrentFileStreamAdapter.notifyDataSetChanged();
                };
                mainHandler.post(myRunnable);
                enlargePopupToList();

            }
        }
    }

    private void enlargePopupToList() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (size.x / 20) * 19;
        int screenBaseHeight = (size.y / 20) * 18;

        resizeWindow(width, screenBaseHeight);
    }

    private void resizeWindow(int width, int height) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            if (getDialog() == null || getDialog().getWindow() == null) return;
            final Window window = getDialog().getWindow();
            window.setLayout(width, height);
            window.setGravity(Gravity.CENTER);
        });
    }


}
