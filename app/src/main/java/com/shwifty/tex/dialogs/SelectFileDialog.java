package com.shwifty.tex.dialogs;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.VideoActivity;
import com.shwifty.tex.handlers.ServerHandler;
import com.shwifty.tex.handlers.StreamHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.PathMax;
import com.shwifty.tex.utilities.Utilities;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class SelectFileDialog extends AlertDialog.Builder {
    private static final String ROOT = "/";

    private File currentPath;
    private TextView title;
    private ListView listView;
    private FilenameFilter filenameFilter;
    private int folderIcon;
    private int fileIcon;
    private FileAdapter adapter;

    private int paddingLeft;
    private int paddingRight;
    private int paddingBottom;
    private int paddingTop;
    private int iconSize;

    private boolean readonly = false;

    private static class SortFiles implements Comparator<File> {
        // for symlinks
        static boolean isFile(File f) {
            return !f.isDirectory();
        }

        @Override
        public int compare(File f1, File f2) {
            if (f1.isDirectory() && isFile(f2))
                return -1;
            else if (isFile(f1) && f2.isDirectory())
                return 1;
            else
                return f1.getPath().compareTo(f2.getPath());
        }
    }

    private class FileAdapter extends ArrayAdapter<File> {
        int selectedIndex = -1;
        int colorSelected;
        int colorTransparent;
        File currentPath;

        FileAdapter(Context context) {
            super(context, android.R.layout.simple_list_item_1);

            if (Build.VERSION.SDK_INT >= 23) {
                colorSelected = getContext().getResources().getColor(android.R.color.holo_blue_dark, getContext().getTheme());
                colorTransparent = getContext().getResources().getColor(android.R.color.transparent, getContext().getTheme());
            } else {
                colorSelected = ContextCompat.getColor(getContext(), android.R.color.holo_blue_dark);
                colorTransparent = ContextCompat.getColor(getContext(), android.R.color.transparent);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            File file = getItem(position);
            if (file != null) {
                view.setText(file.getName());
                if (file.isDirectory()) {
                    setDrawable(view, getDrawable(folderIcon));
                } else {
                    setDrawable(view, getDrawable(fileIcon));
                }
            }
            if (selectedIndex == position)
                view.setBackgroundColor(colorSelected);
            else
                view.setBackgroundColor(colorTransparent);

            return view;
        }

        private void setDrawable(TextView view, Drawable drawable) {
            if (view != null) {
                if (drawable != null) {
                    drawable.setBounds(0, 0, iconSize, iconSize);
                    view.setCompoundDrawables(drawable, null, null, null);
                } else {
                    view.setCompoundDrawables(null, null, null, null);
                }
            }
        }

        void scan(File dir) {
            selectedIndex = -1;
            currentPath = dir;

            if (!dir.isDirectory()) {
                currentPath = currentPath.getParentFile();
            }

            if (currentPath == null) {
                currentPath = new File(ROOT);
            }

            clear();

            File[] files = currentPath.listFiles(filenameFilter);

            if (files == null)
                return;

            ArrayList<File> list = new ArrayList<>(Arrays.asList(files));

            addAll(list);

            sort(new SortFiles());

            if (!dir.isDirectory()) {
                selectedIndex = getPosition(dir);
            }

            notifyDataSetChanged();
        }
    }

    private static class EditTextDialog extends AlertDialog.Builder {
        EditText input;

        EditTextDialog(Context context) {
            super(context);

            input = new EditText(getContext());
            input.setSingleLine(true);

            setView(input);
        }

        public AlertDialog.Builder setPositiveButton(final DialogInterface.OnClickListener listener) {
            return setPositiveButton(android.R.string.ok, listener);
        }

        @Override
        public AlertDialog.Builder setPositiveButton(int textId, final DialogInterface.OnClickListener listener) {
            return super.setPositiveButton(textId, (dialog, which) -> {
                listener.onClick(dialog, which);
                hide();
            });
        }

        @Override
        public AlertDialog.Builder setPositiveButton(CharSequence text, final DialogInterface.OnClickListener listener) {
            return super.setPositiveButton(text, (dialog, which) -> {
                listener.onClick(dialog, which);
                hide();
            });
        }

        void hide() {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        @Override
        public AlertDialog create() {
            AlertDialog d = super.create();

            d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

            return d;
        }

        public void setText(String s) {
            input.setText(s);
            input.setSelection(s.length());
        }

        public String getText() {
            return input.getText().toString();
        }
    }

    public SelectFileDialog(Context context) {
        super(context);

        currentPath = Environment.getExternalStorageDirectory();
        paddingLeft = dp2px(14);
        paddingRight = dp2px(14);
        paddingTop = dp2px(14);
        paddingBottom = dp2px(14);
        iconSize = dp2px(30);

        folderIcon = R.drawable.ic_folder;
        fileIcon = R.drawable.ic_file;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getContext().getResources().getDisplayMetrics());
    }

    private void toast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public AlertDialog show() {
        title = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
        title.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        title.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        PathMax textMax = new PathMax(getContext(), title);
        textMax.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        setCustomTitle(textMax);

        // main view, linearlayout
        final LinearLayout main = new LinearLayout(getContext());
        main.setOrientation(LinearLayout.VERTICAL);
        main.setMinimumHeight(getLinearLayoutMinHeight(getContext()));
        main.setPadding(paddingLeft, 0, paddingRight, 0);

        // add toolbar (UP / NEWFOLDER)
        {
            LinearLayout toolbar = new LinearLayout(getContext());
            toolbar.setOrientation(LinearLayout.HORIZONTAL);
            toolbar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            {
                TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
                String UP = "[..]";
                textView.setText(UP);
                Drawable icon = getDrawable(folderIcon);
                icon.setBounds(0, 0, iconSize, iconSize);
                textView.setCompoundDrawables(icon, null, null, null);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.weight = 1;
                textView.setLayoutParams(lp);
                textView.setOnClickListener(view -> {
                    File parentDirectory = currentPath.getParentFile();

                    if (parentDirectory == null) {
                        parentDirectory = new File(ROOT);
                    } else {
                        currentPath = parentDirectory;
                        RebuildFiles();
                    }
                });
                toolbar.addView(textView);
            }

            if (!readonly) {
                Button textView = new Button(getContext());
                textView.setPadding(paddingLeft, 0, paddingRight, 0);
                textView.setText(R.string.OpenFileDialogNewFolder);
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                textView.setOnClickListener(view -> {
                    final EditTextDialog builder = new EditTextDialog(getContext());
                    builder.setTitle(R.string.OpenFileDialogFolderName);
                    builder.setText("");
                    builder.setPositiveButton((dialog, which) -> {
                        File f = new File(currentPath, builder.getText());
                        if (!f.mkdirs()) {
                            toast("Unable To Create");
                        } else {
                            toast("Folder Created");
                        }
                        adapter.scan(currentPath);
                    });
                    builder.show();
                });
                toolbar.addView(textView, lp);
            }

            main.addView(toolbar);
        }

        // ADD FILES LIST
        {
            listView = new ListView(getContext());
            listView.setOnItemLongClickListener((parent, view, position, id) -> {
                final PopupMenu p = new PopupMenu(getContext(), view);
                if (!readonly) {
                    p.getMenu().add(getContext().getString(R.string.OpenFileDialogRename));
                    p.getMenu().add(getContext().getString(R.string.OpenFileDialogDelete));
                }
                p.setOnMenuItemClickListener(item -> {
                    if (item.getTitle().equals(getContext().getString(R.string.OpenFileDialogRename))) {
                        final File ff = adapter.getItem(position);
                        final EditTextDialog b = new EditTextDialog(getContext());
                        b.setTitle(getContext().getString(R.string.OpenFileDialogFolderName));
                        if(ff !=null) b.setText(ff.getName());
                        b.setPositiveButton((dialog, which) -> {
                            File f = new File(ff.getParent(), b.getText());
                            boolean renamed = ff.renameTo(f);
                            if(renamed) {
                                String rename = getContext().getString(R.string.OpenFileDialogRenamedTo) + "," + f.getName();
                                toast(rename);
                            }else{
                                toast("Error, could not rename");
                            }
                            adapter.scan(currentPath);
                        });
                        b.show();
                        return true;
                    }
                    if (item.getTitle().equals(getContext().getString(R.string.OpenFileDialogDelete))) {
                        File ff = adapter.getItem(position);

                        boolean deleted = Utilities.deleteRecursive(ff);
                        if (deleted) {
                            String deletedMessage = getContext().getString(R.string.OpenFileDialogFolderDeleted) + "," + ff.getName();
                            toast(deletedMessage);
                        } else {
                            String deletedError = getContext().getString(R.string.OpenFileDialogFolderDeletedError) + "," + ff.getName();
                            toast(deletedError);
                        }
                        adapter.scan(currentPath);
                        return true;
                    }
                    return false;
                });

                if (p.getMenu().size() != 0) {
                    p.show();
                    return true;
                }

                return false;
            });
            listView.setOnItemClickListener((adapterView, view, index, l) -> {
                File file = adapter.getItem(index);
                if(file == null)return;

                currentPath = file;
                if (file.isDirectory()) {
                    RebuildFiles();
                } else {
                    openFile();

                }
            });
            main.addView(listView);
        }

        {
            TextView text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
            text.setText(getContext().getString(R.string.OpenFileDialogEmptyList));
            text.setVisibility(View.GONE);
            listView.setEmptyView(text);
            main.addView(text);
        }

        setView(main);
        setNegativeButton(android.R.string.cancel, null);

        adapter = new FileAdapter(getContext());
        adapter.scan(currentPath);
        listView.setAdapter(adapter);

        title.setText(adapter.currentPath.getPath());

        // scroll to selected item
        listView.post(() -> listView.setSelection(adapter.selectedIndex));

        return super.show();
    }

    public void setCurrentPath(File path) {
        currentPath = path;
    }

    private static Display getDefaultDisplay(Context context) {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    }

    private Drawable getDrawable(int resid) {
        Drawable d = ContextCompat.getDrawable(getContext(), resid);
        d.setColorFilter(Utilities.getThemeColor(getContext(), android.R.attr.colorForeground), PorterDuff.Mode.SRC_ATOP);
        return d;
    }

    private static Point getScreenSize(Context context) {
        Point screeSize = new Point();
        getDefaultDisplay(context).getSize(screeSize);
        return screeSize;
    }

    private static int getLinearLayoutMinHeight(Context context) {
        return getScreenSize(context).y;
    }

    private void RebuildFiles() {
        adapter.scan(currentPath);
        listView.setSelection(0);
        title.setText(adapter.currentPath.getPath());
    }

    private void openFile() {
        String mime = Utilities.getMimeType(currentPath, getContext());

        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent torrent = torrentHandler.getTorrentFromFilePath(currentPath.getPath());

        if (torrent == null) {
            openFileNormally(mime);
            return;
        }

        if (torrent.getPercCompleted() < 100) {
            openIncompletedTorrent(mime, torrent);
            return;
        }

        File selectedFile = currentPath;
        String ip = NetworkUtil.getIPAddress(true);

        String myURL = "http://" + ip + selectedFile.getPath();
        URL streamUrl = null;

//        (String scheme, String userInfo, String host, int port, String path, String query, String fragment

        try {
            URL url = new URL(myURL);
            URI uri = new URI("http", null, url.getHost(), ServerHandler.completedPort, url.getPath(), url.getQuery(), null);
            streamUrl = uri.toURL();
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }


        if (streamUrl == null) return;
        StreamHandler streamHandler = new StreamHandler();
        streamHandler.setupStream(torrent, streamUrl.toString(), mime, getContext());


    }

    private void openIncompletedTorrent(String mime, Torrent torrent) {
        Context context = getContext();
        String originalMagnet = torrent.getMagnetLink();
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent originalTorrent = torrentHandler.getTorrentFromMagnet(originalMagnet);

        if (originalTorrent == null) {
            openFileNormally(mime);
            return;
        }

        String torrentDirectory = originalTorrent.getDirectory();
        TorrentFile selectedTorrentFile = null;

        for (TorrentFile torrentFile : originalTorrent.getTorrentFiles()) {
            String filePath = torrentDirectory + File.separator + torrentFile.file.getPath();
            if (filePath.equalsIgnoreCase(currentPath.getPath())) {
                selectedTorrentFile = torrentFile;
            }
        }


        if (selectedTorrentFile == null) {
            openFileNormally(mime);
            return;
        }


        File selectedFile = new File(selectedTorrentFile.file.getPath());
        String ip = NetworkUtil.getIPAddress(true);

        String myURL = ip + getContext().getString(R.string.http) + ip + getContext().getString(R.string.torrentID_url) + torrent.getTorrentId() + ip + getContext().getString(R.string.torrentFileID_url) + selectedTorrentFile.index + "/" + selectedFile.getPath();
        URL streamUrl = null;


        try {
            URL url = new URL(myURL);
            URI uri = new URI("http", null, url.getHost(), ServerHandler.streamPort, url.getPath(), url.getQuery(), null);
            streamUrl = uri.toURL();
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }

        if (streamUrl == null) {
            openFileNormally(mime);
            return;
        }

        StreamHandler streamHandler = new StreamHandler();
        streamHandler.setupStream(originalTorrent, streamUrl.toString(), mime, context);
    }

    private void openFileNormally(String mime) {
        if (mime == null) {
            Toast.makeText(getContext(), getContext().getString(R.string.cannot_open_file_toast), Toast.LENGTH_LONG).show();
            return;
        }

        if (!mime.contains("/")) {
            Toast.makeText(getContext(), getContext().getString(R.string.cannot_open_file_toast), Toast.LENGTH_LONG).show();
            return;
        }


        String[] splitMime = mime.split("/");
        String format = splitMime[1];

        if (format.equalsIgnoreCase(getContext().getString(R.string.video))) {
            Intent vlcIntent = new Intent(getContext(), VideoActivity.class);
            vlcIntent.putExtra(getContext().getString(R.string.video_url), currentPath.getPath());
            getContext().startActivity(vlcIntent);
        }

        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                Uri contentUri = FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + getContext().getString(R.string.provider), currentPath);
                intent.setDataAndType(contentUri, mime);
            } else {
                intent.setDataAndType(Uri.fromFile(currentPath), mime);
            }
            getContext().startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            Toast.makeText(getContext(), R.string.no_app_found, Toast.LENGTH_LONG).show();
        }

    }
}
