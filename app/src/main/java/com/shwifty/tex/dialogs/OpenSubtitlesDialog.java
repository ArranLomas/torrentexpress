package com.shwifty.tex.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.activities.VideoActivity;
import com.shwifty.tex.adapters.SelectSubtitlesAdapter;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.handlers.VLCHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.torrent.TorrentFile;
import com.shwifty.tex.ui.ExpandedListView;
import com.shwifty.tex.utilities.MimeConstants;
import com.shwifty.tex.utilities.Prefs;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by arran on 6/01/2017.
 */

public class OpenSubtitlesDialog extends DialogFragment {
    private Torrent torrent;
    private TorrentHandler torrentHandler = new TorrentHandler();
    public boolean isShowing;
    public static final String TAG_MAGNET_ARG = "magnetString";
    private SelectSubtitlesAdapter selectSubtitlesAdapter;


    @BindView(R.id.torrent_file_list_subtitles)
    ExpandedListView torrentFileList;
    @BindView(R.id.selectFilesTitleSubtitles)
    TextView selectFilesTitle;

    private String magnetString;


    public OpenSubtitlesDialog() {
        // Required empty public constructor
    }

    public static OpenSubtitlesDialog newInstance() {
        return new OpenSubtitlesDialog();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        isShowing = true;
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        isShowing = false;
        super.onDismiss(dialog);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        magnetString = getArguments().getString(TAG_MAGNET_ARG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_select_subtitles, container, false);
        ButterKnife.bind(this, v);
        ExpandedListView list = (ExpandedListView) v.findViewById(R.id.torrent_file_list_subtitles);


        selectSubtitlesAdapter = new SelectSubtitlesAdapter(getActivity(), R.layout.list_item_subtititles);
        torrentFileList.setAdapter(selectSubtitlesAdapter);

        torrent = torrentHandler.getTorrentFromMagnet(magnetString);
        if (torrent == null) {
            dismiss();
            return null;
        }
        showFiles(selectSubtitlesAdapter);

        list.setOnItemClickListener((adapterView, view, position, id) -> {
            if (getActivity() instanceof VideoActivity) {
                MainApplication mainApplication = MainApplication.getInstance();
                if (mainApplication.isPaused()) return;

                TorrentFile subtitleFile = selectSubtitlesAdapter.getItem(position);

                VideoActivity videoActivity = (VideoActivity) getActivity();
                videoActivity.notifySubtitleFileSelected(subtitleFile);
            }
            dismiss();

        });
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void showFiles(final SelectSubtitlesAdapter selectSubtitlesAdapter) {
        ArrayList<Torrent> torrents = torrentHandler.getTorrents();

        Torrent torrent = torrentHandler.getTorrentFromMagnet(magnetString);

        selectSubtitlesAdapter.clear();
        for (int z = 0; z < torrent.getTorrentFiles().size(); z++) {
            //TODO
            TorrentFile torrentFile = torrent.getTorrentFiles().get(z);
            for (String format : MimeConstants.subtitleSet) {
                if (torrentFile.file.getPath().endsWith(format)) {
                    selectSubtitlesAdapter.add(torrentFile);
                }
            }
        }

        selectSubtitlesAdapter.notifyDataSetChanged();

        enlargePopupToList();


    }

    private void enlargePopupToList() {
        Thread thread = new Thread(() -> {
            boolean running = true;
            while (running) {
                if (torrentFileList.isHeightSet()) {
                    running = false;
                }
            }

            int measuredHeight = 150;
            measuredHeight += selectFilesTitle.getHeight();
            measuredHeight += torrentFileList.getMyHeight();

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = (size.x / 20) * 19;
            int screenBaseHeight = (size.y / 20) * 18;

//            if (measuredHeight < screenBaseHeight && measuredHeight > 400) {
//                resizeWindow(width, measuredHeight);
//            } else {
            resizeWindow(width, screenBaseHeight);
//            }
        });
        thread.start();

    }

    private void resizeWindow(int width, int height) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            if (getDialog() == null || getDialog().getWindow() == null) return;
            final Window window = getDialog().getWindow();
            window.setLayout(width, height);
            window.setGravity(Gravity.CENTER);
        });
    }
}

