package com.shwifty.tex.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;

import java.util.ArrayList;

/**
 * Created by arran on 29/12/2016.
 */

public class UpdateAppAlert extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle("An update is available")
                .setMessage("would you like to download the update now? \nThis may stop and remove torrents from the app")
                .setPositiveButton("Download",
                        (dialog1, whichButton) -> {
                            if(getActivity() instanceof MainActivity){
                                MainActivity mainActivity = (MainActivity) getActivity();
                                mainActivity.logDownloadAPK();
                            }
                            MainApplication.downloadSearchEnabledAPK(getActivity());
                            dismiss();
                        }
                )
                .setNegativeButton("Ignore", (dialog1, whichButton) -> {
                    if(getActivity() instanceof MainActivity){
                        MainActivity mainActivity = (MainActivity) getActivity();
                        mainActivity.logIgnoreAPK();
                    }
                    dismiss();
                })

                .create();
    }
}