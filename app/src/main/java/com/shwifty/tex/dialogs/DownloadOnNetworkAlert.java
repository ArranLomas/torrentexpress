package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;

public class DownloadOnNetworkAlert extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.network_dialog_title)
                .setMessage(R.string.network_dialog_text)
                .setPositiveButton(R.string.app_settings,
                        (dialog1, whichButton) -> {
                            MainActivity mainActivity = (MainActivity) getActivity();
                            mainActivity.showSettingsFragment();
                            dismiss();
                        }
                )
                .setNegativeButton(R.string.close, (dialog1, whichButton) -> {
                    dismiss();
                })
                .create();
    }
}