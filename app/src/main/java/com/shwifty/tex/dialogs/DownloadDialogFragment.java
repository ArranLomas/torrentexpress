package com.shwifty.tex.dialogs;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.handlers.DownloadHandler;
import com.shwifty.tex.ui.ExpandedListView;
import com.shwifty.tex.adapters.TorrentFileDownloadAdapter;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DownloadDialogFragment extends DialogFragment {
    private Torrent torrent;
    private TorrentHandler torrentHandler = new TorrentHandler();
    public boolean isShowing;
    public static final String TAG_MAGNET_ARG = "magnetString";
    private TorrentFileDownloadAdapter torrentFileDownloadAdapter;

    @BindView(R.id.torrent_file_list_download)
    ExpandedListView torrentFileList;
    @BindView(R.id.startDownloadButton)
    FloatingActionButton startButton;
    @BindView(R.id.selectFilesTitleDownload)
    TextView selectFilesTitle;
    @BindView(R.id.centre_line_download)
    View centreLine;

    private String magnetString;


    public DownloadDialogFragment() {
        // Required empty public constructor
    }

    public static DownloadDialogFragment newInstance() {
        return new DownloadDialogFragment();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        isShowing = true;
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        isShowing = false;
        super.onDismiss(dialog);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        magnetString = getArguments().getString(TAG_MAGNET_ARG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_download, container, false);
        ButterKnife.bind(this, v);

        torrentFileDownloadAdapter = new TorrentFileDownloadAdapter(getActivity(), R.layout.list_item_download_file);
        torrentFileList.setAdapter(torrentFileDownloadAdapter);

        torrent = torrentHandler.getTorrentFromMagnet(magnetString);
        torrentFileDownloadAdapter.setTorrent(torrent);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        showFiles(torrentFileDownloadAdapter);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showFiles(final TorrentFileDownloadAdapter torrentFileDownloadAdapter) {
        ArrayList<Torrent> torrents = torrentHandler.getTorrents();

        for (int x = 0; x < torrents.size(); x++) {
            final Torrent torrent = torrents.get(x);
            if (torrent.getMagnetLink().equalsIgnoreCase(magnetString)) {
                Handler mainHandler;
                if (getActivity() != null) {
                    mainHandler = new Handler(getActivity().getMainLooper());
                } else {
                    return;
                }

                Runnable myRunnable = () -> {
                    torrentFileDownloadAdapter.clear();
                    for (int z = 0; z < torrent.getTorrentFiles().size(); z++) {
                        torrentFileDownloadAdapter.add(torrent.getTorrentFiles().get(z));
                    }
                    torrentFileDownloadAdapter.notifyDataSetChanged();
                };

                mainHandler.post(myRunnable);
                enlargePopupToList();

            }
        }
    }

    private void enlargePopupToList() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (size.x / 20) * 19;
        int screenBaseHeight = (size.y / 20) * 18;

//            if (measuredHeight < screenBaseHeight) {
//                resizeWindow(width, measuredHeight);
//            } else {
        resizeWindow(width, screenBaseHeight);
//            }

    }

    private void resizeWindow(int width, int height) {
        final Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(width, height);
            window.setGravity(Gravity.CENTER);
        }

    }

    @OnClick(R.id.startDownloadButton)
    void startButtonPressed() {
        Utilities.setDownloadInfoStrings(getActivity(), torrent);
        startDownload();
        getDialog().dismiss();
    }

    public void startDownload() {
        DownloadHandler downloadHandler = new DownloadHandler(getContext());


        downloadHandler.addTorrentForDownload(getContext(), torrent);

        MainApplication.getInstance().startTorrent(getContext(), torrent);

        Intent intent = new Intent(getActivity(), MainActivity.class);
        String actionShowDownloads = getActivity().getString(R.string.action_show_downloads);
        intent.setAction(actionShowDownloads);
        startActivity(intent);
        getDialog().dismiss();

        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.logDownloadEvent(torrent.getName());
        }


    }


}
