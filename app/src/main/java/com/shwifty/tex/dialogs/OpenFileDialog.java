package com.shwifty.tex.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.shwifty.tex.R;
import com.shwifty.tex.utilities.PathMax;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class OpenFileDialog extends AlertDialog.Builder {
    public static final String UP = "[..]";
    public static final String ROOT = "/";

    File currentPath;
    TextView title;
    TextView message;
    ListView listView;
    FilenameFilter filenameFilter;
    int folderIcon;
    int fileIcon;
    FileAdapter adapter;

    int paddingLeft;
    int paddingRight;
    int paddingBottom;
    int paddingTop;
    int iconSize;
    Runnable changeFolder;

    // file / folder readonly dialog selection or output directory? also shows readonly folder tooltip.
    boolean readonly = false;
    // allow select files, or just select directory
    boolean files = true;

    Button positive; // enable / disable OK

    static class SortFiles implements Comparator<File> {
        // for symlinks
        public static boolean isFile(File f) {
            return !f.isDirectory();
        }

        @Override
        public int compare(File f1, File f2) {
            if (f1.isDirectory() && isFile(f2))
                return -1;
            else if (isFile(f1) && f2.isDirectory())
                return 1;
            else
                return f1.getPath().compareTo(f2.getPath());
        }
    }

    public class FileAdapter extends ArrayAdapter<File> {
        int selectedIndex = -1;
        int colorSelected;
        int colorTransparent;
        File currentPath;

        public FileAdapter(Context context) {
            super(context, android.R.layout.simple_list_item_1);

            if (Build.VERSION.SDK_INT >= 23) {
                colorSelected = getContext().getResources().getColor(R.color.colorAccentPressed, getContext().getTheme());
                colorTransparent = getContext().getResources().getColor(android.R.color.transparent, getContext().getTheme());
            } else {
                colorSelected = getContext().getResources().getColor(R.color.colorAccentPressed);
                colorTransparent = getContext().getResources().getColor(android.R.color.transparent);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            File file = getItem(position);
            if (view != null) {
                view.setText(file.getName());
                if (file.isDirectory()) {
                    setDrawable(view, getDrawable(folderIcon));
                } else {
                    setDrawable(view, getDrawable(fileIcon));
                }
                if (selectedIndex == position)
                    view.setBackgroundColor(colorSelected);
                else
                    view.setBackgroundColor(colorTransparent);
            }
            return view;
        }

        private void setDrawable(TextView view, Drawable drawable) {
            if (view != null) {
                if (drawable != null) {
                    drawable.setBounds(0, 0, iconSize, iconSize);
                    view.setCompoundDrawables(drawable, null, null, null);
                } else {
                    view.setCompoundDrawables(null, null, null, null);
                }
            }
        }

        public void scan(File dir) {
            updateSelected(-1);
            currentPath = dir;

            if (!dir.isDirectory()) {
                currentPath = currentPath.getParentFile();
            }

            if (currentPath == null) {
                currentPath = new File(ROOT);
            }

            clear();

            File[] files = currentPath.listFiles(filenameFilter);

            if (files == null)
                return;

            ArrayList<File> list = new ArrayList<>(Arrays.asList(files));

            if (Build.VERSION.SDK_INT < 11) {
                for (File f : list) {
                    add(f);
                }
            } else {
                addAll(list);
            }

            sort(new SortFiles());

            if (!dir.isDirectory()) {
                updateSelected(getPosition(dir));
            }

            notifyDataSetChanged();
        }
    }

    public static class EditTextDialog extends AlertDialog.Builder {
        EditText input;

        public EditTextDialog(Context context) {
            super(context);

            input = new EditText(getContext());

            input.setSingleLine(true);

            setPositiveButton(new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });
            setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    hide();
                }
            });

            setView(input);
        }

        public AlertDialog.Builder setPositiveButton(final DialogInterface.OnClickListener listener) {
            return setPositiveButton(android.R.string.ok, listener);
        }

        @Override
        public AlertDialog.Builder setPositiveButton(int textId, final DialogInterface.OnClickListener listener) {
            return super.setPositiveButton(textId, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listener.onClick(dialog, which);
                    hide();
                }
            });
        }

        @Override
        public AlertDialog.Builder setPositiveButton(CharSequence text, final DialogInterface.OnClickListener listener) {
            return super.setPositiveButton(text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listener.onClick(dialog, which);
                    hide();
                }
            });
        }

        void hide() {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        @Override
        public AlertDialog create() {
            AlertDialog d = super.create();

            d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

            return d;
        }

        public void setText(String s) {
            input.setText(s);
            input.setSelection(s.length());
        }

        public String getText() {
            return input.getText().toString();
        }
    }

    public OpenFileDialog(Context context) {
        super(context);

        currentPath = Environment.getExternalStorageDirectory();
        paddingLeft = dp2px(14);
        paddingRight = dp2px(14);
        paddingTop = dp2px(14);
        paddingBottom = dp2px(14);
        iconSize = dp2px(30);

        folderIcon = R.drawable.ic_folder;
        fileIcon = R.drawable.ic_file;
    }

    public int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getContext().getResources().getDisplayMetrics());
    }

    void toast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public AlertDialog create() {
        title = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
        title.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        title.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        PathMax textMax = new PathMax(getContext(), title);
        textMax.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        setCustomTitle(textMax);

        // main view, linearlayout
        final LinearLayout main = new LinearLayout(getContext());
        main.setOrientation(LinearLayout.VERTICAL);
        main.setMinimumHeight(getLinearLayoutMinHeight(getContext()));
        main.setPadding(paddingLeft, 0, paddingRight, 0);

        // add toolbar (UP / NEWFOLDER)
        {
            LinearLayout toolbar = new LinearLayout(getContext());
            toolbar.setOrientation(LinearLayout.HORIZONTAL);
            toolbar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            {
                TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
                textView.setText(UP);
                Drawable icon = getDrawable(folderIcon);
                icon.setBounds(0, 0, iconSize, iconSize);
                textView.setCompoundDrawables(icon, null, null, null);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.weight = 1;
                textView.setLayoutParams(lp);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        File parentDirectory = currentPath;
                        if (parentDirectory.isDirectory()) {
                            parentDirectory = parentDirectory.getParentFile();
                        } else {
                            parentDirectory = parentDirectory.getParentFile();
                            if (parentDirectory != null)
                                parentDirectory = parentDirectory.getParentFile();
                        }

                        if (parentDirectory == null)
                            parentDirectory = new File(ROOT);

                        if (parentDirectory != null) {
                            currentPath = parentDirectory;
                            RebuildFiles();
                        }
                    }
                });
                toolbar.addView(textView);
            }

            if (!readonly) { // show new folder button
                Button textView = new Button(getContext());
                textView.setPadding(paddingLeft, 0, paddingRight, 0);
                textView.setText(R.string.OpenFileDialogNewFolder);
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final EditTextDialog builder = new EditTextDialog(getContext());
                        builder.setTitle(R.string.OpenFileDialogFolderName);
                        builder.setText("");
                        builder.setPositiveButton(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                File f = new File(currentPath, builder.getText());
                                if (!f.mkdirs()) {
                                    toast("Unable To Create");
                                } else {
                                    toast("Folder Created");
                                }
                                adapter.scan(currentPath);
                            }
                        });
                        builder.show();
                    }
                });
                toolbar.addView(textView, lp);
            }

            main.addView(toolbar);

            message = new TextView(getContext());
            message.setGravity(Gravity.CENTER);
            message.setBackgroundColor(0xfffffae3);
            message.setVisibility(View.GONE);
            main.addView(message);
        }

        // ADD FILES LIST
        {
            listView = new ListView(getContext());
            listView.setOnItemLongClickListener((parent, view, position, id) -> {
                final PopupMenu p = new PopupMenu(getContext(), view);
                if (!readonly) { // show rename / delete
                    p.getMenu().add(getContext().getString(R.string.OpenFileDialogRename));
                    p.getMenu().add(getContext().getString(R.string.OpenFileDialogDelete));
                }
                p.setOnMenuItemClickListener(item -> {
                    if (item.getTitle().equals(getContext().getString(R.string.OpenFileDialogRename))) {
                        final File ff = adapter.getItem(position);
                        final EditTextDialog b = new EditTextDialog(getContext());
                        b.setTitle(getContext().getString(R.string.OpenFileDialogFolderName));
                        b.setText(ff.getName());
                        b.setPositiveButton((dialog, which) -> {
                            File f = new File(ff.getParent(), b.getText());
                            ff.renameTo(f);
                            String rename = getContext().getString(R.string.OpenFileDialogRenamedTo) + "," + f.getName();
                            toast(rename);
                            adapter.scan(currentPath);
                        });
                        b.show();
                        return true;
                    }
                    if (item.getTitle().equals(getContext().getString(R.string.OpenFileDialogDelete))) {
                        File ff = adapter.getItem(position);
                        ff.delete();
                        toast("Error, could not rename");
                        adapter.scan(currentPath);
                        return true;
                    }
                    return false;
                });

                if (p.getMenu().size() != 0) {
                    p.show();
                    return true;
                }

                return false;
            });
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                    File file = adapter.getItem(index);

                    currentPath = file;

                    if (file.isDirectory()) {
                        RebuildFiles();
                    } else {
                        if (files) { // allowed select files
                            if (index != adapter.selectedIndex) {
                                updateSelected(index);
                            } else {
                                currentPath = file.getParentFile();
                                updateSelected(-1);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getContext(), R.string.select_folder, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            main.addView(listView);
        }

        {
            TextView text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
            text.setText(getContext().getString(R.string.OpenFileDialogEmptyList));
            text.setVisibility(View.GONE);
            listView.setEmptyView(text);
            main.addView(text);
        }

        setView(main);
        setNegativeButton(android.R.string.cancel, null);

        adapter = new FileAdapter(getContext());
        listView.setAdapter(adapter);

        final AlertDialog d = super.create();
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                positive = d.getButton(DialogInterface.BUTTON_POSITIVE);
                RebuildFiles();
                // scroll to selected item
                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        listView.setSelection(adapter.selectedIndex);
                    }
                });
            }
        });
        return d;
    }

    void updateSelected(int i) {
        if (positive != null) {
            if (files && i == -1) {
                positive.setEnabled(false);
            } else {
                positive.setEnabled(true);
            }
        }
        adapter.selectedIndex = i;
    }

    public OpenFileDialog setFilter(final String filter) {
        filenameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File file, String fileName) {
                File tempFile = new File(file.getPath(), fileName);
                if (tempFile.isFile())
                    return tempFile.getName().matches(filter);
                return true;
            }
        };
        return this;
    }

    public OpenFileDialog setFolderIcon(int drawable) {
        this.folderIcon = drawable;
        return this;
    }

    // dialog to set output directory / file or readonly dialog?
    public void setReadonly(boolean b) {
        readonly = b;
    }

    // file select dialog or directory select dialog?
    public void setSelectFiles(boolean b) {
        files = b;
    }

    public void setChangeFolderListener(Runnable r) {
        this.changeFolder = r;
    }

    public void setCurrentPath(File path) {
        currentPath = path;
    }

    public File getCurrentPath() {
        return currentPath;
    }

    public OpenFileDialog setFileIcon(int drawable) {
        this.fileIcon = drawable;
        return this;
    }

    private static Display getDefaultDisplay(Context context) {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    }

    Drawable getDrawable(int resid) {
        Drawable d = ContextCompat.getDrawable(getContext(), resid);
//        d.setColorFilter(ThemeUtils.getThemeColor(getContext(), android.R.attr.colorForeground), PorterDuff.Mode.SRC_ATOP);
        return d;
    }

    private static Point getScreenSize(Context context) {
        Display d = getDefaultDisplay(context);
        if (Build.VERSION.SDK_INT < 13) {
            return new Point(d.getWidth(), d.getHeight());
        } else {
            Point screeSize = new Point();
            d.getSize(screeSize);
            return screeSize;
        }
    }

    private static int getLinearLayoutMinHeight(Context context) {
        return getScreenSize(context).y;
    }

    private void RebuildFiles() {
        if (!readonly) { // show readonly directory tooltip
            if (!currentPath.canWrite()) {
                message.setText(R.string.readonly_directory);
                message.setVisibility(View.VISIBLE);
            } else {
                message.setVisibility(View.GONE);
            }
        }
        adapter.scan(currentPath);
        listView.setSelection(0);
        title.setText(adapter.currentPath.getPath());
        if (changeFolder != null) {
            changeFolder.run();
        }
    }
}