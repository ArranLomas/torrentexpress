package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.activities.MainApplication;

public class SplashNoNetwork extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(getActivity().getString(R.string.no_network_dialog_title))
                .setPositiveButton(R.string.contine,
                        (dialog1, whichButton) -> {
                            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                            mainIntent.putExtra("comingFromSplash", true);
                            getActivity().startActivity(mainIntent);
                            dismiss();
                            getActivity().finish();
                        }
                )
                .setNegativeButton(R.string.exit, (dialog1, whichButton) -> {
                    getActivity().finish();
                    MainApplication.getInstance().shutdown(getActivity());
                    dismiss();
                })
                .setNeutralButton(R.string.wifi_settings, (dialog1, whichButton) -> startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS)))
                .create();
    }

}