package com.shwifty.tex.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;


import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.activities.SplashActivity;

public class Disclaimer extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String disclaimer = getActivity().getResources().getString(R.string.disclaimer);

        return builder.setTitle(R.string.disclaimer_title)
                .setMessage(disclaimer)
                .setPositiveButton(R.string.accept,
                        (dialog1, whichButton) -> {
                            Activity activity = getActivity();
                            if (activity instanceof SplashActivity) {
                                SplashActivity splashActivity = (SplashActivity) activity;
                                splashActivity.setDisclaimerAccepted(true);
                            }
                        }
                )
                .setNegativeButton(R.string.exit,
                        (dialog1, whichButton) -> {
                            Activity activity = getActivity();
                            if (activity instanceof SplashActivity) {
                                SplashActivity splashActivity = (SplashActivity) activity;
                                splashActivity.setDisclaimerAccepted(false);
                            }
                            MainApplication.getInstance().shutdown(getActivity());
                            getActivity().finish();
                            dismiss();
                        }
                )
                .create();
    }
}