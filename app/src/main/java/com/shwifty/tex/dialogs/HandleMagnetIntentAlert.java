package com.shwifty.tex.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainActivity;
import com.shwifty.tex.handlers.AddMagnetHandler;
import com.shwifty.tex.handlers.TorrentHandler;
import com.shwifty.tex.torrent.Torrent;


public class HandleMagnetIntentAlert extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String torrentMagnet = AddMagnetHandler.getMagnet();
        TorrentHandler torrentHandler = new TorrentHandler();
        Torrent torrent = torrentHandler.getTorrentFromMagnet(torrentMagnet);
        MainActivity mainActivity = (MainActivity) getActivity();

        return builder.setTitle(getActivity().getString(R.string.add_magnet_dialog_title) + torrent.getName())
                .setMessage(R.string.add_magnet_dialog_body)
                .setPositiveButton(R.string.stream,
                        (dialog1, whichButton) -> {
                            mainActivity.showStreamFilesDialog(torrentMagnet);
                            AddMagnetHandler.setMagnet(null);
                            dismiss();
                        }
                )
                .setNegativeButton(R.string.download,
                        (dialog1, whichButton) -> {
                            mainActivity.showDownloadFilesDialog(torrentMagnet);
                            AddMagnetHandler.setMagnet(null);
                            dismiss();
                        }
                )
                .setNeutralButton(R.string.do_nothing,
                        (dialog1, whichButton) -> {
                            AddMagnetHandler.setMagnet(null);
                            dismiss();
                        }
                )
                .create();
    }
}
