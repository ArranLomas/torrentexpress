package com.shwifty.tex.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.shwifty.tex.activities.MainApplication;
import com.shwifty.tex.utilities.NetworkUtil;
import com.shwifty.tex.utilities.Prefs;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        boolean wifiOnlyPref = Prefs.downloadOnWifiOnly;
        MainApplication mainApplication = MainApplication.getInstance();

        int status = NetworkUtil.getConnectivityStatus(context);


        if (status == NetworkUtil.TYPE_NOT_CONNECTED) {
            mainApplication.noConnectionLibtorrent();
        } else if (status == NetworkUtil.TYPE_MOBILE) {
            if (wifiOnlyPref) {
                mainApplication.pauseLibtorrent();
            } else {
                mainApplication.resumeLibtorrent();
            }
        } else {
            mainApplication.resumeLibtorrent();
        }
    }
}