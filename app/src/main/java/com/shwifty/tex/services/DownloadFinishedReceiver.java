package com.shwifty.tex.services;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.MainApplication;

import java.io.File;

/**
 * Created by arran on 31/12/2016.
 */

public class DownloadFinishedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent receivedIntent) {
        try {
            Bundle extras = receivedIntent.getExtras();

            SharedPreferences prefs = context.getSharedPreferences(MainApplication.TAG_APK_DOWNLOAD, Context.MODE_PRIVATE);
            long myDownloadID = prefs.getLong(MainApplication.TAG_APK_DOWNLOAD, 0);
            long downloadCompletedId = extras.getLong(DownloadManager.EXTRA_DOWNLOAD_ID);
            DownloadManager.Query q = new DownloadManager.Query();

            if (myDownloadID == downloadCompletedId) {
                q.setFilterById(downloadCompletedId);
                DownloadManager mManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                Cursor c = mManager.query(q);
                if (c.moveToFirst()) {
                    int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
                        File toInstall = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + File.separator + context.getString(R.string.app_name) + ".apk");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            Uri apkUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", toInstall);
                            Intent promptInstall = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                            promptInstall.setData(apkUri);
                            promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            promptInstall.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            context.startActivity(promptInstall);
                        } else {
                            Uri apkUri = Uri.fromFile(toInstall);
                            Intent promptInstall = new Intent(Intent.ACTION_VIEW);
                            promptInstall.setDataAndType(apkUri, "application/vnd.android.package-archive");
                            promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(promptInstall);
                        }
                    }
                }
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
