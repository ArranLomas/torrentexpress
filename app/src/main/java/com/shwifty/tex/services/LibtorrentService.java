package com.shwifty.tex.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.shwifty.tex.R;
import com.shwifty.tex.activities.SplashActivity;
import com.shwifty.tex.handlers.ServerHandler;
import com.shwifty.tex.torrent.Torrent;
import com.shwifty.tex.utilities.Prefs;

import go.libtorrent.File;
import go.libtorrent.Libtorrent;
import go.libtorrent.StatsTorrent;

public class LibtorrentService extends Service {
    private final IBinder mBinder = new LocalBinder();
    private NotificationManager mNM;
    private int NOTIFICATION = R.string.download_notification;
    private static int activeCount = 0;
    public static final int TORRENT_STATUS_PAUSED = 0;
    //    public static final int TORRENT_STATUS_DOWNLOADING = 1;
//    public static final int TORRENT_STATUS_SEEDING = 2;
    public static final int TORRENT_STATUS_CHECKING = 3;
    public static final int TORRENT_STATUS_QUEUED = 4;
    private NetworkChangeReceiver networkChangeReceiver;


    public LibtorrentService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createLibTorrent();
        ServerHandler serverHandler = new ServerHandler();
        serverHandler.startStreamServer(getApplicationContext());
        serverHandler.startCompletedServer(getApplicationContext());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class LocalBinder extends Binder {
        public LibtorrentService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LibtorrentService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private static boolean libtorrentCreated = false;

    public void createLibTorrent() {
        if (libtorrentCreated) {
            return;
        }
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            String version = pInfo.versionName;
            Libtorrent.setClientVersion(getApplicationContext().getString(R.string.app_name) + " " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Libtorrent.setBindAddr(":0");
        if (!Libtorrent.create()) {
            libtorrentCreated = false;
            throw new RuntimeException(Libtorrent.error());
        }
        Libtorrent.setActiveCount(activeCount);
        libtorrentCreated = true;
    }

    public void startNetworkReceiver() {
        networkChangeReceiver = new NetworkChangeReceiver();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, filter);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        unregisterReceiver(networkChangeReceiver);
        stopService();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(networkChangeReceiver);
        super.onDestroy();

    }

    public void pause() {
        Libtorrent.pause();
        notificationPaused();
    }

    public void noNetwork() {
        Libtorrent.pause();
        notificationNoNetwork();
    }


    public void resume() {
        notificationRunning();
        Libtorrent.resume();
    }

    public void startTorrent(Torrent torrent) {
        Libtorrent.startTorrent(torrent.getTorrentId());
    }


    public void stopTorrent(Torrent torrent) {
        Libtorrent.stopTorrent(torrent.getTorrentId());
    }

    public void torrentFilesCheck(Torrent torrent, long index, boolean checked) {
        Libtorrent.torrentFilesCheck(torrent.getTorrentId(), index, checked);
    }

    public long addMagnet(String storagePath, String magnetString) {
        activeCount++;
        Libtorrent.setActiveCount(activeCount);
        return Libtorrent.addMagnet(storagePath, magnetString);
    }

    public void downloadMetadata(long torrentID) {
        Libtorrent.downloadMetadata(torrentID);
    }

    public long getFileCount(long torrentID) {
        return Libtorrent.torrentFilesCount(torrentID);
    }

    public File getTorrentFile(long torrentID, int index) {
        return Libtorrent.torrentFiles(torrentID, index);
    }

    public boolean isMetaTorrent(long torrentID) {
        return Libtorrent.metaTorrent(torrentID);
    }

    public long getBytesLength(long torrentID) {
        return Libtorrent.torrentPendingBytesLength(torrentID);
    }

    public long getBytesCompleted(long torrentID) {
        return Libtorrent.torrentPendingBytesCompleted(torrentID);
    }

    public int getTorrentStatus(long torrentID) {
        return Libtorrent.torrentStatus(torrentID);
    }

    public int statusChecking() {
        return Libtorrent.StatusChecking;
    }

    public int statusQueued() {
        return Libtorrent.StatusQueued;
    }

    public int statusPaused() {
        return Libtorrent.StatusPaused;
    }

    public StatsTorrent getTorrentStats(long torrentID) {
        return Libtorrent.torrentStats(torrentID);
    }

    public void removeTorrent(long torrentID) {
        Libtorrent.removeTorrent(torrentID);
        activeCount--;
        Libtorrent.setActiveCount(activeCount);
    }

    public String getName(long torrentId) {
        return Libtorrent.torrentName(torrentId);
    }

    public byte[] readBytes(long id, long fileIndex, long offset, long length) {
        return Libtorrent.readBytes(id, fileIndex, offset, length);
    }

    public void notificationRunning() {
        String appName = getString(R.string.app_name);
        mNM = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.trikl_notification)
                .setContentTitle(appName)
                .setContentTitle(getString(R.string.running))
                .setOngoing(true);
        Intent intent = new Intent(this, SplashActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        if (mNM == null) return;
        mNM.notify(NOTIFICATION, mBuilder.build());
        startForeground(NOTIFICATION, mBuilder.build());
    }

    public void notificationPaused() {
        String appName = getString(R.string.app_name);
        mNM = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.trikl_notification)
                .setContentTitle(appName)
                .setContentTitle(getString(R.string.paused))
                .setOngoing(true);
        Intent intent = new Intent(this, SplashActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        if (mNM == null) return;
        mNM.notify(NOTIFICATION, mBuilder.build());
        startForeground(NOTIFICATION, mBuilder.build());
    }

    public void notificationNoNetwork() {
        String appName = getString(R.string.app_name);
        mNM = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.trikl_notification)
                .setContentTitle(appName)
                .setContentTitle(getString(R.string.stopped))
                .setOngoing(true);
        Intent intent = new Intent(this, SplashActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        if (mNM == null) return;
        mNM.notify(NOTIFICATION, mBuilder.build());
        startForeground(NOTIFICATION, mBuilder.build());
    }

    public void stopService() {
        stopForeground(true);
        if (mNM == null) return;
        mNM.cancel(NOTIFICATION);
    }
}
